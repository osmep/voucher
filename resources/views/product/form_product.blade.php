@extends('layouts.niche')
@section('css')
    <!-- v4.0.0-alpha.6 -->
    {{-- <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
    <!--cubeportfolio-->
    <link type="text/css" rel="stylesheet" href="dist/plugins/cubeportfolio/css/cubeportfolio.min.css">
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('dist/plugins/tooltip/tooltip.css')}}">
    <!-- popover -->
    <link rel="stylesheet" href="{{asset('dist/plugins/popover/bootstrap-popover-x.css')}}"/>
@endsection
@section('header')
    <h2>จัดการสินค้า
    {{-- <small class="text-muted">Welcome</small> --}}
    </h2>
@endsection
@section('container')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
<div class="row clearfix">
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="card">
            <div class="card-body">
                @if (isset($product_edit))
                    {!! Form::open(['route'=>['product.update', $product_edit->id_product],'method'=>'put', 'files'=>true,'id'=>'form_validation']) !!}
                        <div class="card-title">แก้ไขสินค้า</div>
                        <div class="row">
                            <div class="form-group col-12 mt-2">
                                <label class="form-label">ชื่อสินค้า <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="product_name" id="product_name" maxlength="250" value="{{$product_edit->product_name}}" required>
                            </div>
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="id_product_type">ประเภทสินค้า <span class="text-danger">*</span></label>
                                @foreach ($product_type as $pt)
                                    @if ($pt['id_product_type'] == $product_edit->id_product_type)
                                        <div class="radio">
                                            <label for="product_type_{{$pt['id_product_type']}}">
                                                <input name="id_product_type" type="radio" id="product_type_{{$pt['id_product_type']}}" value="{{$pt['id_product_type']}}" checked/>
                                                {{$pt['product_type']}}
                                            </label>
                                        </div>
                                    @else
                                        <div class="radio">
                                            <label for="product_type_{{$pt['id_product_type']}}">
                                                <input name="id_product_type" type="radio" id="product_type_{{$pt['id_product_type']}}" value="{{$pt['id_product_type']}}"/>
                                                {{$pt['product_type']}}
                                            </label>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="form-group col-12">
                                <label for="product_detail">รายละเอียดสินค้า <span class="text-danger">*</span></label><br>
                                <small>เช่น ลักษณะสินค้า จุดเด่น รูปแบบการผลิต (ผลิตเอง-รับจ้างผลิต-ส่งไปให้โรงงานผลิต) รางวัล/การรับรองต่าง ๆ (อาทิ OTOP 5 ดาว แชมป์จากการประกวดต่าง ๆ) ฯลฯ</small>
                                <textarea name="product_detail" cols="30" rows="5" class="form-control no-resize" placeholder="กรุณาระบุรายละเอียดสินค้า..." required>{{$product_edit->product_detail}}</textarea>
                            </div>
                            <div class="form-group col-12">
                                <label class="product_image">รูปภาพสินค้า</label>
                                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="{{asset('voucher/product/'.substr($product_edit->id_product,0,1).'/'.$product_edit->product_image)}}" data-sub-html="Demo Description">
                                            <img class="img-fluid img-thumbnail" src="{{asset('voucher/product/'.substr($product_edit->id_product,0,1).'/'.$product_edit->product_image)}}" alt="{{$product_edit->product_name}}">
                                        </a>
                                    </div>
                                </div>
                                <label><input type="checkbox" id="edit_product_image"> ต้องการแก้ไขรูปสินค้า</label><br>
                                {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                <label for="product_image">อัพโหลดรูปสินค้าใหม่</label>
                                <input type="file" id="product_image" name="product_image" disabled>
                                {{-- <a onclick="return confirm('ท่านต้องการลบรูปภาพสินค้าใช่หรือไม่');" href="/delproductimage/{{$product_edit->id_product}}" class="btn btn-default waves-effect waves-float mt-1" data-bs-toggle="tooltip" data-bs-placement="top" title="ลบรูปภาพ"><i class="fa fa-trash-o"></i></a> --}}
                                {{-- <img src="{{asset('voucher/product/'.substr($product_edit->id_product,0,1).'/'.$product_edit->product_image)}}" alt="{{$product_edit->product_image}}" class="img-fluid" width="200" /> --}}
                            </div>
                            <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                            <div class="form-group col-12">
                                <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                @else
                    @if (count($product) == 0)
                        {!! Form::open(['url'=>'/product','files'=>true,'id'=>'form_validation']) !!}
                            <div class="card-title">เพิ่มสินค้า</div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label class="product_name">ชื่อสินค้า <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="product_name" maxlength="250" id="product_name" required>
                                </div>
                                <div class="form-group col-lg-12 col-sm-12">
                                    <label for="id_product_type">ประเภทสินค้า <span class="text-danger">*</span></label>
                                    @foreach ($product_type as $pt)
                                        <div class="radio">
                                            <label for="product_type_{{$pt['id_product_type']}}">
                                                <input name="id_product_type" type="radio" id="product_type_{{$pt['id_product_type']}}" value="{{$pt['id_product_type']}}" />
                                                {{$pt['product_type']}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group col-12">
                                    <label for="product_detail">รายละเอียดสินค้า <span class="text-danger">*</span></label><br>
                                    <small>เช่น ลักษณะสินค้า จุดเด่น รูปแบบการผลิต (ผลิตเอง-รับจ้างผลิต-ส่งไปให้โรงงานผลิต) รางวัล/การรับรองต่าง ๆ (อาทิ OTOP 5 ดาว แชมป์จากการประกวดต่าง ๆ) ฯลฯ</small>
                                    <textarea name="product_detail" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label class="product_image">รูปภาพสินค้า</label>
                                    <input type="file" name="product_image" id="product_image">
                                </div>
                                <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                <div class="form-group col-12">
                                    <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    @else
                        {!! Form::open(['url'=>'/product','files'=>true,'id'=>'form_validation']) !!}
                            <div class="alert alert-secondary fade show" role="alert">
                                คุณเพิ่มสินค้าครบจำนวนแล้ว
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label class="form-label">ชื่อสินค้า <span class="text-muted">*</span></label>
                                    <input type="text" class="form-control" name="product_name" id="product_name" disabled required>
                                </div>
                                <div class="form-group col-lg-12 col-sm-12">
                                    <label for="id_product_type">ประเภทสินค้า <span class="text-danger">*</span></label>
                                    @foreach ($product_type as $pt)
                                        <div class="radio">
                                            <label for="product_type_{{$pt['id_product_type']}}">
                                                <input name="id_product_type" type="radio" id="product_type_{{$pt['id_product_type']}}" value="{{$pt['id_product_type']}}" disabled/>
                                                {{$pt['product_type']}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="form-group col-12">
                                    <label for="product_detail">รายละเอียดสินค้า <span class="text-danger">*</span></label><br>
                                    <small>เช่น ลักษณะสินค้า จุดเด่น รูปแบบการผลิต (ผลิตเอง-รับจ้างผลิต-ส่งไปให้โรงงานผลิต) รางวัล/การรับรองต่าง ๆ (อาทิ OTOP 5 ดาว แชมป์จากการประกวดต่าง ๆ) ฯลฯ</small>
                                    <textarea name="product_detail" cols="30" rows="5" class="form-control no-resize" disabled required></textarea>
                                </div>
                                <div class="form-group col-12">
                                    <label class="product_image">รูปภาพสินค้า</label>
                                    <input type="file" name="product_image" id="product_image" disabled>
                                </div>
                                <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                <div class="form-group col-12">
                                    <button class="btn btn-raised btn-primary waves-effect" type="submit" disabled>บันทึกข้อมูล</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12">
        @foreach ($product as $prd)
            <!-- Card -->
            <div class="card">
                <img class="card-img-top img-responsive" src="{{asset('voucher/product/'.substr($prd['id_product'],0,1).'/'.$prd['product_image'])}}" alt="{{$prd['product_name']}}">
                <div class="card-body">
                    <h4 class="card-title">{{$prd['product_name']}}</h4>
                    <p class="card-text">{{$prd['product_type']->product_type}}</p>
                    <div class="btn-group">
                        <button type="button" class="js-mytooltip btn btn-default btn-sm" data-toggle="modal" data-target="#largeModal" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></button>
                        <a href="/product/{{$prd['id_product']}}/edit" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>
                        {{ Form::open(['route' => ['product.destroy', $prd['id_product']]]) }}
                            <input type="hidden" name="_method" value="delete">
                            <button type="submit" class="js-mytooltip btn btn-default btn-sm" onclick="return confirm('คุณต้องการลบสินค้าใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <!-- Card -->

            <!-- Large Size -->
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">รายละเอียดสินค้า</h4>
                        </div>
                        <div class="modal-body">
                            <img class="img-fluid img-thumbnail" src="{{asset('voucher/product/'.substr($prd['id_product'],0,1).'/'.$prd['product_image'])}}" alt="{{$prd['product_name']}}">
                            <table class="table">
                                <tr>
                                    <td style="width: 20%"><strong>ชื่อสินค้า</strong></td>
                                    <td>{{$prd['product_name']}}</td>
                                </tr>
                                <tr>
                                    <td><strong>ประเภทสินค้า</strong></td>
                                    <td>{{$prd['product_type']->product_type}}</td>
                                </tr>
                                <tr>
                                    <td><strong>รายละเอียดสินค้า</strong></td>
                                    <td>{{$prd['product_detail']}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}
    <!--cubeportfolio-->
    <!-- load jquery -->
    <script src="{{asset('dist/plugins/cubeportfolio/jquery-latest.min.js')}}"></script>
    <!-- load cubeportfolio -->
    <script src="{{asset('dist/plugins/cubeportfolio/jquery.cubeportfolio.min.js')}}"></script>
    <!-- init cubeportfolio -->
    <script src="{{asset('dist/plugins/cubeportfolio/main.js')}}"></script>
    <!-- tooltip -->
    <script src="{{asset('dist/plugins/tooltip/tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/tooltip/script.js')}}"></script>
    <!-- popover -->
    <script src="{{asset('dist/plugins/popover/bootstrap-popover-x.js')}}"></script>
    <script>
        $("#edit_product_image").click(function () {
            if($(this).is(":checked")) {
                // $( "#product_image" ).rules( "add", {
                //     required: true,
                //     messages: {
                //     required: "กรุณากรอกข้อมูล",
                //     }
                // });
                $("#product_image").removeAttr("disabled");
            }
            else {
                $("#product_image").attr("disabled", "disabled");
                $("#product_image").prop('required',false);
            }
        });
    </script>
@endsection
