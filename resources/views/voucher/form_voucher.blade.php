@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('dist/plugins/tooltip/tooltip.css')}}">
    <!-- popover -->
    <link rel="stylesheet" href="{{asset('dist/plugins/popover/bootstrap-popover-x.css')}}"/>
    @php
        function DateThai($strDate)
        {
            $strYear = date("Y",strtotime($strDate))+543;
            $strMonth= date("n",strtotime($strDate));
            $strDay= date("j",strtotime($strDate));
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
            $strMonthThai=$strMonthCut[$strMonth];
            return "$strDay $strMonthThai $strYear";
        }
    @endphp
    <style>
        p.overflow-hidden {
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
@endsection
@section('header')
    <h2>จัดการ Voucher</h2>
@endsection
@section('container')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
<div class="info-box">
    <div class="row">
        <div class="col-lg-6">
            @if (isset($voucher_edit))
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h4>แก้ไขการใช้งาน Voucher</h4>
                        </div>
                        {!! Form::open(['route'=>['voucher.update', $voucher_edit->id_voucher_register],'method'=>'put', 'files'=>true,'id'=>'form_advisor']) !!}
                            <div class="row">
                                @if ($voucher_edit->id_advisor == '')
                                    <div class="form-group col-12">
                                        <label for="voucher_topic">หัวข้อการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <select id="voucher_topic" name="voucher_topic" class="form-control" required>
                                            @switch($voucher_edit->voucher_topic)
                                                @case('การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต')
                                                    <option value="การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต" selected>การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต</option>
                                                    <option value="การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ">การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ</option>
                                                    <option value="การพัฒนาบรรจุภัณฑ์">การพัฒนาบรรจุภัณฑ์</option>
                                                    @break
                                                @case('การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ')
                                                    <option value="การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต">การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต</option>
                                                    <option value="การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ" selected>การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ</option>
                                                    <option value="การพัฒนาบรรจุภัณฑ์">การพัฒนาบรรจุภัณฑ์</option>
                                                    @break
                                                @case('การพัฒนาบรรจุภัณฑ์')
                                                    <option value="การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต">การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต</option>
                                                    <option value="การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ">การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ</option>
                                                    <option value="การพัฒนาบรรจุภัณฑ์" selected>การพัฒนาบรรจุภัณฑ์</option>
                                                    @break
                                                @default
                                                    <option value="">กรุณาเลือกหัวข้อการพัฒนาผลิตภัณฑ์</option>
                                                    <option value="การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต">การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต</option>
                                                    <option value="การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ">การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ</option>
                                                    <option value="การพัฒนาบรรจุภัณฑ์">การพัฒนาบรรจุภัณฑ์</option>
                                            @endswitch
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_detail">รายละเอียดการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <textarea id="voucher_detail" name="voucher_detail" cols="30" rows="5" class="form-control no-resize" required>{{$voucher_edit->voucher_detail}}</textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่เริ่มให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input id="voucher_date" name="voucher_date" type="date" class="form-control" value="{{$voucher_edit->voucher_date}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่สิ้นสุดให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input id="voucher_date_stop" name="voucher_date_stop" type="date" class="form-control" value="{{$voucher_edit->voucher_date_stop}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_place">สถานที่ให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="voucher_place" name="voucher_place" value="{{$voucher_edit->voucher_place}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_cost">ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <input type="number" step=".01" class="form-control" id="voucher_cost" name="voucher_cost" value="{{$voucher_edit->voucher_cost}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_evidence">หลักฐานการให้คำปรึกษา <span class="text-danger">*</span></label><br>
                                        @if (substr($voucher_edit->voucher_evidence_1,-3) == 'pdf')
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_1)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                        @else
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_1)}}" target="_blank" data-sub-html="Demo Description">
                                                <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_1)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                            </a>
                                        @endif
                                        <div class="demo-switch">
                                            <div class="switch">
                                                <label><input type="checkbox" id="edit_voucher_evidence_1"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                            </div>
                                        </div>
                                        {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                        <div id="field_voucher_evidence_1" style="display: none">
                                            <label for="voucher_evidence_1">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                            @if ($voucher_edit->id_advisor == '')
                                                <input type="file" id="voucher_evidence_1" name="voucher_evidence_1" accept=".jpg, .png, .pdf">
                                            @else
                                                <input type="file" id="voucher_evidence_1" name="voucher_evidence_1" accept=".jpg, .png">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_2 != '')
                                            @if (substr($voucher_edit->voucher_evidence_2,-3) == 'pdf')
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_2)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                            @else
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_2)}}" target="_blank" data-sub-html="Demo Description">
                                                    <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_2)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                </a>
                                            @endif
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_voucher_evidence_2"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_voucher_evidence_2" style="display: none">
                                                <label for="voucher_evidence_2">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_2" name="voucher_evidence_2" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_2" name="voucher_evidence_2" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_2" name="voucher_evidence_2" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_2" name="voucher_evidence_2" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_3 != '')
                                            @if (substr($voucher_edit->voucher_evidence_3,-3) == 'pdf')
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_3)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                            @else
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_3)}}" target="_blank" data-sub-html="Demo Description">
                                                    <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_3)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                </a>
                                            @endif
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_voucher_evidence_3"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_voucher_evidence_3" style="display: none">
                                                <label for="voucher_evidence_3">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_3" name="voucher_evidence_3" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_3" name="voucher_evidence_3" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_3" name="voucher_evidence_3" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_3" name="voucher_evidence_3" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_4 != '')
                                            @if (substr($voucher_edit->voucher_evidence_4,-3) == 'pdf')
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_4)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                            @else
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_4)}}" target="_blank" data-sub-html="Demo Description">
                                                    <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_4)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                </a>
                                            @endif
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_voucher_evidence_4"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_voucher_evidence_4" style="display: none">
                                                <label for="voucher_evidence_4">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_4" name="voucher_evidence_4" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_4" name="voucher_evidence_4" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_4" name="voucher_evidence_4" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_4" name="voucher_evidence_4" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_5 != '')
                                            @if (substr($voucher_edit->voucher_evidence_5,-3) == 'pdf')
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_5)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                            @else
                                                <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_5)}}" target="_blank" data-sub-html="Demo Description">
                                                    <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_5)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                </a>
                                            @endif
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_voucher_evidence_5"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_voucher_evidence_5" style="display: none">
                                                <label for="voucher_evidence_5">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_5" name="voucher_evidence_5" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_5" name="voucher_evidence_5" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                @if ($voucher_edit->id_advisor == '')
                                                    <input type="file" id="voucher_evidence_5" name="voucher_evidence_5" accept=".jpg, .png, .pdf">
                                                @else
                                                    <input type="file" id="voucher_evidence_5" name="voucher_evidence_5" accept=".jpg, .png">
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                    <input type="hidden" name="type_activity" value="voucher">
                                @else
                                    <div class="form-group col-12">
                                        <label class="form-label">หัวข้อการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="consult_topic" name="consult_topic" value="{{$voucher_edit->voucher_topic}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">รายละเอียดการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <textarea id="consult_detail" name="consult_detail" cols="30" rows="5" class="form-control no-resize" required>{{$voucher_edit->voucher_detail}}</textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่เริ่มการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="consult_date" name="consult_date" value="{{$voucher_edit->voucher_date}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่สิ้นสุดการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="consult_date_stop" name="consult_date_stop" value="{{$voucher_edit->voucher_date_stop}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">สถานที่ให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="consult_place" name="consult_place" value="{{$voucher_edit->voucher_place}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <input type="number" step=".01" class="form-control" id="consult_cost" name="consult_cost" value="{{$voucher_edit->voucher_cost}}" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">ที่ปรึกษาในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <select class="form-control" id="id_advisor" name="id_advisor" required>
                                            {{-- <option value="{{$voucher_edit->id_advisor}}" selected>{{$voucher_edit->advisor->advisor_name}}</option> --}}
                                            @foreach ($advisor as $a)
                                                @if ($a['id_advisor'] == $voucher_edit->id_advisor)
                                                    <option value="{{$a['id_advisor']}}" selected>{{$a['advisor_name']}}</option>
                                                @else
                                                    <option value="{{$a['id_advisor']}}">{{$a['advisor_name']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="consult_evidence">หลักฐานการให้คำปรึกษา <span class="text-danger">*</span></label><br>
                                        <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_1)}}" target="_blank" data-sub-html="Demo Description">
                                            <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_1)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                        </a>
                                        <div class="demo-switch">
                                            <div class="switch">
                                                <label><input type="checkbox" id="edit_consult_evidence_1"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                            </div>
                                        </div>
                                        {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                        <div id="field_consult_evidence_1" style="display: none">
                                            <label for="consult_evidence_1">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                            <input type="file" id="consult_evidence_1" name="consult_evidence_1" accept=".jpg, .png">
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_2)
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_2)}}" target="_blank" data-sub-html="Demo Description">
                                                <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_2)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                            </a>
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_consult_evidence_2"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_consult_evidence_2" style="display: none">
                                                <label for="consult_evidence_2">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                <input type="file" id="consult_evidence_2" name="consult_evidence_2" accept=".jpg, .png">
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                <input type="file" id="consult_evidence_2" name="consult_evidence_2" accept=".jpg, .png">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_3)
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_3)}}" target="_blank" data-sub-html="Demo Description">
                                                <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_3)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                            </a>
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_consult_evidence_3"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_consult_evidence_3" style="display: none">
                                                <label for="consult_evidence_3">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                <input type="file" id="consult_evidence_3" name="consult_evidence_3" accept=".jpg, .png">
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                <input type="file" id="consult_evidence_3" name="consult_evidence_3" accept=".jpg, .png">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_4)
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_4)}}" target="_blank" data-sub-html="Demo Description">
                                                <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_4)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                            </a>
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_consult_evidence_4"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_consult_evidence_4" style="display: none">
                                                <label for="consult_evidence_4">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                <input type="file" id="consult_evidence_4" name="consult_evidence_4" accept=".jpg, .png">
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                <input type="file" id="consult_evidence_4" name="consult_evidence_4" accept=".jpg, .png">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        @if ($voucher_edit->voucher_evidence_5)
                                            <a href="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_5)}}" target="_blank" data-sub-html="Demo Description">
                                                <img src="{{asset('voucher/evidence/'.substr($voucher_edit->id_voucher_register,0,1).'/'.$voucher_edit->id_card.'/'.$voucher_edit->voucher_evidence_5)}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                            </a>
                                            <div class="demo-switch">
                                                <div class="switch">
                                                    <label><input type="checkbox" id="edit_consult_evidence_5"><span class="lever"></span> ต้องการแก้ไขหลักฐาน</label>
                                                </div>
                                            </div>
                                            {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                            <div id="field_consult_evidence_5" style="display: none">
                                                <label for="consult_evidence_5">อัพโหลดหลักฐาน <span class="text-danger">*</span></label>
                                                <input type="file" id="consult_evidence_5" name="consult_evidence_5" accept=".jpg, .png">
                                            </div>
                                        @else
                                            <div class="form-group col-12">
                                                <input type="file" id="consult_evidence_5" name="consult_evidence_5" accept=".jpg, .png">
                                            </div>
                                        @endif
                                    </div>
                                    <input type="hidden" name="type_activity" value="consult">
                                @endif
                                <div class="col-12">
                                    <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                    <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                                    <a href="/voucher/create/{{$id_card}}" class="btn btn-raised btn-secondary waves-effect">ยกเลิก</a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h4>เพิ่มการใช้งาน Voucher</h4>
                        </div>
                        <form id="form_validation" action="/voucher" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <label class="col-12">กรุณาเลือกประเภทกิจกรรมที่ต้องการกรอกข้อมูล</label>
                                <div class="col-4">
                                    <div class="radio">
                                        <label for="voucher"><input name="type_activity" type="radio" id="voucher" value="voucher" required/> การพัฒนาผลิตภัณฑ์</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="radio">
                                        <label for="consult"><input name="type_activity" type="radio" id="consult" value="consult"/> การให้คำปรึกษา</label>
                                    </div>
                                </div>
                                {{-- voucher_form --}}
                                <div id="voucher_form" class="col-12" style="display: none">
                                    <div class="form-group col-12">
                                        <label for="voucher_topic">หัวข้อการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <select id="voucher_topic" name="voucher_topic" class="form-control">
                                            <option value="">กรุณาเลือกหัวข้อการพัฒนาผลิตภัณฑ์</option>
                                            <option value="การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต">การพัฒนาผลิตภัณฑ์/เทคโนโลยีการผลิต</option>
                                            <option value="การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ">การทดสอบ/ตรวจวิเคราะห์ ทางห้องปฏิบัติการ</option>
                                            <option value="การพัฒนาบรรจุภัณฑ์">การพัฒนาบรรจุภัณฑ์</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_detail">รายละเอียดการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <textarea id="voucher_detail" name="voucher_detail" cols="30" rows="5" class="form-control no-resize" ></textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่เริ่มให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input id="voucher_date" name="voucher_date" type="date" class="form-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่สิ้นสุดให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input id="voucher_date_stop" name="voucher_date_stop" type="date" class="form-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_place">สถานที่ให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="voucher_place" name="voucher_place" >
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="voucher_cost">ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <input type="number" step=".01" class="form-control" id="voucher_cost" name="voucher_cost" >
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="report_200">หลักฐานการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <input type="file" id="voucher_evidence_1" name="voucher_evidence_1" accept=".jpg, .png, .pdf" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="voucher_evidence_2" name="voucher_evidence_2" accept=".jpg, .png, .pdf">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="voucher_evidence_3" name="voucher_evidence_3" accept=".jpg, .png, .pdf">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="voucher_evidence_4" name="voucher_evidence_4" accept=".jpg, .png, .pdf">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="voucher_evidence_5" name="voucher_evidence_5" accept=".jpg, .png, .pdf">
                                    </div>
                                </div>
                                {{-- consult_form --}}
                                <div id="consult_form" class="col-12" style="display: none">
                                    <div class="col-12">
                                        <div class="alert alert-warning alert-dismissible">
                                            <strong>คำแนะนำ</strong><br>
                                            หากท่านยังไม่ได้เพิ่มข้อมูลที่ปรึกษา กรุณาทำการเพิ่มข้อมูลที่ปรึกษาก่อนการบันทึกข้อมูลการให้คำปรึกษา
                                            <a href="/advisor/create/{{$id_card}}" class="text-primary">คลิกเพื่อเพิ่มที่ปรึกษา</a>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">หัวข้อการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="consult_topic" name="consult_topic" >
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">รายละเอียดการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <textarea id="consult_detail" name="consult_detail" cols="30" rows="5" class="form-control no-resize" ></textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่เริ่มให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="consult_date" name="consult_date">
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">วันที่สิ้นสุดให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" id="consult_date_stop" name="consult_date_stop">
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">ช่องทางการให้บริการ/รับบริการ <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="consult_place" name="consult_place" >
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <input type="number" step=".01" class="form-control" id="consult_cost" name="consult_cost" >
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="form-label">ที่ปรึกษาในการพัฒนาผลิตภัณฑ์ <span class="text-danger">*</span></label>
                                        <select class="form-control" id="id_advisor" name="id_advisor">
                                            <option value="">กรุณาเลือกที่ปรึกษาในการพัฒนาผลิตภัณฑ์</option>
                                            @foreach ($advisor as $a)
                                                <option value="{{$a['id_advisor']}}">{{$a['advisor_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="report_200">หลักฐานการให้คำปรึกษา <span class="text-danger">*</span></label>
                                        <input type="file" id="consult_evidence_1" name="consult_evidence_1" accept=".jpg, .png" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="consult_evidence_2" name="consult_evidence_2" accept=".jpg, .png">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="consult_evidence_3" name="consult_evidence_3" accept=".jpg, .png">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="consult_evidence_4" name="consult_evidence_4" accept=".jpg, .png">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="file" id="consult_evidence_5" name="consult_evidence_5" accept=".jpg, .png">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                    <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4>การใช้งาน Voucher</h4>
                    </div>
                    <hr>
                    รายงานสรุปค่าใช้จ่าย
                    @if ($cost_summary == '')
                        <form action="/savecostsummary" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col-12">
                                <label for="cost_summary">ไฟล์รายงานสรุปค่าใช้จ่าย <span class="text-danger">*</span></label>
                                <input type="file" id="cost_summary" name="cost_summary" accept=".pdf" required>
                            </div>
                            <div class="col-12">
                                <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                            </div>
                        </form>
                    @else
                        <a data-toggle="modal" data-target="#reportSumCost"><h1><i class="fa fa-file-text-o"></i></h1></a>
                        {{-- <a href="{{asset('voucher/evidence/'.$cost_summary->profile->id_team_owner.'/'.$cost_summary->id_card.'/'.$cost_summary->cost_summary)}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a> --}}
                        <div class="demo-switch">
                            <div class="switch">
                                <label><input type="checkbox" id="edit_cost_summary"><span class="lever"></span> ต้องการแก้ไขรายงานสรุปค่าใช้จ่าย</label>
                            </div>
                        </div>
                        <div id="field_cost_summary" style="display: none">
                            <form action="/updatecostsummary" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group col-12">
                                    <label for="cost_summary">ไฟล์รายงานสรุปค่าใช้จ่าย <span class="text-danger">*</span></label>
                                    <input type="file" id="cost_summary" name="cost_summary" accept=".pdf" required>
                                </div>
                                <div class="col-12">
                                    <input type="hidden" id="id_cost_summary" name="id_cost_summary" value="{{$cost_summary->id_cost_summary}}">
                                    <input type="hidden" id="id_card" name="id_card" value="{{$id_card}}">
                                    <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal fade" id="reportSumCost" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel">รายงานสรุปค่าใช้จ่าย</h4>
                                        <div class="modal-option" id="defaultModalLabel">
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        <iframe src="{{asset('voucher/evidence/'.$cost_summary->profile->id_team_owner.'/'.$cost_summary->id_card.'/'.$cost_summary->cost_summary)}}" width="100%" height="500px">
                                        </iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <hr>
                    <span class="text-right">ค่าใช้จ่ายที่ใช้ไปทั้งสิ้น <span class="text-primary">{{number_format($voucher_cost,2)}}</span> บาท</span>
                    @foreach ($voucher_register as $vr)
                        <div class="sl-item sl-primary">
                            <div class="sl-content">
                                <h5>
                                    <span class="badge badge-primary">
                                        @if ($vr['voucher_date'] == $vr['voucher_date_stop'])
                                            {{DateThai($vr['voucher_date'])}}
                                        @else
                                            {{DateThai($vr['voucher_date'])}} - {{DateThai($vr['voucher_date_stop'])}}
                                        @endif
                                    </span>
                                    @if ($vr['id_advisor'] == '')
                                        <span class="badge badge-warning">การพัฒนาผลิตภัณฑ์</span>
                                    @else
                                        <span class="badge badge-success">การให้คำปรึกษา</span>
                                    @endif
                                </h5>
                                <p class="overflow-hidden">
                                    <span class="text-warning">ค่าใช้จ่าย {{number_format($vr['voucher_cost'],2)}} บาท</span><br>
                                    {{$vr['voucher_topic']}} <br>
                                    {{$vr['voucher_detail']}} <br>
                                    <div class="btn-group">
                                        <button type="button" class="js-mytooltip btn btn-default btn-sm" data-toggle="modal" data-target="#defaultModal{{$vr['id_voucher_register']}}" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></button>
                                        <a href="/voucher/{{$vr['id_voucher_register']}}/edit/{{$id_card}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>
                                        {{ Form::open(['route' => ['voucher.destroy', $vr['id_voucher_register']]]) }}
                                            <input type="hidden" name="_method" value="delete">
                                            <button type="submit" class="js-mytooltip btn btn-default btn-sm" onclick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></button>
                                        {{ Form::close() }}
                                    </div>
                                </p>
                            </div>
                        </div>
                        <div class="modal fade" id="defaultModal{{$vr['id_voucher_register']}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel">การใช้งาน Voucher</h4>
                                        <div class="modal-option" id="defaultModalLabel">
                                            {{-- {{ Form::open(['route' => ['voucher.destroy', $vr['id_voucher_register']]]) }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button type="submit" class="js-mytooltip btn btn-danger btn-sm" onclick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i> ลบข้อมูล</button>
                                            {{ Form::close() }} --}}
                                        </div>
                                    </div>
                                    <div class="modal-body">
                                        @if ($vr['id_advisor'] == '')
                                            <table id="show_voucher" class="table">
                                                <tr>
                                                    <td style="width: 30%"><strong>หัวข้อการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['voucher_topic']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>รายละเอียดการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['voucher_detail']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>วันที่ได้ให้บริการ/รับบริการ</strong></td>
                                                    <td>
                                                        @if ($vr['voucher_date'] == $vr['voucher_date_stop'])
                                                            {{DateThai($vr['voucher_date'])}}
                                                        @else
                                                            {{DateThai($vr['voucher_date'])}} - {{DateThai($vr['voucher_date_stop'])}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>สถานที่ให้บริการ/รับบริการ</strong></td>
                                                    <td>{{$vr['voucher_place']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{number_format($vr['voucher_cost'])}} บาท</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <strong>หลักฐานการพัฒนาผลิตภัณฑ์</strong><br>
                                                        @if (substr($vr['voucher_evidence_1'],-3) == 'pdf' | substr($vr['voucher_evidence_1'],-3) == 'PDF')
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                        @else
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if ($vr['voucher_evidence_2'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_2'],-3) == 'pdf' | substr($vr['voucher_evidence_2'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_3'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_3'],-3) == 'pdf' | substr($vr['voucher_evidence_3'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_4'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_4'],-3) == 'pdf' | substr($vr['voucher_evidence_4'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_5'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_5'],-3) == 'pdf' | substr($vr['voucher_evidence_5'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                        @else
                                            <table id="show_consult" class="table">
                                                <tr>
                                                    <td style="width: 30%"><strong>หัวข้อการให้คำปรึกษา</strong></td>
                                                    <td>{{$vr['voucher_topic']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>รายละเอียดการให้คำปรึกษา</strong></td>
                                                    <td>{{$vr['voucher_detail']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>วันที่ให้คำปรึกษา</strong></td>
                                                    <td>{{DateThai($vr['voucher_date'])}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>สถานที่ให้บริการ/รับบริการ</strong></td>
                                                    <td>{{$vr['voucher_place']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{number_format($vr['voucher_cost'])}} บาท</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ที่ปรึกษาในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['advisor']->advisor_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <strong>หลักฐานการให้คำปรึกษา</strong><br>
                                                        <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank" data-sub-html="Demo Description">
                                                            <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                @if ($vr['voucher_evidence_2'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_3'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_4'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_5'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" width="300" alt="หลักฐานการพัฒนาผลิตภัณฑ์" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    {{-- <!-- jQuery 3 -->
    <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- tooltip -->
    <script src="{{asset('dist/plugins/tooltip/tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/tooltip/script.js')}}"></script>
    <!-- popover -->
    <script src="{{asset('dist/plugins/popover/bootstrap-popover-x.js')}}"></script>

    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>
    <script src="{{asset('js/voucher.js')}}"></script>
    <script>
        $('#form_validation').validate();

        // $('#consult_date').change(function(){
        //     if ($(this).val()!='') {
        //         var select = $(this).val();
        //         var id_card = $('#id_card').val();
        //         var _token = $('input[name="_token"]').val();
        //         $.ajax({
        //             url: "{{route('dropdown.fetchadvisor')}}",
        //             method:"POST",
        //             data:{select:select,_token:_token,id_card:id_card}, //โยนข้อมูลไปทำงานที่ controller
        //             success:function(result){
        //                 //ได้ข้อมูลมาแล้วทำอะไรต่อ
        //                 console.log(select);
        //                 $("#id_advisor").addClass("show-tick");
        //                 $('#id_advisor').html(result);
        //             }
        //         })
        //     }
        // });
        $("#edit_cost_summary").click(function () {
            if($(this).is(":checked")) {
                $("#field_cost_summary").show();
            }
            else {
                $("#field_cost_summary").hide();
            }
        });

        $("#edit_voucher_evidence_1").click(function () {
            if($(this).is(":checked")) {
                $("#field_voucher_evidence_1").show();
            }
            else {
                $("#field_voucher_evidence_1").hide();
            }
        });
        $("#edit_voucher_evidence_2").click(function () {
            if($(this).is(":checked")) {
                $("#field_voucher_evidence_2").show();
            }
            else {
                $("#field_voucher_evidence_2").hide();
            }
        });
        $("#edit_voucher_evidence_3").click(function () {
            if($(this).is(":checked")) {
                $("#field_voucher_evidence_3").show();
            }
            else {
                $("#field_voucher_evidence_3").hide();
            }
        });
        $("#edit_voucher_evidence_4").click(function () {
            if($(this).is(":checked")) {
                $("#field_voucher_evidence_4").show();
            }
            else {
                $("#field_voucher_evidence_4").hide();
            }
        });
        $("#edit_voucher_evidence_5").click(function () {
            if($(this).is(":checked")) {
                $("#field_voucher_evidence_5").show();
            }
            else {
                $("#field_voucher_evidence_5").hide();
            }
        });

        $("#edit_consult_evidence_1").click(function () {
            if($(this).is(":checked")) {
                $("#field_consult_evidence_1").show();
            }
            else {
                $("#field_consult_evidence_1").hide();
            }
        });
        $("#edit_consult_evidence_2").click(function () {
            if($(this).is(":checked")) {
                $("#field_consult_evidence_2").show();
            }
            else {
                $("#field_consult_evidence_2").hide();
            }
        });
        $("#edit_consult_evidence_3").click(function () {
            if($(this).is(":checked")) {
                $("#field_consult_evidence_3").show();
            }
            else {
                $("#field_consult_evidence_3").hide();
            }
        });
        $("#edit_consult_evidence_4").click(function () {
            if($(this).is(":checked")) {
                $("#field_consult_evidence_4").show();
            }
            else {
                $("#field_consult_evidence_4").hide();
            }
        });
        $("#edit_consult_evidence_5").click(function () {
            if($(this).is(":checked")) {
                $("#field_consult_evidence_5").show();
            }
            else {
                $("#field_consult_evidence_5").hide();
            }
        });
    </script>
@endsection
