
@if (Auth::user()->approve_status == 1)
    @include('home.home_waiting_approve')
@else
    @if (Auth::user()->id_level == 1)
        @include('home.home_admin')
    @else
        @include('home.home_user')
    @endif
@endif
