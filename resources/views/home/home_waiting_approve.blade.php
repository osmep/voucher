<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Voucher :: Sign Up</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

<!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.min.css')}}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-box-body">
        <div class="text-center"><img src="{{asset('dist/img/logo-osmep.png')}}" alt=""></div>
        <p class="mt-3 text-center">โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</p>
        <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('logout') }}">
            @csrf
            <hr>
            <h5 class="title text-center text-primary">รอการอนุมัติ</h5>
            <p class="text-center">เนื่องจากจะต้องได้รับการอนุมัติเข้าใช้งานระบบ
                <br>หากท่านรอนานเกิน 3 วัน โปรดติดต่อ
                <br>สำนักงานส่งเสริมวิสาหกิจขนาดกลางและขนาดย่อม</p>
            <div class="col-lg-12 mt-3 text-center">
                <button type="submit" class="btn btn-raised btn-outline-primary waves-effect">ออกจากระบบ</button>
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('dist/js/jquery.min.js')}}"></script>

<!-- v4.0.0-alpha.6 -->
<script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- template -->
<script src="{{asset('dist/js/niche.js')}}"></script>
</body>
</html>
