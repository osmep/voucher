<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Voucher :: Dashboard</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/authentication.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
<!-- Bootstrap Select Css -->
<link href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
</head>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <div class="logo"><img src="{{asset('assets/images/logo.png')}}" alt="Nexa"></div>
                        {{-- <h1>Voucher</h1> --}}
                        <p class="mt-3">โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</p>
                        {{-- <ul class="list-unstyled l-social">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>                            
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        </ul> --}}
                    </div>                        
                </div>
                <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('logout') }}">
                    @csrf
                    <h5 class="title">รอการอนุมัติ</h5>
                    <small class="msg">เนื่องจากจะต้องได้รับการอนุมัติเข้าใช้งานระบบ
                        <br>หากท่านรอนานเกิน 3 วัน โปรดติดต่อ
                        <br>สำนักงานส่งเสริมวิสาหกิจขนาดกลางและขนาดย่อม</small>
                    <div class="col-lg-12 mt-3">
                        <button type="submit" class="btn btn-raised btn-primary waves-effect">ออกจากระบบ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Jquery Core Js --> 
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->
</body>
</html>