@extends('layouts.niche')
@section('css')
{{-- <!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}

{{-- <link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.css')}}" /> --}}
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('dist/plugins/datatables/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('header')
    <h2>Dashboard
    <small class="text-muted">Welcome</small>
    </h2>
@endsection
@section('container')
<div class="row">
    <div class="col-lg-4 col-sm-8 col-xs-12">
        <a href="/dashboard">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-blue"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_all)}}<span class="text-muted">/430</span></h1>
                        <span class="progress-description">ผู้ประกอบการทั้งหมด</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_all  = ($entrepreneur_team_all*100)/430;
                        @endphp
                        <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_all}}%; height:6px;"> <span class="sr-only">{{$percent_all}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="/dashboardteam/2">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-secondary"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_2)}}<span class="text-muted">/70</span></h1>
                        <span class="progress-description">สำนักงานส่งเสริมเศรษฐกิจสร้างสรรค์ (องค์การมหาชน)</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_team_2  = ($entrepreneur_team_2*100)/70;
                        @endphp
                        <div class="progress-bar bg-secondary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_team_2}}%; height:6px;"> <span class="sr-only">{{$percent_team_2}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="/dashboardteam/3">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-success"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_3)}}<span class="text-muted">/150</span></h1>
                        <span class="progress-description">บริษัท ห้องปฏิบัติการกลาง (ประเทศไทย) จำกัด</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_team_3  = ($entrepreneur_team_3*100)/150;
                        @endphp
                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_team_3}}%; height:6px;"> <span class="sr-only">{{$percent_team_3}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="/dashboardteam/4">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-danger"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_4)}}<span class="text-muted">/70</span></h1>
                        <span class="progress-description">มหาวิทยาลัยเชียงใหม่</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_team_4  = ($entrepreneur_team_4*100)/70;
                        @endphp
                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_team_4}}%; height:6px;"> <span class="sr-only">{{$percent_team_4}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="/dashboardteam/5">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-warning"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_5)}}<span class="text-muted">/70</span></h1>
                        <span class="progress-description">สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_team_5  = ($entrepreneur_team_5*100)/70;
                        @endphp
                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_team_5}}%; height:6px;"> <span class="sr-only">{{$percent_team_5}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a href="/dashboardteam/6">
            <div class="info-box">
                <div> <i class="ti-stats-up f-20 text-info"></i>
                    <div class="info-box-content">
                        <h1 class="f-25 text-black">{{number_format($entrepreneur_team_6)}}<span class="text-muted">/70</span></h1>
                        <span class="progress-description">อุตสาหกรรมพัฒนามูลนิธิ เพื่อสถาบันอาหาร</span>
                    </div>
                    <div class="progress">
                        @php
                            $percent_team_6  = ($entrepreneur_team_6*100)/70;
                        @endphp
                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_team_6}}%; height:6px;"> <span class="sr-only">{{$percent_team_6}}% Complete</span> </div>
                    </div>
                </div>
                <!-- /.info-box -->
            </div>
        </a>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-8 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์</small></h2>
                </div>
                {{-- <canvas id="bar-chart"></canvas> --}}
                <div id="bar-chart"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="m-t-0">{{number_format($cost)}} <i class="fa fa-money float-right"></i></h3>
                        <p class="text-muted">ค่าใช้จ่ายในการพัฒนาผู้ประกอบการทั้งหมด</p>
                        <div class="progress">
                            @php
                                $percent_cost = ($cost*100)/24124950;
                                $percent_product = ($product*100)/430;
                            @endphp
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{$percent_cost}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_cost}}%; height:6px;"> <span class="sr-only">85% Complete</span> </div>
                        </div>
                        <small>คิดเป็น {{number_format($percent_cost,2)}}%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="m-t-0">{{number_format($product)}} <i class="fa fa-archive float-right"></i></h3>
                        <p class="text-muted">จำนวนสินค้าทั้งหมด</p>
                        <div class="progress">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:{{$percent_product}}%; height:6px;"> <span class="sr-only">85% Complete</span> </div>
                        </div>
                        <small>คิดเป็น {{number_format($percent_product,2)}}%</small>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>การพัฒนาผู้ประกอบการในด้านต่าง ๆ</h4>
                </div>
                <div id="donut-1"></div>
                <table class="table m-t-15 m-b-0">
                    <tbody>
                        <tr>
                            <td>ด้านมาตรฐานผลิตภัณฑ์</td>
                            <td>{{$demand_1}}</td>
                            <td><i class="zmdi zmdi-stop text-primary"></i></td>
                        </tr>
                        <tr>
                            <td>ด้านการพัฒนาเทคโนโลยีการผลิต</td>
                            <td>{{$demand_2}}</td>
                            <td><i class="zmdi zmdi-stop text-warning"></i></td>
                        </tr>
                        <tr>
                            <td>ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์</td>
                            <td>{{$demand_3}}</td>
                            <td><i class="zmdi zmdi-stop text-success"></i></td>
                        </tr>
                        <tr>
                            <td>ด้านการสร้างแบรนด์การตลาด</td>
                            <td>{{$demand_4}}</td>
                            <td><i class="zmdi zmdi-stop text-danger"></i></td>
                        </tr>
                        <tr>
                            <td>ด้านอื่น ๆ</td>
                            <td>{{$demand_5}}<br><br><br><br><br><br></td>
                            <td><i class="zmdi zmdi-stop text-muted"></i></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>ประเภทผลิตภัณฑ์</h4>
                </div>
                <div id="donut-2"></div>
                <table class="table m-t-15 m-b-0">
                    <tbody>
                        <tr>
                            <td>กลุ่มผลิตภัณฑ์อาหาร และเครื่องดื่ม</td>
                            <td>{{$product_type_1}}</td>
                            <td><i class="zmdi zmdi-stop text-primary"></i></td>
                        </tr>
                        <tr>
                            <td>กลุ่มผลิตภัณฑ์สมุนไพร</td>
                            <td>{{$product_type_2}}</td>
                            <td><i class="zmdi zmdi-stop text-warning"></i></td>
                        </tr>
                        <tr>
                            <td>กลุ่มผลิตภัณฑ์เครื่องสำอาง</td>
                            <td>{{$product_type_3}}</td>
                            <td><i class="zmdi zmdi-stop text-success"></i></td>
                        </tr>
                        <tr>
                            <td>กลุ่มผลิตภัณฑ์เกษตรแปรรูป<br><br><br><br><br><br><br><br></td>
                            <td>{{$product_type_4}}</td>
                            <td><i class="zmdi zmdi-stop text-danger"></i></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>จำนวนผู้ประกอบในจังหวัด (ราย)</h4>
                </div>
                @php
                    $c = count($province);
                @endphp
                <div class="table-responsive">
                    <table id="example2" class="table table-bordered table-striped">
                {{-- <table class="table m-t-15 m-b-0"> --}}
                        <thead>
                            <tr>
                                <th>ชื่อจังหวัด</th>
                                <th>จำนวน</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 0; $i < $c; $i++)
                                <tr>
                                    <td>{{$province[$i]->province}}</td>
                                    <td>{{$province[$i]->total}}</td>
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">

{{-- </div>
<div class="row mt-3"> --}}
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card ">
            <div class="card-body">
                <div class="card-title">
                    <h4>ผู้ใช้งานรออนุมัติ</h4>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>อีเมล</th>
                            <th>หน่วยร่วม</th>
                            <th>สถานะ</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($user as $u)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$u['name']}}</td>
                                <td>{{$u['email']}}</td>
                                <td>{{$u['team']->team_name}}</td>
                                <td>
                                    @switch($u['approve_status'])
                                        @case('1')
                                            <span class="badge badge-info">รอการอนุมัติ</span>
                                            @break
                                        @case('2')
                                            <span class="badge badge-success">ใช้งาน</span>
                                            @break
                                        @case('3')
                                            <span class="badge badge-danger">ระงับการใช้งาน</span>
                                            @break
                                        @default
                                            <span class="badge badge-secondary">No Status</span>
                                    @endswitch
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/userapprove/{{$u['id_user']}}/2" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="อนุมัติ"><i class="fa fa-check"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
           </div>
       </div>
   </div>
</div>
<input type="hidden" name="demand_1" id="demand_1" value="{{$demand_1}}">
<input type="hidden" name="demand_2" id="demand_2" value="{{$demand_2}}">
<input type="hidden" name="demand_3" id="demand_3" value="{{$demand_3}}">
<input type="hidden" name="demand_4" id="demand_4" value="{{$demand_4}}">
<input type="hidden" name="demand_5" id="demand_5" value="{{$demand_5}}">

<input type="hidden" name="product_type_1" id="product_type_1" value="{{$product_type_1}}">
<input type="hidden" name="product_type_2" id="product_type_2" value="{{$product_type_2}}">
<input type="hidden" name="product_type_3" id="product_type_3" value="{{$product_type_3}}">
<input type="hidden" name="product_type_4" id="product_type_4" value="{{$product_type_4}}">

<input type="hidden" name="jan" id="jan" value="{{$jan}}">
<input type="hidden" name="feb" id="feb" value="{{$feb}}">
<input type="hidden" name="mar" id="mar" value="{{$mar}}">
<input type="hidden" name="apr" id="apr" value="{{$apr}}">
<input type="hidden" name="may" id="may" value="{{$may}}">
<input type="hidden" name="jun" id="jun" value="{{$jun}}">
<input type="hidden" name="jul" id="jul" value="{{$jul}}">
<input type="hidden" name="aug" id="aug" value="{{$aug}}">
<input type="hidden" name="sep" id="sep" value="{{$sep}}">
<input type="hidden" name="oct" id="oct" value="{{$oct}}">
<input type="hidden" name="nov" id="nov" value="{{$nov}}">
<input type="hidden" name="dec" id="dec" value="{{$dec}}">

<input type="hidden" name="sum_feb" id="sum_feb" value="{{$sum_feb}}">
<input type="hidden" name="sum_mar" id="sum_mar" value="{{$sum_mar}}">
<input type="hidden" name="sum_apr" id="sum_apr" value="{{$sum_apr}}">
<input type="hidden" name="sum_may" id="sum_may" value="{{$sum_may}}">
<input type="hidden" name="sum_jun" id="sum_jun" value="{{$sum_jun}}">
<input type="hidden" name="sum_jul" id="sum_jul" value="{{$sum_jul}}">
<input type="hidden" name="sum_aug" id="sum_aug" value="{{$sum_aug}}">
<input type="hidden" name="sum_sep" id="sum_sep" value="{{$sum_sep}}">
<input type="hidden" name="sum_oct" id="sum_oct" value="{{$sum_oct}}">
<input type="hidden" name="sum_nov" id="sum_nov" value="{{$sum_nov}}">
<input type="hidden" name="sum_dec" id="sum_dec" value="{{$sum_dec}}">
@endsection
@section('script')
    {{-- <!-- jQuery 3 -->
    <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    {{-- <!-- Chartjs JavaScript -->
    <script src="{{asset('dist/plugins/chartjs/chart.min.js')}}"></script>
    <script src="{{asset('dist/plugins/chartjs/chart-int.js')}}"></script> --}}

    <!-- Morris JavaScript -->
    <script src="{{asset('dist/plugins/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('dist/plugins/morris/morris.js')}}"></script>
    <script src="{{asset('dist/plugins/functions/morris-init.js')}}"></script>
    <!-- DataTable -->
    <script src="{{asset('dist/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dist/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : true,
            'scrollX': false,
            'dom': '<"top"fl>rt<"bottom"p><"clear">',
            'pagingType': "simple"
          })
        })
      </script>

      <script src="{{asset('dist/plugins/table-expo/filesaver.min.js')}}"></script>
      <script src="{{asset('dist/plugins/table-expo/xls.core.min.js')}}"></script>
      <script src="{{asset('dist/plugins/table-expo/tableexport.js')}}"></script>
    <script>

        // ======
        // Bar chart Start
        // ======
        Morris.Bar({
            element: 'bar-chart',
            data: [
                {x: 'ม.ค.', y: $('#jan').val(), z: 0},
                {x: 'ก.พ.', y: $('#feb').val(), z: $('#sum_feb').val()},
                {x: 'มี.ค.', y: $('#mar').val(), z: $('#sum_mar').val()},
                {x: 'เม.ย.', y: $('#apr').val(), z: $('#sum_apr').val()},
                {x: 'พ.ค.', y: $('#may').val(), z: $('#sum_may').val()},
                {x: 'มิ.ย.', y: $('#jun').val(), z: $('#sum_jun').val()},
                {x: 'ก.ค.', y: $('#jul').val(), z: $('#sum_jul').val()},
                {x: 'ส.ค.', y: $('#aug').val(), z: $('#sum_aug').val()},
                {x: 'ก.ย.', y: $('#sep').val(), z: $('#sum_sep').val()}
            ],
            xkey: 'x',
            ykeys: ['z','y'],
            labels: ['ยอดรวมของเดือนก่อนหน้า', 'ยอดประจำเดือน'],
            stacked: true,
            barColors: ['#7a92a3', '#0b62a4'],
            rangeSelectColor: '#222222',
        }).on('click', function(i, row){
            console.log(i, row);
            resize: true
        });
        // ======
        // Bar chart End
        // ======

        // // ======
        // // Pie chart
        // // ======
        // new Chart(document.getElementById("pie-chart"),{
        //     type:'pie',
        //     data:{
        //         labels:
        //         ['ด้านมาตรฐานผลิตภัณฑ์','ด้านการพัฒนาเทคโนโลยีการผลิต','ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์','ด้านการสร้างแบรนด์การตลาด','ด้านอื่น ๆ'],
        //         datasets:
        //             [{'label':'My First Dataset',
        //             data:
        //                 [$('#demand_1').val(),
        //                 $('#demand_2').val(),
        //                 $('#demand_3').val(),
        //                 $('#demand_4').val(),
        //                 $('#demand_5').val()],
        //             backgroundColor:
        //                 ['rgb(255, 99, 132)',
        //                 'rgb(75, 192, 192)',
        //                 'rgb(255, 205, 86)',
        //                 'rgb(201, 203, 207)',
        //                 'rgb(54, 162, 235)'],
        //         }]
        //     },
        //     options: {
        //         responsive: true
        //     }
        // });

        // ======
        // Donut Chart Starts
        // ======

        Morris.Donut({
            element: 'donut-1',
            data: [
                {value: $('#demand_1').val(), label: 'ด้านมาตรฐานผลิตภัณฑ์'},
                {value: $('#demand_2').val(), label: 'ด้านการพัฒนาเทคโนโลยีการผลิต'},
                {value: $('#demand_3').val(), label: 'ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์'},
                {value: $('#demand_4').val(), label: 'ด้านการสร้างแบรนด์การตลาด'},
                {value: $('#demand_5').val(), label: 'ด้านอื่น ๆ'}
            ],
            backgroundColor: '#fff',
            labelColor: '#404e67',
            colors: [
                '#ef476f',
                '#ffd166',
                '#06d6a0',
                '#118ab2',
                '#073b4c'
            ],
            formatter: function (x) { return x + " ราย"}
            });

        // ======
        // Donut chart End
        // ======

        // ======
        // Donut Chart Starts
        // ======

        Morris.Donut({
            element: 'donut-2',
            data: [
                {value: $('#product_type_1').val(), label: 'กลุ่มผลิตภัณฑ์อาหาร และเครื่องดื่ม'},
                {value: $('#product_type_2').val(), label: 'กลุ่มผลิตภัณฑ์สมุนไพร'},
                {value: $('#product_type_3').val(), label: 'กลุ่มผลิตภัณฑ์เครื่องสำอาง'},
                {value: $('#product_type_4').val(), label: 'กลุ่มผลิตภัณฑ์เกษตรแปรรูป'}
            ],
            backgroundColor: '#fff',
            labelColor: '#404e67',
            colors: [
                '#ff4558',
                '#ff7d4d',
                '#00a5a8',
                '#626e82'
            ],
            formatter: function (x) { return x + " ราย"}
            });

        // ======
        // Donut chart End
        // ======
    </script>
@endsection
