<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Voucher ::</title>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Favicon-->
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
<!-- Custom Css -->
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,600,700" rel="stylesheet">

<style>
    .text-kanit {font-family: 'Kanit', sans-serif;}
</style>
@yield('css')
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">        
        <div class="line"></div>
		<div class="line"></div>
		<div class="line"></div>
        <p>รอสักครู่</p>
        <div class="m-t-30"><img src="{{asset('assets/images/logo.png')}}" width="48" height="48" alt="Nexa"></div>
    </div>
</div>

<!-- Overlay For Sidebars -->
{{-- <div class="overlay"></div><!-- Search  -->
<div class="search-bar">
    <div class="search-icon"> <i class="material-icons">search</i> </div>
    <input type="text" placeholder="Explore Nexa...">
    <div class="close-search"> <i class="material-icons">close</i> </div>
</div> --}}

<!-- Top Bar -->
<nav class="navbar">
    <div class="col-12">
        
        <div class="navbar-header">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html">Voucher</a>
        </div>

        <ul class="nav navbar-nav navbar-left">
            <li><a href="javascript:void(0);" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">            
            <li><a>โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</a></li>
        </ul>
    </div>
</nav>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar"> 
    <!-- User Info -->
    <div class="user-info">
        <div class="image"> <img src="{{asset('assets/images/logo.png')}}" width="48" height="48" alt="User" /> </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">{{Auth::user()->name}}</div>
            <div class="email">{{Auth::user()->email}}</div>
            <div class="btn-group user-helper-dropdown"> <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"> keyboard_arrow_down </i>
                <ul class="dropdown-menu pull-right">
                    {{-- <li><a href="profile.html"><i class="material-icons">person</i>Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                    <li class="divider"></li> --}}
                    <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('logout') }}">
                        @csrf
                        <li><button type="submit"><i class="material-icons">input</i>Sign Out</a></button></li>
                    </form>
                    <li><a href=""><i class="material-icons">input</i>Sign Out</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info --> 
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="/dashboard"><i class="zmdi zmdi-home"></i><span>Dashboard</span> </a></li>
            <li><a href="/entrepreneur"><i class="zmdi zmdi-account-calendar"></i><span>ข้อมูลผู้ประกอบการ</span> </a></li>
            <li><a href="/export"><i class="zmdi zmdi-file-text"></i><span>รายงาน</span> </a></li>
            <li><a href="/report200"><i class="zmdi zmdi-book"></i><span>รายงานผลความก้าวหน้า</span> </a></li>
        </ul>
    </div>
    <!-- #Menu --> 
</aside>

<section class="content">
   <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                {{-- <h2>Stater Page
                <small class="text-muted">Welcome to Nexa Application</small>
                </h2> --}}
                @yield('header')
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                {{-- <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Extra</a></li>
                    <li class="breadcrumb-item active">Stater Page</li>
                </ul> --}}
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @yield('container')
        {{-- <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <h4>Stater page</h4>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>   
</section>
<!-- Jquery Core Js --> 
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 

<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->
@yield('script')
</body>
</html>