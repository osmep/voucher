<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Voucher ::</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

<!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}">
@yield('css')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="skin-blue sidebar-mini">
<div class="wrapper boxed-wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo blue-bg">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><img src="{{asset('dist/img/logo-sub-osmep.png')}}" alt=""></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><img src="{{asset('dist/img/logo-osmep.png')}}" alt=""></span> </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar blue-bg navbar-static-top">
      <!-- Sidebar toggle button-->
      <ul class="nav navbar-nav pull-left">
        <li><a class="sidebar-toggle" data-toggle="push-menu" href=""></a> </li>
      </ul>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu p-ph-res"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="{{asset('dist/img/user.png')}}" class="user-image" alt="User Image"> <span class="hidden-xs">{{Auth::user()->name}}</span> </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <div class="pull-left user-img"><img src="{{asset('dist/img/user.png')}}" class="img-responsive" alt="User"></div>
                <p class="text-left">{{Auth::user()->name}} <small>{{Auth::user()->email}}</small> </p>
                {{-- <div class="view-link text-left"><a href="#">View Profile</a> </div> --}}
              </li>
              <li role="separator" class="divider"></li>
              <li>
                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i> Logout
                  </a>
                  <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('logout') }}">
                    @csrf
                  </form>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="image text-center"><img src="{{asset('dist/img/user.png')}}" class="img-circle" alt="User Image"> </div>
        <div class="info">
          <p>{{Auth::user()->name}}</p>
          {{-- <a href="#"><i class="fa fa-cog"></i></a>
          <a href="#"><i class="fa fa-envelope-o"></i></a> --}}
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off"></i>
          </a>
          <form class="col-lg-12" id="logout-form" method="POST" action="{{ route('logout') }}">
            @csrf
          </form>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a></li>
        <li><a href="/entrepreneur"><i class="fa fa-address-card-o"></i><span>ข้อมูลผู้ประกอบการ</span> </a></li>
        <li><a href="/export"><i class="fa fa-file-zip-o"></i><span>ออกรายงาน</span> </a></li>
        <li><a href="/report200"><i class="fa fa-file-text-o"></i><span>รายงานผลความก้าวหน้า</span> </a></li>
        @if (Auth::user()->id_level == 1)
            <li><a href="/user"><i class="fa fa-user-circle"></i><span>ผู้ใช้งาน</span> </a></li>
            <li><a href="{{asset('voucher/คู่มือการใช้งานระบบ_เจ้าหน้าที่ สสว.pdf')}}" target="_blank"><i class="fa fa-book"></i><span>คู่มือการใช้งาน</span> </a></li>
        @else
        <li><a href="{{asset('voucher/คู่มือการใช้งานระบบ_หน่วยร่วมดำเนินการ.pdf')}}" target="_blank"><i class="fa fa-book"></i><span>คู่มือการใช้งาน</span> </a></li>
        @endif

      </ul>
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h1 class="text-black">@yield('header')</h1>
      {{-- <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="sub-bread"><i class="fa fa-angle-right"></i> Pages</li>
        <li><i class="fa fa-angle-right"></i> Blank Page</li>
      </ol> --}}
    </div>

    <!-- Main content -->
    <div class="content">
        @yield('container')
        {{-- <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        This is some text within a card block.
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    Copyright © 2021 www.osmepv30.com All rights reserved.</footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('dist/js/jquery.min.js')}}"></script>

<!-- v4.0.0-alpha.6 -->
<script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- template -->
<script src="{{asset('dist/js/niche.js')}}"></script>
@yield('script')
</body>
</html>
