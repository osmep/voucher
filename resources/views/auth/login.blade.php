<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Voucher :: โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

<!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.min.css')}}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-box-body">
            <div class="text-center"><img src="{{asset('dist/img/logo-osmep.png')}}" alt=""></div>
            <h5 class="text-center m-t-2 text-primary">โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</h5>
            <div class="login-box-msg mt-3">ลงชื่อเข้าใช้บัญชีของคุณ</div>
            <form action="{{ route('login') }}" method="post">
                @csrf
                <!-- Session Status -->
                <x-auth-session-status class="alert alert-success" :status="session('status')" />
                <!-- Validation Errors -->
                <x-auth-validation-errors class="text-danger text-sm text-left" :errors="$errors" />
                <div class="form-group has-feedback">
                    <input class="form-control sty1" id="email" type="email" name="email" placeholder="อีเมล" :value="old('email')" required>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control sty1" id="password" name="password" placeholder="รหัสผ่าน" required autocomplete="current-password">
                </div>
                {{-- <div>
                    <div class="col-xs-8">
                        <a href="pages-recover-password.html" class="pull-right"><i class="fa fa-lock"></i> ลืมรหัสผ่าน</a>
                    </div>
                </div> --}}
                <!-- /.col -->
                <div class="col-xs-4 m-t-1">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">เข้าสู่ระบบ</button>
                </div>
                <!-- /.col -->
            </form>

            <div class="m-t-2">คุณยังไม่มีบัญชีใช่หรือไม่ <a href="/register" class="text-center">สมัครสมาชิก</a></div>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('dist/js/jquery.min.js')}}"></script>

<!-- v4.0.0-alpha.6 -->
<script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- template -->
<script src="{{asset('dist/js/niche.js')}}"></script>
</body>
</html>
