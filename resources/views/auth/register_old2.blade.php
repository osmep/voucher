<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>:: Voucher :: Sign Up</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/authentication.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
<!-- Bootstrap Select Css -->
<link href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
</head>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <div class="logo"><img src="{{asset('assets/images/logo.png')}}" alt="Nexa"></div>
                        {{-- <h1>Voucher</h1> --}}
                        <p class="mt-3">โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</p>
                        {{-- <ul class="list-unstyled l-social">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>                            
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        </ul> --}}
                    </div>                        
                </div>
                <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('register') }}">
                    @csrf
                    <h5 class="title">สมัครสมาชิก</h5>
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="text-danger text-sm text-left" :errors="$errors" />
                    <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" id="name" name="name" :value="old('name')" required>
                                <label class="form-label">ชื่อ <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" id="email" name="email" :value="old('email')" required>
                                <label class="form-label">อีเมล <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" id="password" name="password" required autocomplete="new-password">
                                <label class="form-label">รหัสผ่านใหม่ <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                                <label class="form-label">ยืนยันรหัสผ่าน <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        <select class="form-control show-tick" name="id_team" required>
                            <option value="">-- กรุณาเลือกสังกัดทีมของท่าน --  <span class="text-danger">*</span></option>
                            <option value="1">สำนักงานส่งเสริมวิสาหกิจขนาดกลางและขนาดย่อม (สสว.)</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    <div>
                    <div class="col-lg-12 mt-3">                        
                        <button type="submit" class="btn btn-raised btn-primary waves-effect">สมัครสมาชิก</button> 
                        <a href="/login" class="btn btn-raised btn-default waves-effect">เข้าสู่ระบบ</a>                      
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Jquery Core Js --> 
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js --> 
<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->
</body>
</html>
