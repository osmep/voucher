<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Voucher :: Sign Up</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />

<!-- v4.0.0-alpha.6 -->
<link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.min.css')}}">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-box-body">
        <div class="text-center"><img src="{{asset('dist/img/logo-osmep.png')}}" alt=""></div>
        <h3 class="login-box-msg mt-3">สมัครสมาชิก</h3>
        <x-auth-validation-errors class="text-danger text-sm text-left" :errors="$errors" />
        <form action="{{ route('register') }}" method="post">
            @csrf
            <div class="form-group has-feedback">
                <input type="text" class="form-control sty1" id="name" name="name" :value="old('name')" placeholder="ชื่อผู้ใช้งาน" required>
            </div>
            <div class="form-group has-feedback">
                <input type="email" class="form-control sty1" id="email" name="email" :value="old('email')" placeholder="อีเมล" required>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control sty1" id="password" name="password" placeholder="รหัสผ่าน" required autocomplete="new-password">
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control sty1" id="password_confirmation" placeholder="ยืนยันรหัสผ่าน" name="password_confirmation" required>
            </div>
            <div class="form-group has-feedback">
                <select class="form-control sty1" name="id_team" required>
                    <option value="">กรุณาเลือกสังกัดทีมของท่าน</option>
                    @foreach ($team as $t)
                        <option value="{{$t['id_team']}}">{{$t['team_name']}}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <div class="col-xs-4 m-t-1">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">สมัครสมาชิก</button>
                </div>
            <!-- /.col -->
            </div>
        </form>
        <div class="m-t-2">หากคุณมีบัญชีอยู่แล้ว <a href="/" class="text-center">เข้าสู่ระบบ</a></div>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('dist/js/jquery.min.js')}}"></script>

<!-- v4.0.0-alpha.6 -->
<script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- template -->
<script src="{{asset('dist/js/niche.js')}}"></script>
</body>
</html>
