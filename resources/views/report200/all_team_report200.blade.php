@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
@endsection
@section('header')
    แบบรายงานผลความก้าวหน้า สสว.200
@endsection
@section('container')
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="card">
                <div class="card-body table-responsive">
                    <div class="card-title">
                        <h4>หน่วยร่วมดำเนินการ</h4>
                    </div>
                    <table class="table">
                        <tbody>
                            @foreach ($team as $t)
                                <tr>
                                    <td>{{$t['team_name']}}</td>
                                    <td>
                                        <a href="/report200team/{{$t['id_team']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}
@endsection
