@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
@endsection
@section('header')
    แบบรายงานผลความก้าวหน้า สสว.200
@endsection
@section('container')
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
    @endif
    @if (Auth::User()->id_level == 1)
        <div class="row">
            {{-- <div class="col-lg-6 col-md-12 col-sm-12">
                @foreach ($team as $t)
                    <div class="row">
                        <div class="col-12">
                            <a href="/report200team/{{$t['id_team']}}">
                                <div class="info-box">
                                    <span class="info-box-icon bg-aqua"><i class="fa fa-file-text-o"></i></span>
                                    <div class="info-box-content"> <span class="info-box-number">{{$t['team_name']}}</span> <span class="info-box-text">รายงาน สสว.200</span> </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </a>
                        </div>
                    </div>
                @endforeach
            </div> --}}
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <div class="card-title">
                            <h4>หน่วยร่วมดำเนินการ</h4>
                        </div>
                        <table class="table">
                            <tbody>
                                @foreach ($team as $t)
                                    <tr>
                                        <td>{{$t['team_name']}}</td>
                                        <td>
                                            <a href="/report200team/{{$t['id_team']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <div class="card-title">
                            <h4>แบบรายงานผลความก้าวหน้า สสว.200</h4>
                            หน่วยร่วมดำเนินการ : {{$team_name->team_name}}
                            @if ($report > 0)
                                <a href="/downloadallreport200/{{$team_name->id_team}}" class="btn btn-primary btn-sm mt-3"><i class="fa fa-download"></i> ดาวน์โหลดเอกสารทั้งหมด</a>
                            @else
                                <button class="btn btn-primary btn-sm mt-3" disabled><i class="fa fa-download"></i> ดาวน์โหลดเอกสารทั้งหมด</button>
                            @endif

                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ประจำเดือน</th>
                                    <th>เอกสารรายงาน สสว.200</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">มกราคม</th>
                                    <td>
                                        @if ($jan1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($jan1->id_team,0,1).'/'.$jan1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">กุมภาพันธ์</th>
                                    <td>
                                        @if ($feb1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($feb1->id_team,0,1).'/'.$feb1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">มีนาคม</th>
                                    <td>
                                        @if ($mar1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($mar1->id_team,0,1).'/'.$mar1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">เมษายน</th>
                                    <td>
                                        @if ($apr1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($apr1->id_team,0,1).'/'.$apr1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">พฤษภาคม</th>
                                    <td>
                                        @if ($may1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($may1->id_team,0,1).'/'.$may1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">มิถุนายน</th>
                                    <td>
                                        @if ($jun1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($jun1->id_team,0,1).'/'.$jun1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">กรกฎาคม</th>
                                    <td>
                                        @if ($jul1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($jul1->id_team,0,1).'/'.$jul1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">สิงหาคม</th>
                                    <td>
                                        @if ($aug1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <a href="{{asset('voucher/report200/'.substr($aug1->id_team,0,1).'/'.$aug1->report_200)}}" class="btn btn-link" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title"><h5>อัพโหลดแบบรายงานผลความก้าวหน้า สสว.200</h5></div>
                        {!! Form::open(['url'=>'/report200','files'=>true,'id'=>'form-report']) !!}
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="file">ประจำเดือน <span class="text-danger">*</span></label>
                                    <select class="form-control" name="report_month" required>
                                        <option value="">ประจำเดือน</option>
                                        <option value="1">มกราคม</option>
                                        <option value="2">กุมภาพันธ์</option>
                                        <option value="3">มีนาคม</option>
                                        <option value="4">เมษายน</option>
                                        <option value="5">พฤษภาคม</option>
                                        <option value="6">มิถุนายน</option>
                                        <option value="7">กรกฎาคม</option>
                                        <option value="8">สิงหาคม</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="report_200">อัพโหลดไฟล์ <span class="text-danger">*</span></label>
                                    <input type="file" id="report_200" name="report_200" accept=".xlsx" required>
                                </div>
                            </div>
                            <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <div class="card-title"><h5>แบบรายงานผลความก้าวหน้า สสว.200</h5></div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ประจำเดือน</th>
                                    <th>เอกสารรายงาน สสว.200</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">มกราคม</th>
                                    <td>
                                        @if ($jan1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>

                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($jan1->id_team,0,1).'/'.$jan1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $jan1->id_report_200],'id'=>'destroy_report_jan']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_jan').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">กุมภาพันธ์</th>
                                    <td>
                                        @if ($feb1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>

                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($feb1->id_team,0,1).'/'.$feb1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $feb1->id_report_200],'id'=>'destroy_report_feb']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_feb').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">มีนาคม</th>
                                    <td>
                                        @if ($mar1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($mar1->id_team,0,1).'/'.$mar1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $mar1->id_report_200],'id'=>'destroy_report_mar']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_mar').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">เมษายน</th>
                                    <td>
                                        @if ($apr1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($apr1->id_team,0,1).'/'.$apr1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $apr1->id_report_200],'id'=>'destroy_report_apr']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_apr').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">พฤษภาคม</th>
                                    <td>
                                        @if ($may1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($may1->id_team,0,1).'/'.$may1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $may1->id_report_200],'id'=>'destroy_report_may']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_may').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">มิถุนายน</th>
                                    <td>
                                        @if ($jun1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($jun1->id_team,0,1).'/'.$jun1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $jun1->id_report_200],'id'=>'destroy_report_jun']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_jun').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">กรกฎาคม</th>
                                    <td>
                                        @if ($jul1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($jul1->id_team,0,1).'/'.$jul1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $jul1->id_report_200],'id'=>'destroy_report_jul']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_jul').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">สิงหาคม</th>
                                    <td>
                                        @if ($aug1 == '')
                                            <button type="button" class="btn btn-link" disabled><i class="fa fa-file-text-o"></i></button>
                                        @else
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-link" data-toggle="dropdown"> <i class="fa fa-file-text-o"></i></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{asset('voucher/report200/'.substr($aug1->id_team,0,1).'/'.$aug1->report_200)}}" target="_blank"><i class="fa fa-file-o"></i> เอกสาร</a></li>
                                                    <li>
                                                        {{ Form::open(['route' => ['report200.destroy', $aug1->id_report_200],'id'=>'destroy_report_aug']) }}
                                                            <input type="hidden" name="_method" value="delete">
                                                        {{ Form::close() }}
                                                        {{-- <a href="/removereport"><i class="fa fa-trash-o"></i> ลบเอกสาร</a> --}}
                                                        <a href="" onclick="event.preventDefault(); document.getElementById('destroy_report_aug').submit();">
                                                            <i class="fa fa-trash-o"></i> ลบเอกสาร
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif


@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}
    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>
    <script>
        $('#form-report').validate();
    </script>
@endsection
