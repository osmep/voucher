@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('dist/plugins/datatables/css/dataTables.bootstrap.min.css')}}">
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('dist/plugins/tooltip/tooltip.css')}}">
    <!-- popover -->
    <link rel="stylesheet" href="{{asset('dist/plugins/popover/bootstrap-popover-x.css')}}"/>
@endsection
@section('header')
    <h2>จัดการผู้ใช้งาน</h2>
@endsection
@section('container')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
@if (Session::has('error'))
    <div class="alert alert-error alert-dismissible fade show" role="alert"> <strong><i class="fa fa-times-circle"></i></strong> {{Session::get('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                @if (isset($user_edit))
                    <div class="card-title">
                        <h4>แก้ไขผู้ใช้งาน</h4>
                    </div>
                    {!! Form::open(['route'=>['user.update', $user_edit->id_user],'method'=>'put', 'files'=>true,'id'=>'form_advisor']) !!}
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="name">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                                @if (old('name'))
                                    <input type="text" class="form-control" name="name" maxlength="150" value="{{old('name')}}" required>
                                @else
                                    <input type="text" class="form-control" name="name" maxlength="150" value="{{$user_edit->name}}" required>
                                @endif
                            </div>
                            <div class="form-group col-6">
                                <label for="email">อีเมล <span class="text-danger">*</span></label>
                                @if (old('email'))
                                    <input type="email" class="form-control" name="email" maxlength="150" value="{{old('email')}}" required>
                                @else
                                    <input type="email" class="form-control" name="email" maxlength="150" value="{{$user_edit->email}}" required>
                                @endif
                            </div>
                            {{-- <div class="form-group col-6">
                                <label for="password">รหัสผ่าน <span class="text-danger">*</span></label>
                                @if (old('password'))
                                    <input type="password" class="form-control" name="password" value="{{old('password')}}" required>
                                @else
                                    <input type="password" class="form-control" name="password" value="" required>
                                @endif
                            </div>
                            <div class="form-group col-6">
                                <label for="password_confirmation">ยืนยันรหัสผ่าน <span class="text-danger">*</span></label>
                                @if (old('confirm_password'))
                                    <input type="password" class="form-control" name="password_confirmation" value="{{old('confirm_password')}}" required>
                                @else
                                    <input type="password" class="form-control" name="password_confirmation" value="" required>
                                @endif
                            </div> --}}
                            <div class="form-group col-12">
                                <label for="advisor_profile">หน่วยร่วม  <span class="text-danger">*</span></label>
                                <select class="form-control show-tick" id="id_team" name="id_team" required>
                                    <option value="">กรุณาเลือกหน่วยร่วม</option>
                                    @foreach ($team as $t)
                                        @if ($t['id_team'] == $user_edit->id_team)
                                            <option value="{{$t['id_team']}}" selected>{{$t['team_name']}}</option>
                                        @else
                                            <option value="{{$t['id_team']}}">{{$t['team_name']}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_profile">ระดับผู้ใช้งาน  <span class="text-danger">*</span></label>
                                <select class="form-control show-tick" id="id_level" name="id_level" required>
                                    @switch($user_edit->id_level)
                                        @case('1')
                                            <option value="">กรุณาเลือกระดับผู้ใช้งาน</option>
                                            <option value="1" selected>ผู้ดูแลระบบ</option>
                                            <option value="2">ผู้ใช้งาน</option>
                                            @break
                                        @case('2')
                                            <option value="">กรุณาเลือกระดับผู้ใช้งาน</option>
                                            <option value="1">ผู้ดูแลระบบ</option>
                                            <option value="2" selected>ผู้ใช้งาน</option>
                                            @break
                                        @default
                                            <option value="">กรุณาเลือกระดับผู้ใช้งาน</option>
                                            <option value="1">ผู้ดูแลระบบ</option>
                                            <option value="2">ผู้ใช้งาน</option>
                                    @endswitch
                                </select>
                            </div>
                        </div>
                        <button id="btn_submit" class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                    {!! Form::close() !!}
                @else
                    <div class="card-title">
                        <h4>เพิ่มผู้ใช้งาน</h4>
                    </div>
                    <form id="form_advisor" action="/user" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="name">ชื่อผู้ใช้งาน <span class="text-danger">*</span></label>
                                @if (old('name'))
                                    <input type="text" class="form-control" name="name" maxlength="150" value="{{old('name')}}" required>
                                @else
                                    <input type="text" class="form-control" name="name" maxlength="150" required>
                                @endif
                            </div>
                            <div class="form-group col-6">
                                <label for="email">อีเมล <span class="text-danger">*</span></label>
                                @if (old('email'))
                                    <input type="email" class="form-control" name="email" maxlength="150" value="{{old('email')}}" required>
                                @else
                                    <input type="email" class="form-control" name="email" maxlength="150" required>
                                @endif
                            </div>
                            <div class="form-group col-6">
                                <label for="password">รหัสผ่าน <span class="text-danger">*</span></label>
                                @if (old('password'))
                                    <input type="password" class="form-control" name="password" value="{{old('password')}}" required>
                                @else
                                    <input type="password" class="form-control" name="password" required>
                                @endif
                            </div>
                            <div class="form-group col-6">
                                <label for="password_confirmation">ยืนยันรหัสผ่าน <span class="text-danger">*</span></label>
                                @if (old('confirm_password'))
                                    <input type="password" class="form-control" name="password_confirmation" value="{{old('confirm_password')}}" required>
                                @else
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                @endif
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_profile">หน่วยร่วม  <span class="text-danger">*</span></label>
                                <select class="form-control show-tick" id="id_team" name="id_team" required>
                                    <option value="">กรุณาเลือกหน่วยร่วม</option>
                                    @foreach ($team as $t)
                                        <option value="{{$t['id_team']}}">{{$t['team_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_profile">ระดับผู้ใช้งาน  <span class="text-danger">*</span></label>
                                <select class="form-control show-tick" id="id_level" name="id_level" required>
                                    <option value="">กรุณาเลือกระดับผู้ใช้งาน</option>
                                    <option value="1">ผู้ดูแลระบบ</option>
                                    <option value="2">ผู้ใช้งาน</option>
                                </select>
                            </div>
                        </div>
                        <button id="btn_submit" class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"><h4>ผู้ใช้งานทั้งหมด</h4></div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>อีเมล</th>
                            <th>หน่วยร่วม</th>
                            <th>ระดับผู้ใช้งาน</th>
                            <th>สถานะ</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($user as $u)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$u['name']}}</td>
                                <td>{{$u['email']}}</td>
                                <td>{{$u['team']->team_name}}</td>
                                <td>{{$u['level']->level_name}}</td>
                                <td>
                                    @switch($u['approve_status'])
                                        @case('1')
                                            <span class="badge badge-info">รอการอนุมัติ</span>
                                            @break
                                        @case('2')
                                            <span class="badge badge-success">ใช้งาน</span>
                                            @break
                                        @case('3')
                                            <span class="badge badge-danger">ระงับการใช้งาน</span>
                                            @break
                                        @default
                                            <span class="badge badge-secondary">No Status</span>
                                    @endswitch
                                </td>
                                <td>
                                    <div class="btn-group">
                                        @if (Auth::user()->id_user != $u['id_user'])
                                            @switch($u['approve_status'])
                                                @case('1')
                                                    <a href="/userapprove/{{$u['id_user']}}/2" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="อนุมัติ"><i class="fa fa-check"></i></a>
                                                    @break
                                                @case('2')
                                                    <a href="/userapprove/{{$u['id_user']}}/3" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="ระงับผู้ใช้งาน"><i class="fa fa-times"></i></a>
                                                    @break
                                                @case('3')
                                                    <a href="/userapprove/{{$u['id_user']}}/2" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="ยกเลิกการรับงับผู้ใช้งาน"><i class="fa fa-check"></i></a>
                                                    @break
                                                @default

                                            @endswitch
                                            <a href="/user/{{$u['id_user']}}/edit" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>
                                            {{ Form::open(['route' => ['user.destroy', $u['id_user']]]) }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button type="submit" class="js-mytooltip btn btn-default btn-sm" onclick="return confirm('คุณต้องการลบผู้ใช้งานใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></button>
                                            {{ Form::close() }}
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- DataTable -->
    <script src="{{asset('dist/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dist/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

    <!-- tooltip -->
    <script src="{{asset('dist/plugins/tooltip/tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/tooltip/script.js')}}"></script>
    <!-- popover -->
    <script src="{{asset('dist/plugins/popover/bootstrap-popover-x.js')}}"></script>

    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>

    <script>
        $('#form_advisor').validate();

        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
@endsection
