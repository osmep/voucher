@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('dist/plugins/datatables/css/dataTables.bootstrap.min.css')}}">
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('dist/plugins/tooltip/tooltip.css')}}">
    <!-- popover -->
    <link rel="stylesheet" href="{{asset('dist/plugins/popover/bootstrap-popover-x.css')}}"/>
@endsection
@section('header')
    <h2>จัดการที่ปรึกษา</h2>
    <a href="/voucher/create/{{$id_card}}"><span>กลับไปหน้าจัดการ Voucher</span></a>
@endsection
@section('container')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
@if (Session::has('error'))
    <div class="alert alert-error alert-dismissible fade show" role="alert"> <strong><i class="fa fa-times-circle"></i></strong> {{Session::get('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    </div>
@endif
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">

                @if (isset($advisor_edit))
                <div class="card-title">
                    <h4>แก้ไขที่ปรึกษา</h4>
                </div>
                    {!! Form::open(['route'=>['advisor.update', $advisor_edit->id_advisor],'method'=>'put', 'files'=>true,'id'=>'form_advisor']) !!}
                        <div class="row">
                            <div class="form-group col-12">
                                <label for="id_card">เลขบัตรประชาชนที่ปรึกษา <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="id_card" name="id_card" value="{{$advisor_edit->advisor_id_card}}" maxlength="13" minlength="13" onkeyup="keyup(this,event)" onkeypress="return Numbers(event)" disabled>
                                <input type="hidden" name="id_card" value="{{$advisor_edit->advisor_id_card}}">
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_sme_coach">เลขที่สมาชิก SME Coach (ถ้ามี)</label>
                                <input type="text" class="form-control" name="advisor_sme_coach" value="{{$advisor_edit->advisor_sme_coach}}">
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_name">ชื่อที่ปรึกษา <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="advisor_name" maxlength="150" value="{{$advisor_edit->advisor_name}}" required>
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_email">อีเมลที่ปรึกษา <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="advisor_email" maxlength="150" value="{{$advisor_edit->advisor_email}}" required>
                            </div>
                            <div class="form-group col-12">
                                <label class="form-label">หมายเลขโทรศัพท์ที่ปรึกษา <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="advisor_phone" minlength="10" maxlength="10" value="{{$advisor_edit->advisor_phone}}" required>
                            </div>
                            <div class="form-group col-12">
                                <label class="form-label">สาขาที่เชี่ยวชาญ <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="advisor_expert" maxlength="150" value="{{$advisor_edit->advisor_expert}}" required>
                            </div>
                            <div class="form-group col-12">
                                @if ($advisor_edit->advisor_profile == '')
                                    <label for="advisor_profile">ประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label>
                                    <input type="file" id="advisor_profile" name="advisor_profile" required>
                                @else
                                    <label class="form-label">ประวัติที่ปรึกษา</label><br>
                                    <a href="{{asset('voucher/advisor/'.substr($advisor_edit->id_advisor,0,1).'/'.$advisor_edit->advisor_profile)}}" target="_blank"><h3><i class="fa fa-file-pdf-o"></i></h3></a>
                                    <div class="demo-switch">
                                        <div class="switch">
                                            <label><input type="checkbox" id="edit_advisor_profile"><span class="lever"></span> ต้องการแก้ไขประวัติที่ปรึกษา</label>
                                        </div>
                                    </div>
                                    {{-- <label class="form-label text-secondary">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label> --}}
                                    <label for="report_200">อัพโหลดประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label>
                                    <input type="file" id="advisor_profile" name="advisor_profile" disabled>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="id_card_entrepreneur" value="{{$id_card}}">
                        <button id="btn_submit" class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
                    {!! Form::close() !!}
                @else
                <div class="card-title">
                    <h4>เพิ่มที่ปรึกษา</h4>
                </div>
                    <form id="form_advisor" action="/advisor" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="row">
                            <div class="form-group form-float col-12">
                                @if (old('id_card'))
                                    <label for="id_card">เลขบัตรประชาชนที่ปรึกษา <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{old('id_card')}}" id="id_card" name="id_card" maxlength="13" minlength="13" onkeyup="keyup(this,event)" onkeypress="return Numbers(event)" required>
                                @else
                                    <label for="id_card">เลขบัตรประชาชนที่ปรึกษา <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="id_card" name="id_card" maxlength="13" minlength="13" onkeyup="keyup(this,event)" onkeypress="return Numbers(event)" required>
                                @endif
                                <span class="error text-danger"></span>
                                <span class="success text-success"></span>
                                @error('id_card')
                                    <span id="error_controller" class="text-danger">เลขบัตรประชาชนของที่ปรึกษามีในระบบแล้ว</span>
                                @enderror
                            </div>
                            <div class="form-group form-float col-12">
                                <label for="advisor_sme_coach">เลขที่สมาชิก SME Coach (ถ้ามี)</label>
                                @if (old('advisor_sme_coach'))
                                    <input type="text" class="form-control" name="adviser_sme_coach" value="{{old('advisor_sme_coach')}}">
                                @else
                                    <input type="text" class="form-control" name="adviser_sme_coach">
                                @endif
                            </div>
                            <div class="form-group form-float col-12">
                                <label for="advisor_name">ชื่อที่ปรึกษา <span class="text-danger">*</span></label>
                                @if (old('advisor_name'))
                                    <input type="text" class="form-control" name="advisor_name" maxlength="150" value="{{old('advisor_name')}}" required>
                                @else
                                    <input type="text" class="form-control" name="advisor_name" maxlength="150" required>
                                @endif
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_email">อีเมลที่ปรึกษา <span class="text-danger">*</span></label>
                                @if (old('advisor_email'))
                                    <input type="email" class="form-control" name="advisor_email" maxlength="150" value="{{old('advisor_email')}}" required>
                                @else
                                    <input type="email" class="form-control" name="advisor_email" maxlength="150" required>
                                @endif
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_phone">หมายเลขโทรศัพท์ที่ปรึกษา <span class="text-danger">*</span></label>
                                @if (old('advisor_phone'))
                                    <input type="text" class="form-control" name="advisor_phone" minlength="10" maxlength="10" value="{{old('advisor_phone')}}" required>
                                @else
                                    <input type="text" class="form-control" name="advisor_phone" minlength="10" maxlength="10" required>
                                @endif
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_expert">สาขาที่เชี่ยวชาญ <span class="text-danger">*</span></label>
                                @if (old('advisor_expert'))
                                    <input type="text" class="form-control" name="advisor_expert" maxlength="150" value="{{old('advisor_expert')}}" required>
                                @else
                                    <input type="text" class="form-control" name="advisor_expert" maxlength="150" required>
                                @endif
                            </div>
                            <div class="form-group col-12">
                                <label for="advisor_profile">ประวัติที่ปรึกษา <span class="text-danger">*</span> (ไฟล์ .pdf)</label>
                                <input type="file" id="advisor_profile" name="advisor_profile" required>
                            </div>
                        </div>
                        <input type="hidden" name="id_card_entrepreneur" value="{{$id_card}}">
                        <button id="btn_submit" class="btn btn-raised btn-primary waves-effect" type="submit" disabled>บันทึกข้อมูล</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title"><h4>ที่ปรึกษาทั้งหมด</h4></div>
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>ประวัติ</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($advisor as $a)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$a['advisor_name']}}</td>
                                <td>
                                    <a href="{{asset('voucher/advisor/'.substr($a['id_advisor'],0,1).'/'.$a['advisor_profile'])}}" target="_blank"><h6><i class="fa fa-file-pdf-o"></i></h6></a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="js-mytooltip btn btn-default btn-sm" data-toggle="modal" data-target="#largeModal{{$a['id_advisor']}}" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></button>
                                        <a href="/advisor/{{$a['id_advisor']}}/edit/{{$id_card}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>
                                        {{ Form::open(['route' => ['advisor.destroy', $a['id_advisor']]]) }}
                                            <input type="hidden" name="_method" value="delete">
                                            <input type="hidden" name="id_card" value="{{$id_card}}">
                                            <button type="submit" class="js-mytooltip btn btn-default btn-sm" onclick="return confirm('คุณต้องการลบข้อมูลที่ปรึกษาใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></button>
                                        {{ Form::close() }}
                                        <!-- Large Size -->
                                        <div class="modal fade" id="largeModal{{$a['id_advisor']}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="largeModalLabel">รายละเอียดที่ปรึกษา</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <tr>
                                                                <td style="width: 30%"><strong>เลขบัตรประชาชนที่ปรึกษา</strong></td>
                                                                <td>{{$a['advisor_id_card']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 30%"><strong>เลขที่สมาชิก SME Coach</strong></td>
                                                                <td>{{$a['adviser_sme_coach']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 30%"><strong>ชื่อที่ปรึกษา</strong></td>
                                                                <td>{{$a['advisor_name']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>อีเมลที่ปรึกษา</strong></td>
                                                                <td>{{$a['advisor_email']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>หมายเลขโทรศัพท์ที่ปรึกษา</strong></td>
                                                                <td>{{$a['advisor_phone']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>สาขาที่เชี่ยวชาญ</strong></td>
                                                                <td>{{$a['advisor_expert']}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>ประวัติที่ปรึกษา</strong></td>
                                                                <td>
                                                                    <a href="{{asset('voucher/advisor/'.substr($a['id_advisor'],0,1).'/'.$a['advisor_profile'])}}" target="_blank"><h6><i class="fa fa-file-pdf-o"></i></h6></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php $i++; @endphp

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- DataTable -->
    <script src="{{asset('dist/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dist/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

    <!-- tooltip -->
    <script src="{{asset('dist/plugins/tooltip/tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/tooltip/script.js')}}"></script>
    <!-- popover -->
    <script src="{{asset('dist/plugins/popover/bootstrap-popover-x.js')}}"></script>

    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>

    <script>
        $('#form_advisor').validate();

        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
    </script>
    <script>
        //เช็คเลขบัตรประชาชน
        //ให้กรอกเฉพาะตัวเลข
        function Numbers(e){
            var keynum;
            var keychar;
            var numcheck;
            if(window.event) {// IE
                keynum = e.keyCode;
            }
            else if(e.which) {// Netscape/Firefox/Opera
                keynum = e.which;
            }
            if(keynum == 13 || keynum == 8 || typeof(keynum) == "undefined"){
                return true;
            }
            keychar= String.fromCharCode(keynum);
            numcheck = /^[0-9]$/;
            return numcheck.test(keychar);
        }
        //รับค่าจาก textbox
        function keyup(obj,e){
            var keynum;
            var keychar;
            var id = '';
            if(window.event) {// IE
                keynum = e.keyCode;
            }
            else if(e.which) {// Netscape/Firefox/Opera
                keynum = e.which;
            }
            keychar= String.fromCharCode(keynum);

            var tagInput = document.getElementsByTagName('input');

            if(obj.value.length == 0 && keynum == 8) prevObj.focus();

            if(obj.value.length == obj.getAttribute('maxlength')){
                for(i=0;i<=tagInput.length;i++){
                    if(tagInput[i].id.substring(0,7) == 'id_card'){
                        if(tagInput[i].value.length == tagInput[i].getAttribute('maxlength')){
                            id += tagInput[i].value;
                            if(tagInput[i].id == 'id_card') break;
                        }
                        else{
                            tagInput[i].focus();
                            return;
                        }
                    }
                }
                if(checkID(id))
                {
                    $('span.success').removeClass('true').text('เลขบัตรประชาชนถูกต้อง');
                    $('span.error').removeClass('true').text('');
                    $("#btn_submit").removeAttr("disabled");
                    $("#id_card_check").val(id);
                    $("#btn_checkdata").removeAttr("disabled");
                    $("#error_controller").hide();
                    nextObj.focus();
                    //alert('เลขบัตรประชาชนถูกต้อง');
                }
                else
                {
                    //alert('เลขบัตรประชาชนไม่ถูกต้อง');
                    $('span.error').removeClass('true').text('เลขบัตรประชาชนไม่ถูกต้อง');
                    $('span.success').removeClass('true').text('');
                    $("#btn_submit").attr("disabled", "disabled");
                    $("#error_controller").hide();
                    nextObj.focus();
                }
            }
        }

        //เช็คความถูกต้อง
        function checkID(id){
            if(id.length != 13)
                return false;
            for(i=0, sum=0; i < 12; i++)
                sum += parseFloat(id.charAt(i))*(13-i);
            if((11-sum%11)%10!=parseFloat(id.charAt(12)))
                return false;
            return true;
        }

        $(document).ready(function(){
            $("#btnclose").click(function () {
                $("#id_card").val('');
                $("#btn_submit").attr("disabled", "disabled");
                $('span.error').removeClass('true').text('');
                $('span.success').removeClass('true').text('');
            });
        });

        $("#edit_advisor_profile").click(function () {
            if($(this).is(":checked")) {
                $( "#advisor_profile" ).rules( "add", {
                    required: true,
                    messages: {
                    required: "กรุณากรอกข้อมูล",
                    }
                });
                $("#advisor_profile").removeAttr("disabled");
                $("#file").show();
            }
            else {
                $("#advisor_profile").attr("disabled", "disabled");
                $("#advisor_profile").prop('required',false);
                $("#file").hide();
            }
        });
    </script>
@endsection
