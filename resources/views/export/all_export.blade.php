@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
@endsection
@section('header')
    ส่งออกข้อมูล
@endsection
@section('container')
<div class="row">
    @if (Auth::user()->id_level == 1)
        @foreach ($team as $t)
            <div class="col-lg-6 col-xs-6 mt-3">
                <a href="/exportreport300/{{$t['id_team']}}">
                    <div class="card">
                        <div class="card-body">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-download"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-number">
                                    @if ($t['id_team'] == 1)
                                        ข้อมูลทั้งหมด
                                    @else
                                        {{$t['team_name']}}
                                    @endif
                                </span>
                                <span class="info-box-text">ข้อมูล สสว. 300</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>
            </div>
        @endforeach
    @else
        <div class="col-lg-6 col-xs-6 mt-3">
            <a href="/exportreport300/{{Auth::user()->id_team}}">
                <div class="card">
                    <div class="card-body">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-download"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-number">
                                ดาวน์โหลดรายงาน
                            </span>
                            <span class="info-box-text">ข้อมูล สสว. 300</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
        </div>
    @endif


</div>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}
@endsection
