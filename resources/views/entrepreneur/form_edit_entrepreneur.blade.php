@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    @php
        function DateThai($strDate)
        {
            $strYear = date("Y",strtotime($strDate))+543;
            $strMonth= date("n",strtotime($strDate));
            $strDay= date("j",strtotime($strDate));
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
            $strMonthThai=$strMonthCut[$strMonth];
            $today = today();
            if (date("Y-m-j",strtotime($strDate)) == date("Y-m-j",strtotime($today))) {
                return "วันนี้, $strHour:$strMinute";
            } else {
                return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
            }
        }
    @endphp
@endsection
@section('header')
    <h2>แก้ไขข้อมูลผู้ประกอบการ
    {{-- <small class="text-muted">Welcome</small> --}}
    </h2>
@endsection
@section('container')
<div class="info-box">
    <div class="row clearfix">
        <div class="col-lg-9">
            {!! Form::open(['route'=>['entrepreneur.update', $profile->id_card],'method'=>'put', 'files'=>true,'id'=>'form-entrepreneur']) !!}
                <h4 class="text-black">ข้อมูลเจ้าของกิจการ</h4>
                <div class="row">
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="id_card">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" value="{{$profile->id_card}}" disabled required>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="prefix">คำนำหน้าชื่อ <span class="text-danger">*</span></label>
                        @switch($profile->prefix)
                            @case('นาย')
                                <select class="form-control show-tick" id="prefix" name="prefix" required>
                                    <option value="">กรุณาเลือกคำนำหน้าชื่อ <span class="text-danger">*</span></option>
                                    <option value="นาย" selected>นาย</option>
                                    <option value="นาง">นาง</option>
                                    <option value="น.ส.">น.ส.</option>
                                </select>
                                @break
                            @case('นาง')
                                <select class="form-control show-tick" id="prefix" name="prefix" required>
                                    <option value="">กรุณาเลือกคำนำหน้าชื่อ <span class="text-danger">*</span></option>
                                    <option value="นาย">นาย</option>
                                    <option value="นาง" selected>นาง</option>
                                    <option value="น.ส.">น.ส.</option>
                                </select>
                                @break
                            @case('น.ส.')
                                <select class="form-control show-tick" id="prefix" name="prefix" required>
                                    <option value="">กรุณาเลือกคำนำหน้าชื่อ <span class="text-danger">*</span></option>
                                    <option value="นาย">นาย</option>
                                    <option value="นาง">นาง</option>
                                    <option value="น.ส." selected>น.ส.</option>
                                </select>
                                @break
                            @default
                                <select class="form-control show-tick" id="prefix" name="prefix" required>
                                    <option value="">กรุณาเลือกคำนำหน้าชื่อ <span class="text-danger">*</span></option>
                                    <option value="นาย">นาย</option>
                                    <option value="นาง">นาง</option>
                                    <option value="น.ส.">น.ส.</option>
                                </select>
                        @endswitch
                        <input type="hidden" name="field_prefix" id="field_prefix" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="name">ชื่อ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="name" name="name" maxlength="150" value="{{$profile->name}}" required>
                        <input type="hidden" name="field_name" id="field_name" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="lastname">นามสกุล <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="lastname" name="lastname" maxlength="150" value="{{$profile->lastname}}" required>
                        <input type="hidden" name="field_lastname" id="field_lastname" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="address">บ้านเลขที่ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address" name="address" maxlength="100" value="{{$profile->address}}" required>
                        <input type="hidden" name="field_address" id="field_address" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="moo">หมู่</label>
                        <input type="text" class="form-control" id="moo" name="moo" maxlength="100"value="{{$profile->moo}}">
                        <input type="hidden" name="field_moo" id="field_moo" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="soy">ซอย</label>
                        <input type="text" class="form-control" id="soy" name="soy" maxlength="100"value="{{$profile->soy}}">
                        <input type="hidden" name="field_soy" id="field_soy" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="road">ถนน</label>
                        <input type="text" class="form-control" id="road" name="road" maxlength="100"value="{{$profile->road}}">
                        <input type="hidden" name="field_road" id="field_road" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="province">จังหวัด <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="province" name="province" required>
                            <option value="">กรุณาเลือกจังหวัด</option>
                            @foreach ($province as $p)
                                @if ($profile->province == $p['PROVINCE_NAME'])
                                    <option value="{{$p['PROVINCE_ID']}}" selected>{{$p['PROVINCE_NAME']}}</option>
                                @else
                                    <option value="{{$p['PROVINCE_ID']}}">{{$p['PROVINCE_NAME']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="field_province" id="field_province" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="amphur">อำเภอ <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="amphur" name="amphur" required>
                            @foreach ($amphur as $a)
                                @if ($profile->amphur == $a['AMPHUR_NAME'])
                                    <option value="{{$a['AMPHUR_ID']}}" selected>{{$a['AMPHUR_NAME']}}</option>
                                @else
                                    <option value="{{$a['AMPHUR_ID']}}">{{$a['AMPHUR_NAME']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="field_amphur" id="field_amphur" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="district">ตำบล <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="district" name="district" required>
                            @foreach ($district as $d)
                                @if ($profile->district == $d['DISTRICT_NAME'])
                                    <option value="{{$d['DISTRICT_ID']}}" selected>{{$d['DISTRICT_NAME']}}</option>
                                @else
                                    <option value="{{$d['DISTRICT_ID']}}">{{$d['DISTRICT_NAME']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="field_district" id="field_district" value="">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="zipcode">รหัสไปรษณีย์ <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="zipcode" name="zipcode" required>
                            @foreach ($district as $z)
                                @if ($profile->zipcode == $z['DISTRICT_NAME'])
                                    <option value="{{$z['ZIPCODE']}}" selected>{{$z['ZIPCODE']}}</option>
                                @else
                                    <option value="{{$z['ZIPCODE']}}">{{$z['ZIPCODE']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="field_zipcode" id="field_zipcode" value="">
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="phone">โทรศัพท์ <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="phone" name="phone" minlength="10" maxlength="10" value="{{$profile->phone}}" required>
                        <input type="hidden" name="field_phone" id="field_phone" value="">
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="fax">โทรสาร</label>
                        <input type="number" class="form-control" id="fax" name="fax" value="{{$profile->fax}}">
                        <input type="hidden" name="field_fax" id="field_fax" value="">
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="email">อีเมล <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="{{$profile->email}}" required>
                        <input type="hidden" name="field_email" id="field_email" value="">
                    </div>
                </div>
                <h4 class="text-black">ข้อมูลสถานประกอบการ</h4>
                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="company_name">ชื่อสถานประกอบการ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name" value="{{$profile->company->company_name}}" required>
                        <input type="hidden" name="field_company_name" id="field_company_name" value="">
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="osmep_number">เลขสมาชิก สสว. <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="osmep_number" minlength="14" maxlength="14" name="osmep_number" value="{{$profile->company->osmep_number}}" required>
                        <input type="hidden" name="field_osmep_number" id="field_osmep_number" value="">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label>รูปแบบการดำเนินกิจการ <span class="text-danger">*</span></label>
                        @foreach ($business_type as $bt)
                            @if ($profile->business_type_select->id_business_type == $bt['id_business_type'])
                                <div class="radio">
                                    <label for="business_type_{{$bt['id_business_type']}}">
                                        <input name="id_business_type" type="radio" id="business_type_{{$bt['id_business_type']}}" value="{{$bt['id_business_type']}}" checked required />
                                        {{$bt['business_type']}}
                                    </label>
                                </div>
                            @else
                                <div class="radio">
                                    <label for="business_type_{{$bt['id_business_type']}}">
                                        <input name="id_business_type" type="radio" id="business_type_{{$bt['id_business_type']}}" value="{{$bt['id_business_type']}}" required />
                                        {{$bt['business_type']}}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                        @if ($profile->business_type_select->other_please_specify != '')
                            <div id="business_type_other_1" class="form-group col-lg-6 col-sm-12">
                                <label for="other_please_specify">โปรดระบุ</label>
                                <input type="text" class="form-control" id="other_please_specify" name="other_please_specify" value="{{$profile->business_type_select->other_please_specify}}">
                            </div>
                        @else
                            <div id="business_type_other_1" class="form-group col-lg-6 col-sm-12" style="display: none">
                                <label for="other_please_specify">โปรดระบุ</label>
                                <input type="text" class="form-control" id="other_please_specify" name="other_please_specify">
                            </div>
                        @endif
                        <input type="hidden" name="field_id_business_type" id="field_id_business_type" value="">
                        <input type="hidden" name="field_other_please_spacify" name="field_other_please_spacify" value="">
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="business_processing_time">ระยะเวลาดำเนินกิจการ <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="business_processing_time" name="business_processing_time" value="{{$profile->company->business_processing_time}}" required>
                        <input type="hidden" name="field_business_processing_time" id="field_business_processing_time" value="">
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="number_of_employment">จำนวนการจ้างงาน <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="number_of_employment" name="number_of_employment" value="{{$profile->company->number_of_employment}}" required>
                        <input type="hidden" name="field_number_of_employment" id="field_number_of_employment" value="">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="problems_in_business">ปัญหาและอุปสรรค ในการดำเนินกิจการ <span class="text-danger">*</span></label>
                        <textarea id="problems_in_business" name="problems_in_business" cols="30" rows="5" class="form-control no-resize" required>{{$profile->company->problems_in_business}}</textarea>
                        <input type="hidden" name="field_problems_in_business" id="field_problems_in_business" value="">
                    </div>
                </div>
                <h4 class="text-black">ความต้องการรับการพัฒนา <span class="text-danger">*</span></h4>
                <div class="row">
                    <div class="form-group col-lg-12 col-sm-12">
                        @foreach ($development as $d)
                            @if ($profile->development_demand->id_development == $d['id_development'])
                                <div class="radio">
                                    <label for="development_{{$d['id_development']}}">
                                        <input name="id_development" type="radio" id="development_{{$d['id_development']}}" value="{{$d['id_development']}}" checked required/>
                                        {{$d['development']}}
                                    </label>
                                </div>
                            @else
                                <div class="radio">
                                    <label for="development_{{$d['id_development']}}">
                                        <input name="id_development" type="radio" id="development_{{$d['id_development']}}" value="{{$d['id_development']}}" required/>
                                        {{$d['development']}}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                        @if ($profile->development_demand->development_other != '')
                            <div class="form-group col-lg-6 col-sm-12" id="development_other_1">
                                <label class="form-label">โปรดระบุ</label>
                                <input type="text" class="form-control" name="development_other" id="development_other" value="{{$profile->development_demand->development_other}}">
                            </div>
                        @else
                            <div class="form-group col-lg-6 col-sm-12" id="development_other_1" style="display: none">
                                <label class="form-label">โปรดระบุ</label>
                                <input type="text" class="form-control" name="development_other" id="development_other">
                            </div>
                        @endif
                        <input type="hidden" name="field_id_development" id="field_id_development" value="">
                        <input type="hidden" name="field_development_other" id="field_development_other">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="development_detail">รายละเอียดความต้องการรับการพัฒนา ระบุ <span class="text-danger">*</span></label>
                        <textarea id="development_detail" name="development_detail" cols="30" rows="5" class="form-control no-resize" required>{{$profile->development_demand->development_detail}}</textarea>
                        <input type="hidden" name="field_development_detail" id="field_development_detail" value="">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="id_sector">ภาคธุรกิจ <span class="text-danger">*</span></label>
                        @foreach ($sector as $s)
                            @if ($profile->company->id_sector == $s['id_sector'])
                                <div class="radio">
                                    <label for="id_sector_{{$s['id_sector']}}">
                                        <input type="radio" name="id_sector" id="id_sector_{{$s['id_sector']}}" value="{{$s['id_sector']}}" class="with-gap" checked required>
                                        {{$s['sector']}}
                                    </label>
                                </div>
                            @else
                                <div class="radio">
                                    <label for="id_sector_{{$s['id_sector']}}">
                                        <input type="radio" name="id_sector" id="id_sector_{{$s['id_sector']}}" value="{{$s['id_sector']}}" class="with-gap" required>
                                        {{$s['sector']}}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                        <input type="hidden" name="field_id_sector" id="field_id_sector" value="">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="tsic">เลข TSIC <span class="text-danger">*</span></label>
                        <select class="form-control" data-live-search="true" id="tsic" name="tsic" required>
                            <option value="">เลข TSIC</option>
                            @foreach ($tsic as $t)
                                @if ($profile->company->tsic == $t['tsic_code'])
                                    <option value="{{$t['tsic_code']}}" selected>{{$t['tsic_code'].' '.$t['tsic_name']}}</option>
                                @else
                                    <option value="{{$t['tsic_code']}}">{{$t['tsic_code'].' '.$t['tsic_name']}}</option>
                                @endif
                            @endforeach
                        </select>
                        <input type="hidden" name="field_tsic" id="field_tsic" value="">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        @if ($profile->document->document != '')
                            <label for="report_200">สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล (อายุไม่เกิน 6 เดือน) หรือสำเนาทะเบียนการค้า หรือสำเนาการจดทะเบียนจากหน่วยงานราชการอื่น (ถ้ามี)</label>
                            <a href="/voucher/document/{{substr($profile->document->id_document,0,1)}}/{{$profile->id_card}}/{{$profile->document->document}}" target="_blank"><h3><i class="fa fa-file-pdf-o"></i></h3></a>
                            <label><input type="checkbox" id="check_file_document"> ต้องการแก้ไขไฟล์</label><br>
                            <div id="file_document" style="display: none">
                                <input type="file" id="document" name="document" accept=".pdf">
                            </div>
                        @else
                            <label for="report_200">สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล (อายุไม่เกิน 6 เดือน) หรือสำเนาทะเบียนการค้า หรือสำเนาการจดทะเบียนจากหน่วยงานราชการอื่น (ถ้ามี)</label>
                            <input type="file" id="document" name="document" accept=".pdf">
                        @endif
                        <input type="hidden" name="field_document" id="field_document" value="สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        @if ($profile->document->map !='')
                            <label for="report_200">แผนที่แสดงที่ตั้งของกิจการ (ภาพวาด/ภาพถ่าย/ภาพจาก Application ด้านแผนที่ต่างๆ เช่น Google map) <span class="text-danger">*</span></label>
                            <img src="/voucher/document/{{substr($profile->id_document,0,1)}}/{{$profile->id_card}}/{{$profile->document->map}}" class="img-thumbnail" alt="" srcset=""><br>
                            {{-- <a href="/voucher/document/{{substr($profile->id_document,0,1)}}/{{$profile->id_card}}/{{$profile->document->map}}" target="_blank"><h3><i class="fa fa-file-pdf-o"></i></h3></a> --}}
                            <label><input type="checkbox" id="check_file_map"> ต้องการแก้ไขไฟล์</label><br>
                            <div id="file_map" style="display: none">
                                <input type="file" id="map" name="map" accept=".jpg, .png">
                            </div>
                        @else
                            <label for="report_200">แผนที่แสดงที่ตั้งของกิจการ (ภาพวาด/ภาพถ่าย/ภาพจาก Application ด้านแผนที่ต่างๆ เช่น Google map) <span class="text-danger">*</span></label>
                            <input type="file" id="map" name="map" accept=".jpg, .png" required>
                        @endif
                        <input type="hidden" name="field_map" id="field_map" value="แผนที่แสดงที่ตั้งของกิจการ">
                    </div>
                </div>
                <button class="btn btn-raised btn-primary waves-effect" type="submit">บันทึกข้อมูล</button>
            {!! Form::close() !!}
        </div>
        <div class="col-lg-3">
            <h4 class="text-black">ประวัติการแก้ไข</h4>
            @foreach ($edit_log as $el)

            @endforeach
            @forelse ($edit_log as $el)
                <div class="box box-widget">
                    <div class="box-footer box-comments">
                        <span class="username"> {{$el['field_name']}}
                            <span class="text-muted pull-right">
                                {{DateThai($el['created_at'])}}
                            </span>
                        </span>
                        <dl>
                            <dt>รายละเอียดการแก้ไข</dt>
                            <dd>
                                @switch($el['field_name'])
                                    @case('สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล')
                                        มีการเปลี่ยนแปลงไฟล์สำเนาหนังสือรับรองการจดทะเบียนนิติบุคคล<br>
                                        @break
                                    @case('แผนที่แสดงที่ตั้งของกิจการ')
                                        มีการเปลี่ยนแปลงไฟล์แผนที่แสดงที่ตั้งของกิจการ<br>
                                        @break
                                    @default
                                        เปลี่ยน <b>{{$el['text_old']}}</b> เป็น <b>{{$el['text_new']}}</b><br>
                                @endswitch
                            </dd>
                            <dt>แก้ไขโดย</dt>
                            <dd>
                                {{$el['user']->name}}
                            </dd>
                        </dl>
                    </div>
                </div>
                <br>
            @empty
                <div class="box box-widget">
                    <div class="box-footer box-comments">
                        <span class="username">ไม่พบประวัติการแก้ไข</span>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
</div>
@endsection
@section('script')
    {{-- <!-- jQuery 3 -->
    <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>

    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="{{asset('js/edit_log.js')}}"></script>
    <script>
        $('#form-entrepreneur').validate();

        $('#tsic').select2();

        $('#province').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        console.log(select);
                        $("#amphur").removeAttr("disabled");
                        $('#amphur').html(result);

                    }
                })
            }
        });
        $('#amphur').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile2')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        $("#district").removeAttr("disabled");
                        $('#district').html(result);
                    }
                })
            }
        });
        $('#district').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile3')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        $("#zipcode").removeAttr("disabled");
                        $('#zipcode').html(result);
                    }
                })
            }
        });

        $("#business_type_1").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_2").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_3").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_4").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_5").click(function () {
            $("#business_type_other_1").show();
            $( "#business_type_other" ).rules( "add", {
                required: true,
                messages: {
                required: "กรุณากรอกข้อมูล",
                }
            });
        });

        $("#development_1").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_2").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_3").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_4").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_5").click(function () {
            $("#development_other_1").show();
            $( "#development_other" ).rules( "add", {
                required: true,
                messages: {
                required: "กรุณากรอกข้อมูล",
                }
            });
        });

        $("#check_file_document").click(function () {
            if($(this).is(":checked")) {
                $("#file_document").show();
                $( "#document" ).rules( "add", {
                    required: true,
                    messages: {
                    required: "กรุณากรอกข้อมูล",
                    }
                });
                $("#document").removeAttr("disabled");
            }
            else {
                $("#file_document").hide();
                $("#document").attr("disabled", "disabled");
                $("#document").prop('required',false);
                $("#document").val("");
                $("#field_document").val("");
            }
        });

        $("#check_file_map").click(function () {
            if($(this).is(":checked")) {
                $("#file_map").show();
                $( "#map" ).rules( "add", {
                    required: true,
                    messages: {
                    required: "กรุณากรอกข้อมูล",
                    }
                });
                $("#map").removeAttr("disabled");
            }
            else {
                $("#file_map").hide();
                $("#map").attr("disabled", "disabled");
                $("#map").prop('required',false);
                $("#map").val("");
                $("#field_map").val("");
            }
        });
    </script>
@endsection
