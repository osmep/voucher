@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}

    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('dist/plugins/datatables/css/dataTables.bootstrap.min.css')}}">
    <!-- tooltip -->
    <link rel="stylesheet" href="{{asset('dist/plugins/tooltip/tooltip.css')}}">
    <!-- popover -->
    <link rel="stylesheet" href="{{asset('dist/plugins/popover/bootstrap-popover-x.css')}}"/>
@endsection
@section('header')
    ข้อมูลผู้ประกอบการ
@endsection
@section('container')
@if (Auth::user()->id_level == 1)
    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneur">
                <div class="info-box"> <span class="info-box-icon bg-primary text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_all)}}</span> <span class="info-box-text">ผู้ประกอบการทั้งหมด</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneurteam/2">
                <div class="info-box"> <span class="info-box-icon bg-secondary text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_2)}}</span> <span class="info-box-text">สำนักงานส่งเสริมเศรษฐกิจสร้างสรรค์ (องค์การมหาชน)</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneurteam/3">
                <div class="info-box"> <span class="info-box-icon bg-success text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_3)}}</span> <span class="info-box-text">บริษัท ห้องปฏิบัติการกลาง (ประเทศไทย) จำกัด</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneurteam/4">
                <div class="info-box"> <span class="info-box-icon bg-danger text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_4)}}</span> <span class="info-box-text">มหาวิทยาลัยเชียงใหม่</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneurteam/5">
                <div class="info-box"> <span class="info-box-icon bg-warning text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_5)}}</span> <span class="info-box-text">สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <div class="col-lg-4 col-xs-6">
            <a href="/entrepreneurteam/6">
                <div class="info-box"> <span class="info-box-icon bg-info text-white"><i class="ti-stats-up"></i></span>
                    <div class="info-box-content"> <span class="info-box-number">{{number_format($entrepreneur_team_6)}}</span> <span class="info-box-text">อุตสาหกรรมพัฒนามูลนิธิ เพื่อสถาบันอาหาร</span> </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
    </div>
@endif
<div class="info-box">
    @if (session('id_card')!=null)
        <div class="alert alert-warning alert-dismissible fade show" role="alert"> <strong><i class="fa fa-warning"></i></strong> เลขบัตรประชาชน {{session('id_card')}} นี้มีในระบบแล้ว
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert"> <strong><i class="fa fa-check-circle"></i></strong> {{Session::get('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
    @endif
    @if (Auth::user()->id_level == 2)
        <button class="btn btn-primary" data-toggle="popover-x" data-target="#myPopover20d" data-placement="right right-bottom"><i class="fa fa-plus-circle"></i> เพิ่มข้อมูลผู้ประกอบการ</button>
    @endif
    <!-- PopoverX content -->
    <div id="myPopover20d" class="popover popover-x popover-default">
        <form id="form_advanced_validation" action="/checkidcard" method="POST">
            @csrf
            <div class="arrow"></div>
            <h3 class="popover-header popover-title"><span class="close pull-right" data-dismiss="popover-x">&times;</span>ตรวจสอบเลขบัตรประชาชน</h3>
            <div class="popover-body popover-content">
                <div class="form-group form-float col-12">
                    <div class="form-line">
                        <input type="text" class="form-control" id="id_card" name="id_card" maxlength="13" minlength="13" placeholder="กรุณากรอกเลขบัตรประชาชน" onkeyup="keyup(this,event)" onkeypress="return Numbers(event)" required>
                    </div>
                    <span class="error text-danger"></span>
                    <span class="success text-success"></span>
                </div>
            </div>
            <div class="popover-footer">
                <button id="btn_submit" type="submit" class="btn btn-sm btn-primary" disabled>ตรวจสอบ</button>
            </div>
        </form>
    </div>
    {{-- <form action="/downloadreportword" method="post">
        @csrf
        <input type="hidden" name="select_id" value="">
        <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> ดาวน์โหลดรายงานที่เลือก</button>
    </form> --}}
    <h4 class="text-black mt-3">แสดงข้อมูลผู้ประกอบการทั้งหมด</h4>
    <form action="/downloadreportword" method="post" id="form-downloadreport">
        @csrf
        <div class="form-group">
            <button type="submit" class="js-mytooltip btn btn-default"><i class="fa fa-download"></i> ดาวน์โหลดรายงานที่เลือก</button>
            <!-- Button trigger modal -->
            @if (Auth::user()->id_level == 1)
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-download"></i> ดาวน์โหลดรายงานทั้งหมด
                </button>
            @else
                <a onclick="submitformteamdownloadreportwordall()" class="btn btn-default"><i class="fa fa-download"></i> ดาวน์โหลดรายงานทั้งหมด</a>
            @endif
        </div>
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
        @endforeach
        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        @if (Auth::user()->id_level == 1)
                            <th>หน่วยร่วม</th>
                        @endif
                        <th>เลขบัตรประชาชน</th>
                        <th>ชื่อ-นามสกุล</th>
                        <th>ชื่อสถานประกอบการ</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=1;
                    @endphp
                    @foreach ($profile as $p)
                        <tr>
                            <td>
                                <div class="checkbox">
                                    <label for="checkboxidcard{{$p['id_card']}}">
                                        <input name="id_card[]" type="checkbox" id="checkboxidcard{{$p['id_card']}}" value="{{$p['id_card']}}">
                                    </label>
                                </div>
                            </td>
                            <td>{{$i}}</td>
                            @if (Auth::user()->id_level == 1)
                                <td>{{$p['team']->team_name}}</td>
                            @endif
                            <td>{{$p['id_card']}}</td>
                            <td>{{$p['prefix'].$p['name'].' '.$p['lastname']}}</td>
                            <td>{{$p['company']->company_name}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="/entrepreneur/{{$p['id_card']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></a>
                                    <a href="/entrepreneur/{{$p['id_card']}}/edit" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="แก้ไขข้อมูล"><i class="fa fa-edit"></i></a>
                                    {{-- {{ Form::open(['route' => ['entrepreneur.destroy', $p['id_card'],'id'=>'deleteentrepreneur']]) }}
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="js-mytooltip btn btn-default btn-sm" onclick="return confirm('คุณต้องการลบข้อมูลผู้ประกอบการใช่หรือไม่')" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></button>
                                    {{ Form::close() }} --}}
                                    <a href="/entrepreneur/{{$p['id_card']}}/destroy" onclick="return confirm('คุณต้องการลบข้อมูลผู้ประกอบการใช่หรือไม่')" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="ลบข้อมูล"><i class="fa fa-trash-o"></i></a>
                                    <a href="/voucher/create/{{$p['id_card']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="จัดการ Voucher"><i class="fa fa-ticket"></i></a>
                                    <a href="/product/create/{{$p['id_card']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="จัดการสินค้า"><i class="fa fa-archive"></i></a>
                                    <a href="/exportword/{{$p['id_card']}}" class="js-mytooltip btn btn-default btn-sm" data-mytooltip-custom-class="align-center" data-mytooltip-content="รายงานการพัฒนาผู้ประกอบการตามแพ็คเกจ"><i class="fa fa-file-word-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </form>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ดาวน์โหลดรายงานการพัฒนาผู้ประกอบการตามแพ็คเกจ</h5>
            <a class="btn-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
        </div>
        <div class="modal-body">
            <form action="downloadreportwordall" method="POST">
                @csrf
                <div class="form-group">
                    <label for="data">เลือกข้อมูลที่ต้องการดาวน์โหลดรายงาน</label>
                    <select name="data" id="data" class="form-control" required>
                        <option value="">กรุณาเลือกข้อมูลที่ต้องการ</option>
                        <option value="1">ทั้งหมด</option>
                        <option value="2">สำนักงานส่งเสริมเศรษฐกิจสร้างสรรค์ (องค์การมหาชน)</option>
                        <option value="3">บริษัท ห้องปฏิบัติการกลาง (ประเทศไทย) จำกัด</option>
                        <option value="4">มหาวิทยาลัยเชียงใหม่</option>
                        <option value="5">สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย</option>
                        <option value="6">อุตสาหกรรมพัฒนามูลนิธิ เพื่อสถาบันอาหาร</option>
                    </select>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
            <button type="submit" class="btn btn-primary">ดาวน์โหลด</button>
            </form>
        </div>
    </div>
    </div>
</div>
<form id="formteam-downloadreportwordall" action="downloadreportwordall" method="post">
    @csrf
    <input type="hidden" name="data" value="{{Auth::user()->id_team}}">
</form>
@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- DataTable -->
    <script src="{{asset('dist/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dist/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
    <!-- tooltip -->
    <script src="{{asset('dist/plugins/tooltip/tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/tooltip/script.js')}}"></script>
    <!-- popover -->
    <script src="{{asset('dist/plugins/popover/bootstrap-popover-x.js')}}"></script>
    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script>
        //เช็คเลขบัตรประชาชน
        //ให้กรอกเฉพาะตัวเลข
        function Numbers(e){
            var keynum;
            var keychar;
            var numcheck;
            if(window.event) {// IE
                keynum = e.keyCode;
            }
            else if(e.which) {// Netscape/Firefox/Opera
                keynum = e.which;
            }
            if(keynum == 13 || keynum == 8 || typeof(keynum) == "undefined"){
                return true;
            }
            keychar= String.fromCharCode(keynum);
            numcheck = /^[0-9]$/;
            return numcheck.test(keychar);
        }
        //รับค่าจาก textbox
        function keyup(obj,e){
            var keynum;
            var keychar;
            var id = '';
            if(window.event) {// IE
                keynum = e.keyCode;
            }
            else if(e.which) {// Netscape/Firefox/Opera
                keynum = e.which;
            }
            keychar= String.fromCharCode(keynum);

            var tagInput = document.getElementsByTagName('input');

            if(obj.value.length == 0 && keynum == 8) prevObj.focus();

            if(obj.value.length == obj.getAttribute('maxlength')){
                for(i=0;i<=tagInput.length;i++){
                    if(tagInput[i].id.substring(0,7) == 'id_card'){
                        if(tagInput[i].value.length == tagInput[i].getAttribute('maxlength')){
                            id += tagInput[i].value;
                            if(tagInput[i].id == 'id_card') break;
                        }
                        else{
                            tagInput[i].focus();
                            return;
                        }
                    }
                }
                if(checkID(id))
                {
                    $('span.success').removeClass('true').text('เลขบัตรประชาชนถูกต้อง');
                    $('span.error').removeClass('true').text('');
                    $("#btn_submit").removeAttr("disabled");
                    $("#id_card_check").val(id);
                    $("#btn_checkdata").removeAttr("disabled");
                    nextObj.focus();
                    //alert('เลขบัตรประชาชนถูกต้อง');
                }
                else
                {
                    //alert('เลขบัตรประชาชนไม่ถูกต้อง');
                    $('span.error').removeClass('true').text('เลขบัตรประชาชนไม่ถูกต้อง');
                    $('span.success').removeClass('true').text('');
                    $("#btn_submit").attr("disabled", "disabled");
                    nextObj.focus();
                }
            }
        }

        //เช็คความถูกต้อง
        function checkID(id){
            if(id.length != 13)
                return false;
            for(i=0, sum=0; i < 12; i++)
                sum += parseFloat(id.charAt(i))*(13-i);
            if((11-sum%11)%10!=parseFloat(id.charAt(12)))
                return false;
            return true;
        }

        $(document).ready(function(){
            $("#btnclose").click(function () {
                $("#id_card").val('');
                $("#btn_submit").attr("disabled", "disabled");
                $('span.error').removeClass('true').text('');
                $('span.success').removeClass('true').text('');
            });


        });

        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
        })

        function submitformteamdownloadreportwordall() {
            document.getElementById("formteam-downloadreportwordall").submit();
        }
    </script>
@endsection
