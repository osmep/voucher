@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
    <!--cubeportfolio-->
    <link type="text/css" rel="stylesheet" href="{{asset('dist/plugins/cubeportfolio/css/cubeportfolio.min.css')}}">
    @php
        function DateThai($strDate)
        {
            $strYear = date("Y",strtotime($strDate))+543;
            $strMonth= date("n",strtotime($strDate));
            $strDay= date("j",strtotime($strDate));
            $strHour= date("H",strtotime($strDate));
            $strMinute= date("i",strtotime($strDate));
            $strSeconds= date("s",strtotime($strDate));
            $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
            $strMonthThai=$strMonthCut[$strMonth];
            return "$strDay $strMonthThai $strYear";
        }
    @endphp
    <style>
        p.overflow-hidden {
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .pagebreak {
            page-break-before: always;
        } /* page-break-after works, as well */
    </style>
@endsection
@section('header')
    แสดงข้อมูลผู้ประกอบการ
@endsection
@section('container')
    <div class="row no-print">
        <div class="col-lg-12 col-md-12 col-sm-12 text-right">
            <button onclick="myFunction()" class="btn btn-light"><i class="fa fa-print"></i> พิมพ์</button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 mt-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title"><h4>ข้อมูลผู้ประกอบการ</h4></div>

                    <table class="table">
                        <tr>
                            <td style="width: 30%"><strong>เลขบัตรประชาชน</strong></td>
                            <td>{{$profile->id_card}}</td>
                        </tr>
                        <tr>
                            <td><strong>ชื่อ-นามสกุล</strong></td>
                            <td>{{$profile->prefix.$profile->name.' '.$profile->lastname}}</td>
                        </tr>
                        <tr>
                            <td><strong>ที่อยู่</strong></td>
                            <td>
                                <p>
                                    {{$profile->address}}
                                    @if ($profile->moo != '')
                                        หมู่ {{$profile->moo}}
                                    @endif
                                    @if ($profile->soy != '')
                                        ซ.{{$profile->soy}}
                                    @endif
                                    @if ($profile->road != '')
                                        ถ.{{$profile->road}}
                                    @endif
                                    ต.{{$profile->district}} อ.{{$profile->amphur}} จ.{{$profile->province}} {{$profile->zipcode}}
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>โทรศัพท์</strong></td>
                            <td>{{$profile->phone}}</td>
                        </tr>
                        <tr>
                            <td><strong>โทรสาร</strong></td>
                            <td>{{$profile->fax}}</td>
                        </tr>
                        <tr>
                            <td><strong>อีเมล</strong></td>
                            <td>{{$profile->email}}</td>
                        </tr>
                        <tr>
                            <td><strong>ชื่อสถานประกอบการ</strong></td>
                            <td>{{$profile->company->company_name}}</td>
                        </tr>
                        <tr>
                            <td><strong>รหัสสมาชิก สสว.</strong></td>
                            <td>{{$profile->company->osmep_number}}</td>
                        </tr>
                        <tr>
                            <td><strong>รูปแบบการดำเนินกิจการ</strong></td>
                            <td>
                                @if ($profile->business_type_select->id_business_type == 5)
                                    {{$profile->business_type_select->other_please_specify}}
                                @else
                                    {{$profile->business_type_select->business_type->business_type}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><strong>ระยะเวลาดำเนินกิจการ</strong></td>
                            <td>{{$profile->company->business_processing_time}}</td>
                        </tr>
                        <tr>
                            <td><strong>จำนวนการจ้างงาน</strong></td>
                            <td>{{$profile->company->number_of_employment}}</td>
                        </tr>
                        <tr>
                            <td><strong>ปัญหาและอุปสรรค ในการดำเนินกิจการ</strong></td>
                            <td>{{$profile->company->problems_in_business}}</td>
                        </tr>
                        <tr>
                            <td><strong>ความต้องการรับการพัฒนา</strong></td>
                            <td>
                                @if ($profile->development_demand->id_development == 5)
                                    {{$profile->development_demand->development_other}}
                                @else
                                    {{$profile->development_demand->development->development}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><strong>รายละเอียดความต้องการรับการพัฒนา</strong></td>
                            <td>{{$profile->development_demand->development_detail}}</td>
                        </tr>
                        <tr>
                            <td><strong>สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล</strong></td>
                            <td>
                                @if ($profile->document->document == '')
                                    ไม่มี
                                @else
                                    <a href="/voucher/document/{{substr($profile->document->id_document,0,1)}}/{{$profile->id_card}}/{{$profile->document->document}}" target="_blank"><h3><i class="fa fa-file-pdf-o"></i></h3></a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td><strong>แผนที่แสดงที่ตั้งของกิจการ</strong></td>
                            <td>
                                <img src="/voucher/document/{{substr($profile->id_document,0,1)}}/{{$profile->id_card}}/{{$profile->document->map}}" class="img-thumbnail" alt="" srcset="">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 mt-3">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h4>การใช้งาน Voucher</h4>
                        <span class="text-right">ค่าใช้จ่ายที่ใช้ไปทั้งสิ้น <span class="text-primary">{{number_format($voucher_cost)}}</span> บาท</span>
                    </div>
                    @foreach ($voucher_register as $vr)
                        <div class="sl-item sl-primary">
                            <div class="sl-content">
                                <h5>
                                    <span class="badge badge-primary">
                                        @if ($vr['voucher_date'] == $vr['voucher_date_stop'])
                                            {{DateThai($vr['voucher_date'])}}
                                        @else
                                            {{DateThai($vr['voucher_date'])}} - {{DateThai($vr['voucher_date_stop'])}}
                                        @endif
                                    </span>
                                    @if ($vr['id_advisor'] == '')
                                        <span class="badge badge-warning">การพัฒนาผลิตภัณฑ์</span>
                                    @else
                                        <span class="badge badge-success">การให้คำปรึกษา</span>
                                    @endif
                                </h5>
                                <p class="overflow-hidden">
                                    <span class="text-warning">ค่าใช้จ่าย {{number_format($vr['voucher_cost'])}} บาท</span><br>
                                    {{$vr['voucher_topic']}} <br>
                                    {{$vr['voucher_detail']}} <br>
                                    <a href="" data-toggle="modal" data-target="#defaultModal{{$vr['id_voucher_register']}}">เพิ่มเติม</a>
                                </p>
                            </div>
                        </div>
                        <div class="modal fade" id="defaultModal{{$vr['id_voucher_register']}}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="defaultModalLabel">การใช้งาน Voucher</h4>
                                    </div>
                                    <div class="modal-body">
                                        @if ($vr['id_advisor'] == '')
                                            <table id="show_voucher" class="table">
                                                <tr>
                                                    <td style="width: 30%"><strong>หัวข้อการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['voucher_topic']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>รายละเอียดการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['voucher_detail']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>วันที่ได้ให้บริการ/รับบริการ</strong></td>
                                                    <td>
                                                        @if ($vr['voucher_date'] == $vr['voucher_date_stop'])
                                                            {{DateThai($vr['voucher_date'])}}
                                                        @else
                                                            {{DateThai($vr['voucher_date'])}} - {{DateThai($vr['voucher_date_stop'])}}
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>สถานที่ให้บริการ/รับบริการ</strong></td>
                                                    <td>{{$vr['voucher_place']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{number_format($vr['voucher_cost'])}} บาท</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <strong>หลักฐานการพัฒนาผลิตภัณฑ์</strong><br>
                                                        @if (substr($vr['voucher_evidence_1'],-3) == 'pdf' | substr($vr['voucher_evidence_1'],-3) == 'PDF')
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                        @else
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if ($vr['voucher_evidence_2'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_2'],-3) == 'pdf' | substr($vr['voucher_evidence_2'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_3'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_3'],-3) == 'pdf' | substr($vr['voucher_evidence_3'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_4'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_4'],-3) == 'pdf' | substr($vr['voucher_evidence_4'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_5'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            @if (substr($vr['voucher_evidence_5'],-3) == 'pdf' | substr($vr['voucher_evidence_5'],-3) == 'PDF')
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank"><h1><i class="fa fa-file-text-o"></i></h1></a>
                                                            @else
                                                                <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank" data-sub-html="Demo Description">
                                                                    <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                        @else
                                            <table id="show_consult" class="table">
                                                <tr>
                                                    <td style="width: 30%"><strong>หัวข้อการให้คำปรึกษา</strong></td>
                                                    <td>{{$vr['voucher_topic']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>รายละเอียดการให้คำปรึกษา</strong></td>
                                                    <td>{{$vr['voucher_detail']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>วันที่ให้คำปรึกษา</strong></td>
                                                    <td>{{DateThai($vr['voucher_date'])}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>สถานที่ให้บริการ/รับบริการ</strong></td>
                                                    <td>{{$vr['voucher_place']}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{number_format($vr['voucher_cost'])}} บาท</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ที่ปรึกษาในการพัฒนาผลิตภัณฑ์</strong></td>
                                                    <td>{{$vr['advisor']->advisor_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <strong>หลักฐานการให้คำปรึกษา</strong><br>
                                                        <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" target="_blank" data-sub-html="Demo Description">
                                                            <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_1'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                @if ($vr['voucher_evidence_2'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_2'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_3'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_3'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_4'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_4'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                                @if ($vr['voucher_evidence_5'] != '')
                                                    <tr>
                                                        <td colspan="2">
                                                            <a href="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" target="_blank" data-sub-html="Demo Description">
                                                                <img src="{{asset('voucher/evidence/'.substr($vr['id_voucher_register'],0,1).'/'.$vr['id_card'].'/'.$vr['voucher_evidence_5'])}}" width="300" alt="หลักฐานการให้คำปรึกษา" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            </table>
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3 pagebreak">
        <div class="col-lg-3 col-md-12 col-sm-12">
            @foreach ($product as $prd)
                <!-- Card -->
                <div class="card">
                    <img class="card-img-top img-responsive" src="{{asset('voucher/product/'.substr($prd['id_product'],0,1).'/'.$prd['product_image'])}}" alt="{{$prd['product_name']}}">
                    <div class="card-body">
                        <h4 class="card-title">{{$prd['product_name']}}</h4>
                        <p class="card-text">{{$prd['product_type']->product_type}}</p>
                        <div class="btn-group">
                            <button type="button" class="js-mytooltip btn btn-default btn-sm" data-toggle="modal" data-target="#largeModal" data-mytooltip-custom-class="align-center" data-mytooltip-content="แสดงข้อมูล"><i class="fa fa-eye"></i></button>
                        </div>
                    </div>
                </div>
                <!-- Card -->

                <!-- Large Size -->
                <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="largeModalLabel">รายละเอียดสินค้า</h4>
                            </div>
                            <div class="modal-body">
                                <img class="img-fluid img-thumbnail" src="{{asset('voucher/product/'.substr($prd['id_product'],0,1).'/'.$prd['product_image'])}}" alt="{{$prd['product_name']}}">
                                <table class="table">
                                    <tr>
                                        <td style="width: 20%"><strong>ชื่อสินค้า</strong></td>
                                        <td>{{$prd['product_name']}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>ประเภทสินค้า</strong></td>
                                        <td>{{$prd['product_type']->product_type}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>รายละเอียดสินค้า</strong></td>
                                        <td>{{$prd['product_detail']}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
@section('script')
    <!-- jQuery 3 -->
    {{-- <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}
    <!--cubeportfolio-->
    <!-- load jquery -->
    <script src="{{asset('dist/plugins/cubeportfolio/jquery-latest.min.js')}}"></script>
    <!-- load cubeportfolio -->
    <script src="{{asset('dist/plugins/cubeportfolio/jquery.cubeportfolio.min.js')}}"></script>
    <!-- init cubeportfolio -->
    <script src="{{asset('dist/plugins/cubeportfolio/main.js')}}"></script>

    <script>
        function myFunction() {
            window.print();
            //document.execCommand("print");
            console.log('print');
        }
    </script>
@endsection
