@extends('layouts.niche')
@section('css')
    {{-- <!-- v4.0.0-alpha.6 -->
    <link rel="stylesheet" href="{{asset('dist/bootstrap/css/bootstrap.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/et-line-font/et-line-font.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/themify-icons/themify-icons.css')}}"> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('header')
    <h2>เพิ่มข้อมูลผู้ประกอบการ
    {{-- <small class="text-muted">Welcome</small> --}}
    </h2>
@endsection
@section('container')
<div class="info-box">
    <div class="row clearfix">
        <div class="col-lg-12">
            {!! Form::open(['url'=>'/entrepreneur','files'=>true,'id'=>'form-entrepreneur']) !!}
            {{-- <form action="/entrepreneur" id="form_validation" method="POST" enctype="multipart/form-data">
                @csrf --}}
                <h4 class="text-black">ข้อมูลเจ้าของกิจการ</h4>
                <div class="row">
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="id_card">เลขบัตรประชาชน <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" value="{{$id_card}}" disabled required>
                    </div>
                    <input type="hidden" name="id_card" value="{{$id_card}}">
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="prefix">คำนำหน้าชื่อ <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" name="prefix" required>
                            <option value="">กรุณาเลือกคำนำหน้าชื่อ <span class="text-danger">*</span></option>
                            <option value="นาย">นาย</option>
                            <option value="นาง">นาง</option>
                            <option value="น.ส.">น.ส.</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="name">ชื่อ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="name" maxlength="150" required>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="lastname">นามสกุล <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="lastname" maxlength="150" required>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="address">บ้านเลขที่ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="address" maxlength="100" required>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="moo">หมู่</label>
                        <input type="text" class="form-control" name="moo" maxlength="100">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="soy">ซอย</label>
                        <input type="text" class="form-control" name="soy" maxlength="100">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="road">ถนน</label>
                        <input type="text" class="form-control" name="road" maxlength="100">
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="province">จังหวัด <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="province" name="province" required>
                            <option value="">กรุณาเลือกจังหวัด</option>
                            @foreach ($province as $p)
                                <option value="{{$p['PROVINCE_ID']}}">{{$p['PROVINCE_NAME']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="amphur">อำเภอ <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="amphur" name="amphur" disabled required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="district">ตำบล <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="district" name="district" disabled required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-sm-12">
                        <label for="zipcode">รหัสไปรษณีย์ <span class="text-danger">*</span></label>
                        <select class="form-control show-tick" id="zipcode" name="zipcode" disabled required>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="phone">โทรศัพท์ <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="phone" name="phone" minlength="10" maxlength="10" required>
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="fax">โทรสาร</label>
                        <input type="number" class="form-control" id="fax" name="fax">
                    </div>
                    <div class="form-group col-lg-4 col-sm-12">
                        <label for="email">อีเมล <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                </div>
                <h4 class="text-black">ข้อมูลสถานประกอบการ</h4>
                <div class="row">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="company_name">ชื่อสถานประกอบการ <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="company_name" name="company_name" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="osmep_number">เลขสมาชิก สสว. <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="osmep_number" name="osmep_number" maxlength="14" minlength="14" onkeyup="keyup(this,event)" required>
                        <div id="message"></div>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label>รูปแบบการดำเนินกิจการ <span class="text-danger">*</span></label>
                        @foreach ($business_type as $bt)
                        <div class="radio">
                            <label for="business_type_{{$bt['id_business_type']}}">
                                <input name="id_business_type" type="radio" id="business_type_{{$bt['id_business_type']}}" value="{{$bt['id_business_type']}}" required />
                                {{$bt['business_type']}}
                            </label>
                        </div>
                        @endforeach
                        <div id="business_type_other_1" class="form-group col-lg-6 col-sm-12" style="display: none">
                            <label for="other_please_specify">โปรดระบุ</label>
                            <input type="text" class="form-control" id="other_please_specify" name="other_please_specify">
                        </div>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="business_processing_time">ระยะเวลาดำเนินกิจการ <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="business_processing_time" name="business_processing_time" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="number_of_employment">จำนวนการจ้างงาน <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="number_of_employment" name="number_of_employment" required>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="problems_in_business">ปัญหาและอุปสรรค ในการดำเนินกิจการ <span class="text-danger">*</span></label>
                        <textarea id="problems_in_business" name="problems_in_business" cols="30" rows="5" class="form-control no-resize" required></textarea>
                    </div>
                </div>
                <h4 class="text-black">ความต้องการรับการพัฒนา <span class="text-danger">*</span></h4>
                <div class="row">
                    <div class="form-group col-lg-12 col-sm-12">
                        @foreach ($development as $d)
                            <div class="radio">
                                <label for="development_{{$d['id_development']}}">
                                    <input name="id_development" type="radio" id="development_{{$d['id_development']}}" value="{{$d['id_development']}}" required/>
                                    {{$d['development']}}
                                </label>
                            </div>
                        @endforeach
                        <div class="form-group col-lg-6 col-sm-12" id="development_other_1" style="display: none">
                            <label class="form-label">โปรดระบุ</label>
                            <input type="text" class="form-control" name="development_other" id="development_other">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="development_detail">รายละเอียดความต้องการรับการพัฒนา ระบุ <span class="text-danger">*</span></label>
                        <textarea id="development_detail" name="development_detail" cols="30" rows="5" class="form-control no-resize" required></textarea>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="id_sector">ภาคธุรกิจ <span class="text-danger">*</span></label>
                        @foreach ($sector as $s)
                            <div class="radio">
                                <label for="id_sector_{{$s['id_sector']}}">
                                    <input type="radio" name="id_sector" id="id_sector_{{$s['id_sector']}}" value="{{$s['id_sector']}}" class="with-gap" required>
                                    {{$s['sector']}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="tsic">เลข TSIC <span class="text-danger">*</span></label>
                        <select class="form-control" data-live-search="true" id="tsic" name="tsic" required>
                            <option value="">เลข TSIC</option>
                            @foreach ($tsic as $t)
                                <option value="{{$t['tsic_code']}}">{{$t['tsic_code'].' '.$t['tsic_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="report_200">สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล (อายุไม่เกิน 6 เดือน) หรือสำเนาทะเบียนการค้า หรือสำเนาการจดทะเบียนจากหน่วยงานราชการอื่น (ถ้ามี)</label>
                        <small>รองรับการอัพโหลดไฟล์ PDF เท่านั้น</small>
                        <input type="file" id="document" name="document" accept=".pdf">
                    </div>
                    <div class="form-group col-lg-12 col-sm-12">
                        <label for="report_200">แผนที่แสดงที่ตั้งของกิจการ (ภาพวาด/ภาพถ่าย/ภาพจาก Application ด้านแผนที่ต่างๆ เช่น Google map) <span class="text-danger">*</span></label>
                        <small>รองรับการอัพโหลดไฟล์รูปภาพเท่านั้น</small>
                        <input type="file" id="map" name="map" accept=".jpg, .png" required>
                    </div>
                </div>
                <button class="btn btn-raised btn-primary waves-effect" id="btn_submit" type="submit">บันทึกข้อมูล</button>
            {{-- </form> --}}
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('script')
    {{-- <!-- jQuery 3 -->
    <script src="{{asset('dist/js/jquery.min.js')}}"></script>

    <!-- v4.0.0-alpha.6 -->
    <script src="{{asset('dist/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- template -->
    <script src="{{asset('dist/js/niche.js')}}"></script> --}}

    <!-- Validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <script src="{{asset('js/jquery-validate-message.js')}}"></script>

    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        $('#form-entrepreneur').validate();

        $('#tsic').select2();

        $('#province').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        console.log(select);
                        $("#amphur").removeAttr("disabled");
                        $('#amphur').html(result);

                    }
                })
            }
        });
        $('#amphur').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile2')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        $("#district").removeAttr("disabled");
                        $('#district').html(result);
                    }
                })
            }
        });
        $('#district').change(function(){
            if ($(this).val()!='') {
                var select = $(this).val();
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('dropdown.fetchprofile3')}}",
                    method:"POST",
                    data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                    success:function(result){
                        //ได้ข้อมูลมาแล้วทำอะไรต่อ
                        $("#zipcode").removeAttr("disabled");
                        $('#zipcode').html(result);
                    }
                })
            }
        });

        $("#business_type_1").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_2").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_3").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_4").click(function () {
            $("#business_type_other_1").hide();
            $("#business_type_other_1").prop('required',false);
            $("#business_type_other").val('');
        });
        $("#business_type_5").click(function () {
            $("#business_type_other_1").show();
            $( "#business_type_other" ).rules( "add", {
                required: true,
                messages: {
                required: "กรุณากรอกข้อมูล",
                }
            });
        });

        $("#development_1").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_2").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_3").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_4").click(function () {
            $("#development_other_1").hide();
            $("#development_other_1").prop('required',false);
            $("#development_other_1").val('');
        });
        $("#development_5").click(function () {
            $("#development_other_1").show();
            $( "#development_other" ).rules( "add", {
                required: true,
                messages: {
                required: "กรุณากรอกข้อมูล",
                }
            });
        });


        //รับค่าจาก textbox
        function keyup(obj,e){
            var id = obj.value.length;
            var osmep_number = obj.value;
            var select = osmep_number;
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{route('dropdown.fetchosmepnumber')}}",
                method:"POST",
                data:{select:select,_token:_token}, //โยนข้อมูลไปทำงานที่ controller
                success:function(result){
                    console.log(result);
                    $('#message').html(result);
                }
            })
        }

    </script>
@endsection
