<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>:: Voucher :: Sign In</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/authentication.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/color_skins.css')}}">
</head>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <div class="logo"><img src="{{asset('assets/images/logo.png')}}" alt="Nexa"></div>
                        {{-- <h1>Voucher</h1> --}}
                        <p class="mt-3">โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน</p>
                        {{-- <ul class="list-unstyled l-social">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        </ul> --}}
                    </div>
                </div>
                <form class="col-lg-12" id="sign_in" method="POST" action="{{ route('login') }}">
                    @csrf
                    <h5 class="title mb-3">ลงชื่อเข้าใช้บัญชีของคุณ</h5>
                    <!-- Session Status -->
                    <x-auth-session-status class="alert alert-success" :status="session('status')" />
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="text-danger text-sm text-left" :errors="$errors" />
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input class="form-control" id="email" type="email" name="email" :value="old('email')" required>
                            <label class="form-label">อีเมล</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="password" class="form-control" id="password" name="password" required autocomplete="current-password">
                            <label class="form-label">รหัสผ่าน</label>
                        </div>
                    </div>
                    {{-- <div>
                        <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-cyan">
                        <label for="rememberme">Remember Me</label>
                    </div>  --}}
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-raised btn-primary waves-effect">ลงชื่อเข้าใช้</button>
                        <a href="/register" class="btn btn-raised btn-default waves-effect">สมัครสมาชิก</a>
                    </div>
                </form>
                <div class="col-lg-12 m-t-20">
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">ลืมรหัสผ่าน ?</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script>
<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script>
</body>
</html>
