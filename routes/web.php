<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EntrepreneurController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\Report200Controller;
use App\Http\Controllers\ExportReportController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AdvisorController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\DocumentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');
Route::get('/dashboard', [DashboardController::class, 'index']);
Route::get('/dashboardteam/{id_team}', [DashboardController::class, 'dashboardteam']);


require __DIR__.'/auth.php';
//จัดการผู้ประกอบการ
Route::resource('entrepreneur', EntrepreneurController::class);
Route::get('/entrepreneur/create/{id_card}', [EntrepreneurController::class, 'create']);
Route::get('/entrepreneur/{id}/destroy', [EntrepreneurController::class, 'destroy']);
Route::post('/entrepreneur/create/fetchprofile', [EntrepreneurController::class, 'fetchprofile'])->name('dropdown.fetchprofile');
Route::post('/entrepreneur/create/fetchprofile2', [EntrepreneurController::class, 'fetchprofile2'])->name('dropdown.fetchprofile2');
Route::post('/entrepreneur/create/fetchprofile3', [EntrepreneurController::class, 'fetchprofile3'])->name('dropdown.fetchprofile3');
Route::post('/entrepreneur/create/fetchosmepnumber', [EntrepreneurController::class, 'fetchosmepnumber'])->name('dropdown.fetchosmepnumber');
Route::post('/checkidcard', [EntrepreneurController::class, 'checkidcard']);
Route::get('/entrepreneurteam/{id_team}', [EntrepreneurController::class, 'entrepreneurteam']);
Route::post('/downloadreportword', [EntrepreneurController::class, 'downloadreportword']);
Route::post('/downloadreportwordall', [EntrepreneurController::class, 'downloadreportwordall']);
Route::resource('voucher', VoucherController::class);
Route::get('voucher/create/{id}', [VoucherController::class, 'create']);
Route::get('voucher/{id}/edit/{id_card}', [VoucherController::class, 'edit']);
Route::post('/voucher/create/fetchadvisor', [VoucherController::class, 'fetchadvisor'])->name('dropdown.fetchadvisor');
Route::post('/savecostsummary', [VoucherController::class, 'savecostsummary']);
Route::post('/updatecostsummary', [VoucherController::class, 'updatecostsummary']);
Route::resource('advisor', AdvisorController::class);
Route::get('advisor/create/{id}', [AdvisorController::class, 'create']);
Route::get('advisor/{id}/edit/{id_card}', [AdvisorController::class, 'edit']);
Route::resource('report200', Report200Controller::class);
Route::get('report200team/{id_team}', [Report200Controller::class, 'report200team']);
Route::get('downloadallreport200/{id_team}', [Report200Controller::class, 'downloadallreport200']);
Route::resource('export', ExportReportController::class);
Route::resource('product', ProductController::class);
Route::get('/product/create/{id_card}', [ProductController::class, 'create']);
Route::get('delproductimage/{id}', [ProductController::class, 'delproductimage']);
Route::resource('user', UserController::class);
Route::get('userapprove/{id}/{status}', [UserController::class, 'userapprove']);
Route::get('exportreport300/{id_team}', [ExportController::class, 'exportreport300']);
Route::get('exportword', [DocumentController::class, 'create']);
Route::get('exportword/{id_card}', [DocumentController::class, 'store']);
