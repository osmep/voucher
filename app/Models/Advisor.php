<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    use HasFactory;

    protected $table = 'advisor';
    protected $primaryKey = 'id_advisor';
    protected $fillable = [ 
        'advisor_id_card', 'advisor_name', 'advisor_email', 'advisor_phone', 'advisor_expert', 'advisor_profile', 'advisor_sme_coach'
    ];
}
