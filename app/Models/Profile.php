<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\Document;
use App\Models\Product;
use App\Models\DevelopmentDemand;
use App\Models\BusinessTypeSelect;
use App\Models\VoucherRegister;
use App\Models\User;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profile';
    protected $primaryKey = 'id_card';
    protected $fillable = [
        'prefix', 'name', 'lastname', 'address', 'moo', 'soy', 'road', 'district', 'amphur', 'province', 'zipcode',
        'phone', 'fax', 'email', 'id_team_owner', 'id_owner', 'id_comp', 'id_document', 'id_development_demand',
        'id_business_type_select'
    ];

    /**
     * Get the user associated with the Profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    protected $casts = [
        'id_card' => 'string',
    ];

    public function company()
    {
        return $this->hasOne(Company::class, 'id_comp', 'id_comp');
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'id_document', 'id_document');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id_product', 'id_product');
    }

    public function development_demand()
    {
        return $this->hasOne(DevelopmentDemand::class, 'id_development_demand', 'id_development_demand');
    }

    public function business_type_select()
    {
        return $this->hasOne(BusinessTypeSelect::class, 'id_business_type_select', 'id_business_type_select');
    }

    public function voucher_register()
    {
        return $this->hasMany(VoucherRegister::class, 'id_card', 'id_card');
    }

    public function team()
    {
        return $this->hasOne(Team::class, 'id_team', 'id_team_owner');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id_user', 'id_owner');
    }
}
