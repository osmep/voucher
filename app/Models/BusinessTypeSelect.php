<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\BusinessType;

class BusinessTypeSelect extends Model
{
    use HasFactory;

    protected $table = 'business_type_select';
    protected $primaryKey = 'id_business_type_select';
    protected $fillable = [ 
        'other_please_specify', 'id_business_type'
    ];

    /**
     * Get the user associated with the BusinessTypeSelect
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function business_type()
    {
        return $this->hasOne(BusinessType::class, 'id_business_type', 'id_business_type');
    }
}
