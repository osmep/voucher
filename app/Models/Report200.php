<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report200 extends Model
{
    use HasFactory;
    protected $table = 'report_200';
    protected $primaryKey = 'id_report_200';
    protected $fillable = [
        'report_200', 'report_month', 'id_team'
    ];
}
