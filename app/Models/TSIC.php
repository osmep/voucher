<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TSIC extends Model
{
    use HasFactory;

    protected $table = 'tsic';
    protected $primaryKey = 'tsic_id';
    protected $fillable = [
        'tsic_code', 
        'tsic_name',
    ];
}
