<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $table = 'province';
    protected $primaryKey = 'PROVINCE_ID';
    protected $fillable = [ 
        'PROVINCE_CODE', 'PROVINCE_NAME', 'PROVINCE_NAME_ENG', 'GEO_ID'
    ];
}
