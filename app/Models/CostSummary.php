<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostSummary extends Model
{
    use HasFactory;
    protected $table = 'cost_summary';
    protected $primaryKey = 'id_cost_summary';
    protected $fillable = [
        'cost_summary', 'id_card'
    ];

    /**
     * Get the profile associated with the CostSummary
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'id_card', 'id_card');
    }
}
