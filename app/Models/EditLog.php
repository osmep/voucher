<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditLog extends Model
{
    use HasFactory;
    protected $table = 'edit_log';
    protected $primaryKey = 'id_edit_log';
    protected $fillable = [
        'id_card', 'id_user_edit', 'field_name', 'text_old', 'text_new'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id_user', 'id_user_edit');
    }
}
