<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdvisorRegister extends Model
{
    use HasFactory;

    protected $table = 'advisor_register';
    protected $primaryKey = 'id_advisor_register';
    protected $fillable = [
        'id_advisor', 'date_consult'
    ];
}
