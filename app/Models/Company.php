<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Model\Sector;

class Company extends Model
{
    use HasFactory;

    protected $table = 'company';
    protected $primaryKey = 'id_comp';
    protected $fillable = [ 
        'company_name', 'osmep_number', 'business_processing_time', 'number_of_employment', 
        'problems_in_business', 'tsic', 'id_sector'
    ];

    /**
     * Get the user associated with the Company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sector()
    {
        return $this->hasOne(Sector::class, 'id_sector', 'id_sector');
    }
}
