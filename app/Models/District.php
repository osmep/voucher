<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;

    protected $table = 'district';
    protected $primaryKey = 'DISTRICT_ID';
    protected $fillable = [ 
        'DISTRICT_CODE', 'DISTRICT_NAME', 'DISTRICT_NAME_ENG', 'AMPHUR_ID', 'PROVINCE_ID', 'GEO_ID', 'ZIPCODE'
    ];
}
