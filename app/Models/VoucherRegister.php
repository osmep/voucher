<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Advisor;
use App\Models\Profile;

class VoucherRegister extends Model
{
    use HasFactory;

    protected $table = 'voucher_register';
    protected $primaryKey = 'id_voucher_register';
    protected $fillable = [
        'voucher_topic',
        'voucher_detail',
        'voucher_evidence_1',
        'voucher_evidence_2',
        'voucher_evidence_3',
        'voucher_evidence_4',
        'voucher_evidence_5',
        'voucher_cost',
        'voucher_date',
        'voucher_date_stop',
        'voucher_place',
        'voucher_manday',
        'id_advisor',
        'id_card'
    ];

    /**
     * Get the user associated with the VoucherRegister
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function advisor()
    {
        return $this->hasOne(Advisor::class, 'id_advisor', 'id_advisor');
    }

    /**
     * Get all of the profile for the VoucherRegister
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profile()
    {
        return $this->hasMany(Profile::class, 'id_card', 'id_card');
    }
}
