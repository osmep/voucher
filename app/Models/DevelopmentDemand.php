<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Development;

class DevelopmentDemand extends Model
{
    use HasFactory;

    protected $table = 'development_demand';
    protected $primaryKey = 'id_development_demand';
    protected $fillable = [ 
        'development_detail', 'id_development'
    ];

    /**
     * Get the development associated with the DevelopmentDemand
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function development()
    {
        return $this->hasOne(Development::class, 'id_development', 'id_development');
    }
}
