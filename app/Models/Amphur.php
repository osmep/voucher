<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amphur extends Model
{
    use HasFactory;

    protected $table = 'amphur';
    protected $primaryKey = 'AMPHUR_ID';
    protected $fillable = [ 
        'AMPHUR_CODE', 'AMPHUR_NAME', 'AMPHUR_NAME_ENG','GEO_ID', 'PROVINCE_ID'
    ];
}
