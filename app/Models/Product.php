<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product'; //เรียกตาราง Product หากไม่ได้ตั้งชื่อตารางแล้วตามด้วย s เช่น products
    protected $primaryKey = 'id_product'; //กำหนด PrimaryKey ของตาราง ให้ Model มองชื่อที่กำหนดเป็น PrimaryKey ทำต่อเมื่อชื่อ PrimaryKey ไม่ใช่่ id
    protected $fillable = [ 
        'product_name', 'product_detail', 'product_image', 'id_product_type' //กำหนดฟิลด์ที่สามารถอ่านเขียนได้
    ];

    /**
     * Get the product_type associated with the Product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product_type()
    {
        return $this->hasOne(ProductType::class, 'id_product_type', 'id_product_type');
        //กำหนดความสัมพันธ์ของตาราง
        //ในที่นี้หมายถึง id_product_type ของตาราง product มีความสัมพันธ์แบบ hasOne (1 ต่อ 1) กับตาราง ProductType ที่ id_product_type
    }
}
