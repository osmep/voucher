<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Models\Profile;

class Reprot300Export implements FromView
{
    protected $id_team;

    function __construct($id_team) {
            $this->id_team = $id_team;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        if ($this->id_team == 1) {
            $profile = Profile::all();
        } else {
            $profile = Profile::where('id_team_owner',$this->id_team)->get();
        }
        $data = array(
            'profile' => $profile,
        );
        return view('export.export300',$data);
    }
}
