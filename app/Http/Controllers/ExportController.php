<?php

namespace App\Http\Controllers;

use App\Exports\Reprot300Export;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    // public function exportreport300()
    // {
    //     return Excel::download(new Reprot300Export, 'report300.xlsx');
    // }

    public function exportreport300(Request $request)
    {
        return Excel::download(new Reprot300Export($request->id_team), 'report300.xlsx');
    }
}
