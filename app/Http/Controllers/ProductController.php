<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Profile;
use Auth;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $product = Product::where('id_card',$id)->get();
        $product_type = ProductType::all();
        $data = array(
            'product' => $product,
            'product_type' => $product_type,
            'id_card' => $id
        );
        return view('product/form_product',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::find($request->id_card);
        $product = new Product;
        $id_product = Product::where('id_product', 'like', $profile->id_team_owner.'%')->pluck('id_product')->max();
        if ($id_product=='') {
            $id_product_1 = $profile->id_team_owner.'000001';
        }
        else
        {
            $id_product_1 = $id_product+1;
        }
        $product->id_product = $id_product_1;
        $product->product_name = $request->product_name;
        $product->product_detail = $request->product_detail;
        $product->id_product_type = $request->id_product_type;
        if ($request->file('product_image')) {
            if (!is_dir("voucher/product/".$profile->id_team_owner)) {
                mkdir("voucher/product/".$profile->id_team_owner);
            }
            $folderproduct = 'voucher/product/'.$profile->id_team_owner;
            $imgwidth = 900; //resize image
            $file = $request->file('product_image');
            $filename_product = $id_product_1.'-'.$request->id_card.'.'.$request->file('product_image')->getClientOriginalExtension();
            $path = public_path($folderproduct.'/' . $filename_product);
            $img = \Image::make($file->getRealPath());// create instance of Intervention Image
            if($img->width()>$imgwidth){
                // See the docs - http://image.intervention.io/api/resize
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img->resize($imgwidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($path);
            $product->product_image = $filename_product;
        }
        $product->id_card = $request->id_card;
        $product->save();
        Session::flash('success','เพิ่มสินค้าสำเร็จ');
        return redirect('product/create/'.$request->id_card);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_edit = Product::find($id);
        $product = Product::where('id_card',$product_edit->id_card)->get();
        $product_type = ProductType::all();
        $data = array(
            'product_edit' => $product_edit,
            'product' => $product,
            'product_type' => $product_type,
            'id_card' => $product_edit->id_card
        );
        return view('product/form_product',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $profile = Profile::find($product->id_card);
        $product->product_name = $request->product_name;
        $product->product_detail = $request->product_detail;
        $product->id_product_type = $request->id_product_type;
        if ($request->file('product_image')) {
            if (!is_dir("voucher/product/".$profile->id_team_owner)) {
                mkdir("voucher/product/".$profile->id_team_owner);
            }
            $folderproduct = 'voucher/product/'.$profile->id_team_owner;
            $imgwidth = 900; //resize image
            $file = $request->file('product_image');
            $filename_product = $id.'-'.$request->id_card.'.'.$request->file('product_image')->getClientOriginalExtension();
            $path = public_path($folderproduct.'/' . $filename_product);
            $img = \Image::make($file->getRealPath());// create instance of Intervention Image
            if($img->width()>$imgwidth){
                // See the docs - http://image.intervention.io/api/resize
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img->resize($imgwidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($path);
            $product->product_image = $filename_product;
        }
        $product->save();
        Session::flash('success','แก้ไขสินค้าสำเร็จ');
        return redirect('product/create/'.$request->id_card);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        @unlink("voucher/product/".substr($id,0,1)."/".$product->product_image);
        $product->delete();
        Session::flash('success','ลบสินค้าสำเร็จ');
        return redirect('product/create/'.$product->id_card);
    }

    public function delproductimage($id)
    {
        $product = Product::find($id);
        @unlink("voucher/product/".substr($id,0,1)."/".$product->product_image);
        $product->product_image = null;
        $product->save();
        return redirect('product/'.$id.'/edit');
    }
}
