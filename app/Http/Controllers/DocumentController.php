<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\VoucherRegister;
use App\Models\Product;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createdocument');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_card)
    {
        $profile = Profile::find($id_card);
        $consult = VoucherRegister::where('id_card',$id_card)->whereNotNull('id_advisor')->get();
        $voucher = VoucherRegister::where('id_card',$id_card)->whereNull('id_advisor')->get();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $fontheading = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'bgColor'=>'FFFF00','lineHeight'=> 1.0);
        $fontheadingnogb = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'lineHeight'=> 1.0);
        $fontbody = array('name'=>'TH Sarabun New','size' => 16,'lineHeight'=> 1.0);
        $fontbodybold = array('name'=>'TH Sarabun New','size' => 16,'bold' => true,'lineHeight'=> 1.0);
        $fontbody14 = array('name'=>'TH Sarabun New','size' => 14,'lineHeight'=> 1.0);
        $tableStyle = array('borderSize' => 1, 'borderColor' => '#000000','position'=>'center','cellMargin'  => 50);
        $tableLogoStyle = array('position'=>'center');
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');
        $cellColSpan = array('gridSpan' => 5);
        $imagestyle = array('width' => 300, 'alignment'=>'center');
        $logostyle = array('height' => 100, 'alignment'=>'center');
        $fontcenter = array('align'=>'center');
        $fontright = array('align'=>'right');
        $section = $phpWord->addSection();
        switch ($profile->id_team_owner) {
            case '2':
                $table = $section->addTable($tableLogoStyle);
                $table->addRow();
                $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                $table->addCell(2000)->addImage("./dist/img/logo/CEA.png",$logostyle);
                break;
            case '3':
                $table = $section->addTable($tableLogoStyle);
                $table->addRow();
                $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                $table->addCell(2000)->addImage("./dist/img/logo/CLT.png",$logostyle);
                break;
            case '4':
                $table = $section->addTable($tableLogoStyle);
                $table->addRow();
                $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                $table->addCell(2000)->addImage("./dist/img/logo/CMU.png",$logostyle);
                break;
            case '5':
                $table = $section->addTable($tableLogoStyle);
                $table->addRow();
                $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                $table->addCell(2000)->addImage("./dist/img/logo/TISTR.png",$logostyle);
                break;
            case '6':
                $table = $section->addTable($tableLogoStyle);
                $table->addRow();
                $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                $table->addCell(2000)->addImage("./dist/img/logo/NFI.png",$logostyle);
                break;
            default:
                $section->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                break;
        }
        $text = $section->addText('โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์',$fontheadingnogb,$fontcenter);
        $text = $section->addText('สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน',$fontheadingnogb,$fontcenter);
        $text = $section->addText('ชื่อผู้ประกอบการ '.$profile->prefix.$profile->name.' '.$profile->lastname,$fontbody,$fontcenter);
        $text = $section->addText('ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name,$fontbody,$fontcenter);
        $text = $section->addText('จังหวัด '.$profile->province,$fontbody,$fontcenter);
        $section->addPageBreak();
        $text = $section->addText('ส่วนที่ 1 ข้อมูลพื้นฐานของผู้ประกอบการ/ผลิตภัณฑ์',$fontheading,$fontcenter);
        $text = $section->addText('1. ชื่อ-นามสกุล '.$profile->prefix.$profile->name.' '.$profile->lastname.' อายุ..................ปี ตำแหน่ง................................................................',$fontbody);
        $text = $section->addText('	ระดับการศึกษาและสถาบัน..............................................................................................',$fontbody);
        $text = $section->addText('	ประสบการณ์/ความถนัดของผู้บริหาร..............................................................................................',$fontbody);
        $text = $section->addText('	ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name.' ปีที่ก่อตั้ง..................',$fontbody);
        $text = $section->addText('	ระยะเวลาดำเนินธุรกิจ '.$profile->company->business_processing_time.' ปี',$fontbody);
        $text = $section->addText('	ที่ตั้งธุรกิจ..............................................................................................',$fontbody);
        $text = $section->addText('	โทรศัพท์ '.$profile->phone.' Line ID:................................................................',$fontbody);
        $text = $section->addText('	ประเภท/ รายละเอียดกิจการ..............................................................................................',$fontbody);
        $text = $section->addText('2. การจดทะเบียนจัดตั้งธุรกิจ',$fontbody);
        switch ($profile->business_type_select->id_business_type) {
            case '1':
                $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	🗹 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                break;
            case '2':
                $text = $section->addText('	🗹 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                break;
            case '3':
                $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	🗹 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                break;
            case '4':
                $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) กลุ่มสหกรณ์',$fontbody);
                break;
            case '5':
                $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) '.$profile->business_type_select->other_please_specify,$fontbody);
                break;
            default:
                $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                break;
        }
        $text = $section->addText('	หมายเหตุ: การจดทะเบียนจัดตั้งธุรกิจ ได้แก่',$fontbody14);
        $text = $section->addText('	1) บุคคลธรรมดา ได้แก่ จดทะเบียนพาณิชย์ และจดทะเบียนการค้า',$fontbody14);
        $text = $section->addText('	2) นิติบุคคล ได้แก่ บริษัทจำกัด และ ห้างส่วนจำกัด',$fontbody14);
        $text = $section->addText('	3) วิสาหกิจชุมชน',$fontbody14);
        $text = $section->addText('3. วิธีการบริหารกิจการ :..............................................................................................',$fontbody);
        $text = $section->addText('	☐ บริหารโดยเจ้าของคนเดียว			🗹 บริหารโดยเจ้าของและพี่น้อง, หรือญาติ',$fontbody);
        $text = $section->addText('	☐ บริหารโดยมีการว่าจ้างบุคคลภายนอก		☐ อื่นๆ (ระบุ)..................',$fontbody);
        $text = $section->addText('4. ประเภทแพคเกจ (เลือกมาหนึ่งอย่างที่เด่นชัดที่สุด)',$fontbody);
        $text = $section->addText('	☐ 1) พัฒนามาตรฐานผลิตภัณฑ์สมุนไพร/เครื่องสำอาง',$fontbody);
        $text = $section->addText('	☐ 2) พัฒนามาตรฐานผลิตภัณฑ์อาหาร เครื่องดื่ม เกษตรแปรรูป',$fontbody);
        $text = $section->addText('	☐ 3) พัฒนามาตรฐานผลิตภัณฑ์ที่มีมูลค่าสูง',$fontbody);
        $text = $section->addText('	☐ 4) พัฒนาการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์/สร้างแบนรด์',$fontbody);
        $text = $section->addText('5. ลักษณะกิจการ / โครงการ',$fontbody);
        $text = $section->addText('	☐ 1) เริ่มกิจการใหม่	☐ 2) ขยายกิจการ	☐ 3) ปรับปรุงกิจการ',$fontbody);
        $text = $section->addText('	คาดว่าจะเริ่มดำเนินการ ประมาณเดือน..................ปี',$fontbody);
        $text = $section->addText('6. ลักษณะสินค้า/บริการที่เข้ารับการพัฒนา  (กรณีที่มีภาพตัวอย่างมากกว่านี้ อาจทำเป็นเอกสารแนบ) ',$fontbody);
        $count_product = Product::where('id_card',$id_card)->count();
        $product = Product::where('id_card',$id_card)->first();
        if ($count_product <> 0) {
            $section->addImage("./voucher/product/".$profile->id_team_owner.'/'.$product->product_image,$imagestyle);
            $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
            $text = $section->addText($product->product_detail,$fontbody);
        } else {
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('รูปภาพสินค้า/บริการ',$fontbody,$fontcenter);
            $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        }
        $text = $section->addText('7. ลักษณะบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
        $section->addImage("./dist/img/block_image.png",$imagestyle);
        $text = $section->addText('รูปภาพบรรจุภัณฑ์ (ถ้ามี)',$fontbody,$fontcenter);
        $text = $section->addText('รายละเอียดบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('8. ยอดขายโดยประมาณ (บาทต่อเดือน)',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('9. แผนผังที่ตั้งกิจการ (ระบุพิกัด GPS  X………………Y………………)',$fontbody);
        $section->addImage("./voucher/document/".$profile->id_team_owner.'/'.$profile->id_card.'/'.$profile->document->map,$imagestyle);
        //ส่วนที่สอง
        $section->addPageBreak();
        $text = $section->addText('ส่วนที่ 2 รายงานการวิเคราะห์สภาพปัญหา/ความต้องการรับการพัฒนา',$fontheading,$fontcenter);
        if ($count_product <> 0) {
            $text = $section->addText('1. สินค้า/บริการหลัก : '.$product->product_name,$fontbody);
        } else {
            $text = $section->addText('1. สินค้า/บริการหลัก :…………………………………………………………………………………',$fontbody);
        }
        $text = $section->addText('2. ชื่อตราสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
        $section->addImage("./dist/img/block_image.png",$imagestyle);
        $text = $section->addText('LOGO (ถ้ามี)',$fontbody);
        $text = $section->addText('3. มาตรฐานสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
        $text = $section->addText('4. กำลังการผลิต/บริการสูงสุดต่อปี ……………………………………หน่วย……………………………………',$fontbody);
        $text = $section->addText('กำลังการผลิต/บริการจริงต่อปี ……………………………………หน่วย……………………………………',$fontbody);
        $text = $section->addText('คิดเป็น……………………………………% ปริมาณการผลิตที่คาดหวัง ปี 2564……………………………………',$fontbody);
        $text = $section->addText('5. บุคลากร (Man) ได้แก่',$fontbody);
        $text = $section->addText('	(1) บุคลากรการผลิต',$fontbody);
        $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
        $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
        $text = $section->addText('	(2) บุคลากรขายและบริหาร',$fontbody);
        $text = $section->addText('		☐ ผู้บริหาร…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
        $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
        $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
        $text = $section->addText('6. เครื่องจักร (Machine) (ถ้ามี)',$fontbody);

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(2000)->addText("รายการ",$fontbody);
        $table->addCell(2000)->addText("ราคาต่อหน่วย (บาท)",$fontbody);
        $table->addCell(2000)->addText("จำนวน",$fontbody);
        $table->addCell(2000)->addText("ราคารวม (บาท)",$fontbody);
        $table->addCell(2000)->addText("อายุการใช้งาน (ปี)",$fontbody);
        $table->addCell(2000)->addText("ค่าเสื่อมราคา (บาทต่อปี)",$fontbody);
        $table->addCell(2000)->addText("ลักษณะการใช้งาน",$fontbody);
        $table->addRow();
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(4000, $cellColSpan)->addText("รวม ค่าเสื่อมราคาในการผลิต",$fontbody);
        $table->addCell(2000);
        $table->addCell(2000)->addText("บาทต่อปี ",$fontbody);

        $text = $section->addText('7. วัตถุดิบหลัก (Material) ได้แก่',$fontbody);
        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(3000)->addText("ลำดับ",$fontbody);
        $table->addCell(3000)->addText("ชื่อวัตถุดิบ",$fontbody);
        $table->addCell(3000)->addText("สถานที่",$fontbody);
        $table->addRow();
        $table->addCell(3000)->addText("1",$fontbody);
        $table->addCell(3000);
        $table->addCell(3000);
        $table->addRow();
        $table->addCell(3000)->addText("2",$fontbody);
        $table->addCell(3000);
        $table->addCell(3000);
        $table->addRow();
        $table->addCell(3000)->addText("3",$fontbody);
        $table->addCell(3000);
        $table->addCell(3000);
        $table->addRow();
        $table->addCell(3000)->addText("4",$fontbody);
        $table->addCell(3000);
        $table->addCell(3000);
        $table->addRow();
        $table->addCell(3000)->addText("5",$fontbody);
        $table->addCell(3000);
        $table->addCell(3000);
        $text = $section->addText('8. กระบวนการผลิต (Method)',$fontbody);
        $text = $section->addText('	ขั้นตอนกระบวนการผลิต (Process Flow) (อธิบายขั้นตอนพร้อมภาพประกอบ)',$fontbody);
        $section->addImage("./dist/img/block_image.png",$imagestyle);
        $text = $section->addText('9. การขาย/การตลาด',$fontbody);
        $text = $section->addText('ลูกค้าเป้าหมาย :',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('ช่องทางการจัดจำหน่าย : ',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('10. สภาพปัญหาเบื้องต้นและข้อเสนอแนะเพื่อการพัฒนาผลิตภัณฑ์ (กรุณา X ไม่จำกัดจำนวนข้อ)',$fontbody);
        $text = $section->addText('	◯ การพัฒนาศักยภาพการผลิต',$fontbody);
        $text = $section->addText('		☐ ด้านมาตรฐานการผลิต',$fontbody);
        $text = $section->addText('		☐ ด้านการปรับปรุงกระบวนการผลิต',$fontbody);
        $text = $section->addText('		☐ ด้านออกแบบปรับปรุงเครื่องจักรเพื่อการลดต้นทุน หรือลดขั้นตอนกระบวนการผลิต',$fontbody);
        $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
        $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	◯ การพัฒนาผลิตภัณฑ์หรือบริการให้เป็นที่ต้องการของตลาด ',$fontbody);
        $text = $section->addText('		☐ ด้านมาตรฐานผลิตภัณฑ์ อาทิ อย. ฉลากโภชนาการ การตรวจวิเคราะห์ ฯลฯ',$fontbody);
        $text = $section->addText('		☐ ด้านพัฒนาผลิตภัณฑ์ อาทิ ปรับปรุงสูตร ยืดอายุสินค้า ฯลฯ',$fontbody);
        $text = $section->addText('		☐ ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์',$fontbody);
        $text = $section->addText('		☐ ด้านการสร้างแบรนด์',$fontbody);
        $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
        $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	การพัฒนาด้านอื่นๆ ที่เกี่ยวข้อง (ถ้ามี)',$fontbodybold);
        $text = $section->addText('	◯ การเชื่อมโยงแหล่งเงินทุน ',$fontbody);
        $text = $section->addText('		☐ ด้านการเขียนแผนธุรกิจฉบับที่สามารถยื่นขอสินเชื่อกับธนาคาร',$fontbody);
        $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
        $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	◯ การขยายช่องทางการตลาด ',$fontbody);
        $text = $section->addText('		☐ การทดสอบตลาด (Market Testing) งานแสดงสินค้า',$fontbody);
        $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
        $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	◯ อื่นๆ (ระบุ) ',$fontbody);
        $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('11. วิธีการวัดผลการพัฒนาผู้ประกอบการ',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('12. รายละเอียดประมาณการค่าใช้จ่ายในการพัฒนาผู้ประกอบการ (ต่อราย) ',$fontbody);
        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(8000)->addText("รายจ่าย",$fontbody);
        $table->addCell(2000)->addText("จำนวนเงิน",$fontbody);
        $table->addRow();
        $table->addCell(8000)->addText("งบประมาณสนับสนุน",$fontbody);
        $table->addCell(2000)->addText("(ตามจำนวนที่ได้รับ/หัว และใช้จ่ายจริงในแต่ละราย)",$fontbody);
        $table->addRow();
        $table->addCell(8000)->addText("1. ค่าใช้จ่ายในการให้คำปรึกษาแนะนำ การถ่ายทอดองค์ความรู้เชิงลึก",$fontbody);
        $table->addCell(2000);
        foreach ($consult as $c) {
            $table->addRow();
            $table->addCell(8000)->addText($c['voucher_topic'],$fontbody);
            $table->addCell(2000)->addText($c['voucher_cost'],$fontbody,$fontright);
        }
        $table->addRow();
        $table->addCell(8000)->addText("2. ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์/ตรวจวิเคราะห์",$fontbody);
        $table->addCell(2000);
        foreach ($voucher as $v) {
            $table->addRow();
            $table->addCell(8000)->addText($v['voucher_topic'],$fontbody);
            $table->addCell(2000)->addText($v['voucher_cost'],$fontbody,$fontright);
        }
        $sum_cost = VoucherRegister::where('id_card',$id_card)->sum('voucher_cost');
        $vat_sum_cost = ($sum_cost*7)/100;
        $total_vat = $sum_cost+$vat_sum_cost;
        $table->addRow();
        $table->addCell(8000)->addText("ค่าใช้จ่ายรวม (ก่อน Vat.)",$fontbody);
        // $table->addCell(2000)->addText($sum_cost,$fontbody,$fontright);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(8000)->addText("ภาษีมูลค่าเพิ่ม 7%",$fontbody);
        // $table->addCell(2000)->addText($vat_sum_cost,$fontbody,$fontright);
        $table->addCell(2000);
        $table->addRow();
        $table->addCell(8000)->addText("รวมทั้งสิ้น",$fontbody);
        // $table->addCell(2000)->addText($total_vat,$fontbody,$fontright);
        $table->addCell(2000)->addText($sum_cost,$fontbody,$fontright);
        //ส่วนที่ 3
        $section->addPageBreak();
        $text = $section->addText('ส่วนที่ 3 รายงานการพัฒนาผลิตภัณฑ์/บรรจุภัณฑ์',$fontheading,$fontcenter);
        $text = $section->addText('หัวข้อการพัฒนา…………………………………………………………………………………………',$fontbody);
        $text = $section->addText('ขั้นตอน/กระบวนการการพัฒนา',$fontbody);
        $text = $section->addText('	1. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	2. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	3. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	4. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	5. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('	6. …………………………………………………………………………………………',$fontbody);
        $text = $section->addText('รายละเอียดการพัฒนา  (กำหนดแนวทางการปรับปรุง/แก้ไขปัญหา)',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(5000);
        $table->addCell(5000);
        $table->addRow();
        $table->addCell(5000)->addText("รูปก่อนให้คำปรึกษา",$fontbody,$fontcenter);
        $table->addCell(5000)->addText("รูปหลังให้คำปรึกษา",$fontbody,$fontcenter);
        $text = $section->addText('ภาพเปรียบเทียบก่อนและหลังให้คำปรึกษา เช่น พัฒนาผลิตภัณฑ์ใหม่ (รูปผลิตภัณฑ์) การออกแบบบรรจุภัณฑ์ (รูปบรรจุภัณฑ์) ปรับปรุงกระบวนการผลิต (Workflow การดำเนินงาน) ปรับปรุงเครื่องจักร (รูปเครื่องจักร) เป็นต้น',$fontbody14);
        $text = $section->addText('	ผลสำเร็จของงาน',$fontbodybold);
        $text = $section->addText('	สรุปผลดำเนินงานพัฒนาผู้ประกอบการที่เห็นผลชัดเจนในการยกระดับศักยภาพทางธุรกิจของผู้ประกอบการ ด้าน ………………………………………… โดยสามารถแก้ไขปัญหา ………………………………………… และประเมินผลการยกระดับผู้ประกอบการที่คาดหวัง เช่น มียอดขายเพิ่มขึ้น หรือมีการลงทุนเพิ่มขึ้น หรือมูลค่าการลงทุน หรือการจ้างงานเพิ่มขึ้น หรือการลดต้นทุนต่อหน่วย (บาท) การลดขั้นตอนกระบวนการผลิต (ระยะเวลา : นาที) เป็นต้น)',$fontbody);
        $text = $section->addText('ภาพประกอบการการให้คำปรึกษา',$fontheading,$fontcenter);
        $table = $section->addTable($tableStyle);
        $imagestyle2 = array('width' => 200, 'alignment'=>'center');
        $evidence = array();
        foreach ($consult as $c) {
            if ($c['voucher_evidence_1'] != '') {
                array_push($evidence, $c['voucher_evidence_1']);
            }
            if ($c['voucher_evidence_2'] != '') {
                array_push($evidence, $c['voucher_evidence_2']);
            }
            if ($c['voucher_evidence_3'] != '') {
                array_push($evidence, $c['voucher_evidence_3']);
            }
            if ($c['voucher_evidence_4'] != '') {
                array_push($evidence, $c['voucher_evidence_4']);
            }
            if ($c['voucher_evidence_5'] != '') {
                array_push($evidence, $c['voucher_evidence_5']);
            }
        }
        $c_evidence = count($evidence);
        for ($i=0; $i < $c_evidence; $i++) {
            if (($i%2) == 0) {
                $table->addRow();
            }
            $table->addCell(2000)->addImage("./voucher/evidence/".$profile->id_team_owner."/".$profile->id_card."/".$evidence[$i],$imagestyle2);
        }
        $text = $section->addText();
        $text = $section->addText('ผู้ประกอบการ ………………………………………………         ที่ปรึกษา ………………………………………………',$fontbody,$fontcenter);
        $text = $section->addText('             (                                       )                   (                                            )',$fontbody,$fontcenter);
        $text = $section->addText('พยาน 	………………………………………………',$fontbody,$fontcenter);
        $text = $section->addText('(                                       )',$fontbody,$fontcenter);
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $filename = 'รายงานการพัฒนาผู้ประกอบการตามแพ็คเกจ-'.$profile->name.' '.$profile->lastname.'.docx';
        $path = public_path('voucher/reportdoc/' . $filename);
        $objWriter->save($path);
        return response()->download($path);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
