<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Models\Amphur;
use App\Models\BusinessType;
use App\Models\Development;
use App\Models\District;
use App\Models\Profile;
use App\Models\ProductType;
use App\Models\Province;
use App\Models\Sector;
use App\Models\TSIC;
use App\Models\Company;
use App\Models\BusinessTypeSelect;
use App\Models\Document;
use App\Models\DevelopmentDemand;
use App\Models\Product;
use App\Models\VoucherRegister;
use App\Models\EditLog;
use DB;
use Auth;
use Session;
use File;
use ZipArchive;

class EntrepreneurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->id_level == 1) {
            $profile = Profile::all();
            $entrepreneur_team_all = Profile::count();
            $entrepreneur_team_2 = Profile::where('id_comp','like','2%')->count();
            $entrepreneur_team_3 = Profile::where('id_comp','like','3%')->count();
            $entrepreneur_team_4 = Profile::where('id_comp','like','4%')->count();
            $entrepreneur_team_5 = Profile::where('id_comp','like','5%')->count();
            $entrepreneur_team_6 = Profile::where('id_comp','like','6%')->count();
            $data = array(
                'profile' => $profile,
                'entrepreneur_team_all' => $entrepreneur_team_all,
                'entrepreneur_team_2' => $entrepreneur_team_2,
                'entrepreneur_team_3' => $entrepreneur_team_3,
                'entrepreneur_team_4' => $entrepreneur_team_4,
                'entrepreneur_team_5' => $entrepreneur_team_5,
                'entrepreneur_team_6' => $entrepreneur_team_6,
            );
        } else {
            $profile = Profile::where('id_comp','like',Auth::user()->id_team.'%')->get();
            $data = array(
                'profile' => $profile,
            );
        }

        return view('entrepreneur/all_entrepreneur',$data);
    }

    public function entrepreneurteam($id_team)
    {
        $profile = Profile::where('id_team_owner',$id_team)->get();
        $entrepreneur_team_all = Profile::count();
        $entrepreneur_team_2 = Profile::where('id_comp','like','2%')->count();
        $entrepreneur_team_3 = Profile::where('id_comp','like','3%')->count();
        $entrepreneur_team_4 = Profile::where('id_comp','like','4%')->count();
        $entrepreneur_team_5 = Profile::where('id_comp','like','5%')->count();
        $entrepreneur_team_6 = Profile::where('id_comp','like','6%')->count();
        $data = array(
            'profile' => $profile,
            'entrepreneur_team_all' => $entrepreneur_team_all,
            'entrepreneur_team_2' => $entrepreneur_team_2,
            'entrepreneur_team_3' => $entrepreneur_team_3,
            'entrepreneur_team_4' => $entrepreneur_team_4,
            'entrepreneur_team_5' => $entrepreneur_team_5,
            'entrepreneur_team_6' => $entrepreneur_team_6,
            'id_team' => $id_team
        );
        return view('entrepreneur/all_entrepreneur',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_card)
    {
        $amphur = Amphur::all();
        $business_type = BusinessType::all();
        $development = Development::all();
        $district = District::all();
        $product_type = ProductType::all();
        $province = Province::orderBy('PROVINCE_NAME', 'asc')->get();
        $sector = Sector::all();
        $tsic = TSIC::all();
        //$id_card = Session::get('id_card');
        $data = array(
            'amphur' => $amphur,
            'business_type' => $business_type,
            'development' => $development,
            'district' => $district,
            'product_type' => $product_type,
            'province' => $province,
            'sector' => $sector,
            'tsic' => $tsic,
            'id_card' => $id_card
        );
        return view('entrepreneur/form_entrepreneur',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'osmep_number' => 'required|unique:company,osmep_number',
        ]);
        $company = new Company;
        $id_comp = Company::where('id_comp', 'like', Auth::user()->id_team.'%')->pluck('id_comp')->max();
        if ($id_comp=='') {
            $id_comp_1 = Auth::user()->id_team.'000001';
        }
        else
        {
            $id_comp_1 = $id_comp+1;
        }
        $company->id_comp = $id_comp_1;
        $company->company_name = $request->company_name;
        $company->osmep_number = $request->osmep_number;
        $company->business_processing_time = $request->business_processing_time;
        $company->number_of_employment = $request->number_of_employment;
        $company->problems_in_business = $request->problems_in_business;
        $company->tsic = $request->tsic;
        $company->id_sector = $request->id_sector;
        $company->save();

        $document = new Document;
        $id_document = Document::where('id_document', 'like', Auth::user()->id_team.'%')->pluck('id_document')->max();
        if ($id_document=='') {
            $id_document_1 = Auth::user()->id_team.'000001';
        }
        else
        {
            $id_document_1 = $id_document+1;
        }
        $document->id_document = $id_document_1;
        if (!is_dir("voucher/document/".Auth::user()->id_team)) {
            mkdir("voucher/document/".Auth::user()->id_team);
        }
        if($request->file('document')){
            if (!is_dir("voucher/document/".Auth::user()->id_team.'/'.$request->id_card)) {
                mkdir("voucher/document/".Auth::user()->id_team.'/'.$request->id_card);
            }
            $folderdocument = 'voucher/document/'.Auth::user()->id_team.'/'.$request->id_card;
            $filename_doc = $request->id_card.'-สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล.'.$request->file('document')->getClientOriginalExtension();
            $request->file('document')->move($folderdocument,$filename_doc);
            $document->document = $filename_doc;
        }

        if ($request->file('map')) {
            if (!is_dir("voucher/document/".Auth::user()->id_team.'/'.$request->id_card)) {
                mkdir("voucher/document/".Auth::user()->id_team.'/'.$request->id_card);
            }
            $folderdocument = 'voucher/document/'.Auth::user()->id_team.'/'.$request->id_card;
            $imgwidth = 900; //resize image
            $file = $request->file('map');
            $filename_map = $request->id_card.'-แผนที่แสดงที่ตั้งของกิจการ.'.$request->file('map')->getClientOriginalExtension();
            $path = public_path($folderdocument.'/' . $filename_map);
            $img = \Image::make($file->getRealPath());// create instance of Intervention Image
            if($img->width()>$imgwidth){
                // See the docs - http://image.intervention.io/api/resize
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img->resize($imgwidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($path);
            $document->map = $filename_map;
        }
        $document->save();

        $development_demand = new DevelopmentDemand;
        $id_development_demand = DevelopmentDemand::where('id_development_demand', 'like', Auth::user()->id_team.'%')->pluck('id_development_demand')->max();
        if ($id_development_demand=='') {
            $id_development_demand_1 = Auth::user()->id_team.'000001';
        }
        else
        {
            $id_development_demand_1 = $id_development_demand+1;
        }
        $development_demand->id_development_demand = $id_development_demand_1;
        $development_demand->development_detail = $request->development_detail;
        $development_demand->id_development = $request->id_development;
        $development_demand->development_other = $request->development_other;
        $development_demand->save();

        $business_type_select = new BusinessTypeSelect;
        $id_business_type_select = BusinessTypeSelect::where('id_business_type_select', 'like', Auth::user()->id_team.'%')->pluck('id_business_type_select')->max();
        if ($id_business_type_select=='') {
            $id_business_type_select_1 = Auth::user()->id_team.'000001';
        }
        else
        {
            $id_business_type_select_1 = $id_business_type_select+1;
        }
        $business_type_select->id_business_type_select = $id_business_type_select_1;
        $business_type_select->other_please_specify = $request->other_please_specify;
        $business_type_select->id_business_type = $request->id_business_type;
        $business_type_select->save();

        $profile = new Profile;
        $profile->id_card = $request->id_card;
        $profile->prefix = $request->prefix;
        $profile->name = $request->name;
        $profile->lastname = $request->lastname;
        $profile->address = $request->address;
        $profile->moo = $request->moo;
        $profile->soy = $request->soy;
        $profile->road = $request->road;
        $district = District::where('DISTRICT_ID',$request->district)->pluck('DISTRICT_NAME')->first();
        $amphur = Amphur::where('AMPHUR_ID',$request->amphur)->pluck('AMPHUR_NAME')->first();
        $province = Province::where('PROVINCE_ID',$request->province)->pluck('PROVINCE_NAME')->first();
        $profile->district = $district;
        $profile->amphur = $amphur;
        $profile->province = $province;
        $profile->zipcode = $request->zipcode;
        $profile->phone = $request->phone;
        $profile->fax = $request->fax;
        $profile->email = $request->email;
        $profile->id_team_owner = Auth::user()->id_team;
        $profile->id_owner = Auth::user()->id_user;
        $profile->id_comp = $id_comp_1;
        $profile->id_document = $id_document_1;
        $profile->id_development_demand = $id_development_demand_1;
        $profile->id_business_type_select = $id_business_type_select_1;
        $profile->save();
        Session::flash('success','เพิ่มข้อมูลสำเร็จ');
        return redirect('entrepreneur');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        $amphur = Amphur::all();
        $business_type = BusinessType::all();
        $development = Development::all();
        $district = District::all();
        $product_type = ProductType::all();
        $province = Province::all();
        $sector = Sector::all();
        $tsic = TSIC::all();
        $product = Product::where('id_card',$id)->get();
        $voucher_register = VoucherRegister::where('id_card',$id)->get();
        $voucher_cost = VoucherRegister::where('id_card',$id)->pluck('voucher_cost')->sum();
        //$id_card = Session::get('id_card');
        $data = array(
            'profile' => $profile,
            'amphur' => $amphur,
            'business_type' => $business_type,
            'development' => $development,
            'district' => $district,
            'product_type' => $product_type,
            'province' => $province,
            'sector' => $sector,
            'tsic' => $tsic,
            'product' => $product,
            'voucher_register' => $voucher_register,
            'voucher_cost' => $voucher_cost,
        );
        return view('entrepreneur/show_entrepreneur',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        $amphur = Amphur::all();
        $business_type = BusinessType::all();
        $development = Development::all();
        $district = District::all();
        $product_type = ProductType::all();
        $province = Province::all();
        $sector = Sector::all();
        $tsic = TSIC::all();
        $edit_log = EditLog::where('id_card',$id)->orderBy('created_at','DESC')->get();
        //$id_card = Session::get('id_card');
        $data = array(
            'profile' => $profile,
            'amphur' => $amphur,
            'business_type' => $business_type,
            'development' => $development,
            'district' => $district,
            'product_type' => $product_type,
            'province' => $province,
            'sector' => $sector,
            'tsic' => $tsic,
            'edit_log' => $edit_log,
        );
        return view('entrepreneur/form_edit_entrepreneur',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $district = District::where('DISTRICT_ID',$request->district)->pluck('DISTRICT_NAME')->first();
        $amphur = Amphur::where('AMPHUR_ID',$request->amphur)->pluck('AMPHUR_NAME')->first();
        $province = Province::where('PROVINCE_ID',$request->province)->pluck('PROVINCE_NAME')->first();
        if ($request->field_prefix != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_prefix;
            $edit_log->text_old = $profile->prefix;
            $edit_log->text_new = $request->prefix;
            $edit_log->save();
        }
        if ($request->field_name != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_name;
            $edit_log->text_old = $profile->name;
            $edit_log->text_new = $request->name;
            $edit_log->save();
        }
        if ($request->field_lastname != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_lastname;
            $edit_log->text_old = $profile->lastname;
            $edit_log->text_new = $request->lastname;
            $edit_log->save();
        }
        if ($request->field_address != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_address;
            $edit_log->text_old = $profile->address;
            $edit_log->text_new = $request->address;
            $edit_log->save();
        }
        if ($request->field_moo != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_moo;
            $edit_log->text_old = $profile->moo;
            $edit_log->text_new = $request->moo;
            $edit_log->save();
        }
        if ($request->field_soy != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_soy;
            $edit_log->text_old = $profile->soy;
            $edit_log->text_new = $request->soy;
            $edit_log->save();
        }
        if ($request->field_road != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_road;
            $edit_log->text_old = $profile->road;
            $edit_log->text_new = $request->road;
            $edit_log->save();
        }
        if ($request->field_district != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_district;
            $edit_log->text_old = $profile->district;
            $edit_log->text_new = $request->district;
            $edit_log->save();
        }
        if ($request->field_district != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_district;
            $edit_log->text_old = $profile->district;
            $edit_log->text_new = $district;
            $edit_log->save();
        }
        if ($request->field_amphur != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_amphur;
            $edit_log->text_old = $profile->amphur;
            $edit_log->text_new = $amphur;
            $edit_log->save();
        }
        if ($request->field_province != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_province;
            $edit_log->text_old = $profile->province;
            $edit_log->text_new = $province;
            $edit_log->save();
        }
        if ($request->field_zipcode != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_zipcode;
            $edit_log->text_old = $profile->zipcode;
            $edit_log->text_new = $request->zipcode;
            $edit_log->save();
        }
        if ($request->field_phone != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_phone;
            $edit_log->text_old = $profile->phone;
            $edit_log->text_new = $request->phone;
            $edit_log->save();
        }
        if ($request->field_fax != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_fax;
            $edit_log->text_old = $profile->fax;
            $edit_log->text_new = $request->fax;
            $edit_log->save();
        }
        if ($request->field_email != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_email;
            $edit_log->text_old = $profile->email;
            $edit_log->text_new = $request->email;
            $edit_log->save();
        }

        $profile->prefix = $request->prefix;
        $profile->name = $request->name;
        $profile->lastname = $request->lastname;
        $profile->address = $request->address;
        $profile->moo = $request->moo;
        $profile->soy = $request->soy;
        $profile->road = $request->road;
        $profile->district = $district;
        $profile->amphur = $amphur;
        $profile->province = $province;
        $profile->zipcode = $request->zipcode;
        $profile->phone = $request->phone;
        $profile->fax = $request->fax;
        $profile->email = $request->email;
        $profile->save();


        $company = Company::find($profile->id_comp);
        if ($request->field_company_name != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_company_name;
            $edit_log->text_old = $company->company_name;
            $edit_log->text_new = $request->company_name;
            $edit_log->save();
        }
        if ($request->field_osmep_number != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_osmep_number;
            $edit_log->text_old = $company->osmep_number;
            $edit_log->text_new = $request->osmep_number;
            $edit_log->save();
        }
        if ($request->field_business_processing_time != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_business_processing_time;
            $edit_log->text_old = $company->business_processing_time;
            $edit_log->text_new = $request->business_processing_time;
            $edit_log->save();
        }
        if ($request->field_number_of_employment != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_number_of_employment;
            $edit_log->text_old = $company->number_of_employment;
            $edit_log->text_new = $request->number_of_employment;
            $edit_log->save();
        }
        if ($request->field_problems_in_business != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_problems_in_business;
            $edit_log->text_old = $company->problems_in_business;
            $edit_log->text_new = $request->problems_in_business;
            $edit_log->save();
        }
        if ($request->field_tsic != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_tsic;
            $edit_log->text_old = $company->tsic;
            $edit_log->text_new = $request->tsic;
            $edit_log->save();
        }
        if ($request->field_id_sector != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_id_sector;
            $edit_log->text_old = $company->id_sector;
            $edit_log->text_new = $request->id_sector;
            $edit_log->save();
        }

        $company->company_name = $request->company_name;
        $company->osmep_number = $request->osmep_number;
        $company->business_processing_time = $request->business_processing_time;
        $company->number_of_employment = $request->number_of_employment;
        $company->problems_in_business = $request->problems_in_business;
        $company->tsic = $request->tsic;
        $company->id_sector = $request->id_sector;
        $company->save();

        $document = Document::find($profile->id_document);
        if (!is_dir("voucher/document/".$profile->id_team_owner)) {
            mkdir("voucher/document/".$profile->id_team_owner);
        }
        if($request->file('document')){
            @unlink('voucher/document/'.$profile->id_team_owner.'/'.$id.'/'.$document->document);
            if (!is_dir("voucher/document/".$profile->id_team_owner.'/'.$id)) {
                mkdir("voucher/document/".$profile->id_team_owner.'/'.$id);
            }
            $folderdocument = 'voucher/document/'.$profile->id_team_owner.'/'.$id;
            $filename_doc = $id.'-สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล.'.$request->file('document')->getClientOriginalExtension();
            $request->file('document')->move($folderdocument,$filename_doc);
            $document->document = $filename_doc;

            if ($request->field_document != '') {
                $edit_log = new EditLog;
                $edit_log->id_card = $id;
                $edit_log->id_user_edit = Auth::user()->id_user;
                $edit_log->field_name = $request->field_document;
                $edit_log->text_old = $document->document;
                $edit_log->text_new = $filename_doc;
                $edit_log->save();
            }
        }

        if ($request->file('map')) {
            @unlink('voucher/document/'.$profile->id_team_owner.'/'.$id.'/'.$document->map);
            if (!is_dir("voucher/document/".$profile->id_team_owner.'/'.$id)) {
                mkdir("voucher/document/".$profile->id_team_owner.'/'.$id);
            }
            $folderdocument = 'voucher/document/'.$profile->id_team_owner.'/'.$id;
            $imgwidth = 900; //resize image
            $file = $request->file('map');
            $filename_map = $id.'-แผนที่แสดงที่ตั้งของกิจการ.'.$request->file('map')->getClientOriginalExtension();
            $path = public_path($folderdocument.'/' . $filename_map);
            $img = \Image::make($file->getRealPath());// create instance of Intervention Image
            if($img->width()>$imgwidth){
                // See the docs - http://image.intervention.io/api/resize
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img->resize($imgwidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save($path);
            $document->map = $filename_map;

            if ($request->field_map != '') {
                $edit_log = new EditLog;
                $edit_log->id_card = $id;
                $edit_log->id_user_edit = Auth::user()->id_user;
                $edit_log->field_name = $request->field_map;
                $edit_log->text_old = $document->map;
                $edit_log->text_new = $filename_map;
                $edit_log->save();
            }
        }
        $document->save();

        $development_demand = DevelopmentDemand::find($profile->id_development_demand);
        if ($request->field_development_detail != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_development_detail;
            $edit_log->text_old = $development_demand->development_detail;
            $edit_log->text_new = $request->development_detail;
            $edit_log->save();
        }
        if ($request->field_id_development != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_id_development;
            $edit_log->text_old = $development_demand->id_development;
            $edit_log->text_new = $request->id_development;
            $edit_log->save();
        }
        if ($request->field_development_other != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_development_other;
            $edit_log->text_old = $development_demand->development_other;
            $edit_log->text_new = $request->development_other;
            $edit_log->save();
        }
        $development_demand->development_detail = $request->development_detail;
        $development_demand->id_development = $request->id_development;
        $development_demand->development_other = $request->development_other;
        $development_demand->save();

        $business_type_select = BusinessTypeSelect::find($profile->id_business_type_select);
        if ($request->field_other_please_specify != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_other_please_specify;
            $edit_log->text_old = $business_type_select->other_please_specify;
            $edit_log->text_new = $request->other_please_specify;
            $edit_log->save();
        }
        if ($request->field_id_business_type != '') {
            $edit_log = new EditLog;
            $edit_log->id_card = $id;
            $edit_log->id_user_edit = Auth::user()->id_user;
            $edit_log->field_name = $request->field_id_business_type;
            $edit_log->text_old = $business_type_select->id_business_type;
            $edit_log->text_new = $request->id_business_type;
            $edit_log->save();
        }
        $business_type_select->other_please_specify = $request->other_please_specify;
        $business_type_select->id_business_type = $request->id_business_type;
        $business_type_select->save();
        Artisan::call('view:clear');
        Session::flash('success','แก้ไขข้อมูลสำเร็จ');
        return redirect('entrepreneur/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);

        $company = Company::find($profile->id_comp);
        $company->delete();

        $document = Document::find($profile->id_document);
        @unlink('voucher/document/'.$profile->id_team_owner.'/'.$id.'/'.$document->document);
        @unlink('voucher/document/'.$profile->id_team_owner.'/'.$id.'/'.$document->map);
        $document->delete();

        $development_demand = DevelopmentDemand::find($profile->id_development_demand);
        $development_demand->delete();

        $business_type_select = BusinessTypeSelect::find($profile->id_business_type_select);
        $business_type_select->delete();

        $product = Product::where('id_card',$id)->get();
        if (count($product) > 0) {
            foreach ($product as $p) {
                @unlink("voucher/product/".substr($p['id_product'],0,1)."/".$p['product_image']);
            }
        }
        Product::where('id_card',$id)->delete();

        $voucher = VoucherRegister::where('id_card',$id)->get();
        if (count($voucher) > 0) {
            foreach ($voucher as $v) {
                @unlink("voucher/evidence/".substr($v['id_voucher_register'],0,1)."/".$v['id_card']."/".$v['voucher_evidence']);
            }
        }
        VoucherRegister::where('id_card',$id)->delete();

        $profile->delete();
        Session::flash('success','ลบข้อมูลสำเร็จ');
        return redirect('entrepreneur');
    }

    //ตัวเลือกจังหวัด
    function fetchprofile(Request $request){
        $id_province = $request->get('select');
        $result = array();
        $query = DB::table('province')
                ->join('amphur','province.PROVINCE_ID','=','amphur.PROVINCE_ID')
                ->select('amphur.AMPHUR_NAME','amphur.AMPHUR_ID')
                ->where('province.PROVINCE_ID',$id_province)
                ->orderBy('amphur.AMPHUR_NAME', 'asc')
                // ->groupBy('amphur.AMPHUR_NAME')
                ->get();
        $output = '<option value="">กรุณาเลือกเขต/อำเภอ</option>';
        foreach ($query as $row) {
            $output.='<option value="'.$row->AMPHUR_ID.'">'.$row->AMPHUR_NAME.'</option>';
        }
        echo $output;
    }

    function fetchprofile2(Request $request){
        $id_amphur = $request->get('select');
        $result = array();
        $query = DB::table('amphur')
                ->join('district','amphur.AMPHUR_ID','=','district.AMPHUR_ID')
                ->select('district.DISTRICT_NAME','district.DISTRICT_ID')
                ->where('amphur.AMPHUR_ID',$id_amphur)
                ->orderBy('district.DISTRICT_NAME', 'asc')
                //->groupBy('districts.district_thai')
                ->get();
        $output = '<option value="">กรุณาเลือกแขวง/ตำบล</option>';
        foreach ($query as $row) {
            $output.='<option value="'.$row->DISTRICT_ID.'">'.$row->DISTRICT_NAME.'</option>';
        }
        echo $output;
    }

    function fetchprofile3(Request $request){
        $id_district = $request->get('select');
        $result = array();
        $query = DB::table('district')->select('ZIPCODE')->where('district.DISTRICT_ID',$id_district)->get();
        $output = '<option value="">กรุณาเลือกรหัสไปรษณีย์</option>';
        foreach ($query as $row) {
            $output.='<option value="'.$row->ZIPCODE.'">'.$row->ZIPCODE.'</option>';
        }
        echo $output;
    }

    function fetchosmepnumber(Request $request){
        $osmep_number = $request->get('select');
        $result = array();
        $findcompany = Company::where('osmep_number',$osmep_number)->first();
        $findprofile = Profile::where('id_comp',$findcompany->id_comp)->get();
        if (count($findprofile) == 0) {
            $findcompany->delete();
        }
        $query = Company::where('osmep_number',$osmep_number)->get();
        //$query = DB::table('company')->select('osmep_number')->where('company.osmep_number',$osmep_number)->pluck('osmep_number');
        //$query = Profile::where('id_card',1750300058128)->pluck('id_card');
        if (count($query) == 0) {
            $output = '<span class="success text-success">ยังไม่มีเลขสมาชิก สสว. นี้ในระบบ สามารถบันทึกได้</span> <br>
                        <script type="text/javascript">
                        $("#btn_submit").removeAttr("disabled");
                        </script>';
        } else {
            $output = '<span class="error text-danger">มีเลขสมาชิก สสว. นี้ในระบบ ไม่สามารถบันทึกได้</span> <br>
                        <script type="text/javascript">
                        $("#btn_submit").attr("disabled", "disabled");
                        </script>';
        }
        echo $output;
    }

    public function checkidcard(Request $request)
    {
        $profile = Profile::find($request->id_card);
        if ($profile == '') {
            //return redirect('entrepreneur/create')->with('id_card', $request->id_card);
            return redirect('entrepreneur/create/'.$request->id_card);
        } else {
            return redirect('entrepreneur')->with('id_card', $request->id_card)->with('status', $request->id_card.' เลขบัตรประชาชนมีอยู่ในระบบแล้ว');
        }
    }

    public function downloadreportword(Request $request)
    {
        $request->validate([
            'id_card' => 'required',
        ]);
        File::deleteDirectory('voucher/reportdoc_temp');
        // @rmdir('voucher/reportdoc_temp');
        if (!is_dir("voucher/reportdoc_temp")) {
            mkdir("voucher/reportdoc_temp");
        }
        $count_id_card = count($request->id_card);
        //return $request->id_card[1];
        for ($p=0; $p < $count_id_card; $p++) {
            $id_card = $request->id_card[$p];
            $profile = Profile::find($id_card);
            $consult = VoucherRegister::where('id_card',$id_card)->whereNotNull('id_advisor')->get();
            $voucher = VoucherRegister::where('id_card',$id_card)->whereNull('id_advisor')->get();
            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $fontheading = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'bgColor'=>'FFFF00','lineHeight'=> 1.0);
            $fontheadingnogb = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'lineHeight'=> 1.0);
            $fontbody = array('name'=>'TH Sarabun New','size' => 16,'lineHeight'=> 1.0);
            $fontbodybold = array('name'=>'TH Sarabun New','size' => 16,'bold' => true,'lineHeight'=> 1.0);
            $fontbody14 = array('name'=>'TH Sarabun New','size' => 14,'lineHeight'=> 1.0);
            $tableStyle = array('borderSize' => 1, 'borderColor' => '#000000','position'=>'center','cellMargin'  => 50);
            $tableLogoStyle = array('position'=>'center');
            $cellRowSpan = array('vMerge' => 'restart');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan = array('gridSpan' => 5);
            $imagestyle = array('width' => 300, 'alignment'=>'center');
            $logostyle = array('height' => 100, 'alignment'=>'center');
            $fontcenter = array('align'=>'center');
            $fontright = array('align'=>'right');
            $section = $phpWord->addSection();
            switch ($profile->id_team_owner) {
                case '2':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CEA.png",$logostyle);
                    break;
                case '3':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CLT.png",$logostyle);
                    break;
                case '4':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CMU.png",$logostyle);
                    break;
                case '5':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/TISTR.png",$logostyle);
                    break;
                case '6':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/NFI.png",$logostyle);
                    break;
                default:
                    $section->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    break;
            }
            $text = $section->addText('โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์',$fontheadingnogb,$fontcenter);
            $text = $section->addText('สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน',$fontheadingnogb,$fontcenter);
            $text = $section->addText('ชื่อผู้ประกอบการ '.$profile->prefix.$profile->name.' '.$profile->lastname,$fontbody,$fontcenter);
            $text = $section->addText('ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name,$fontbody,$fontcenter);
            $text = $section->addText('จังหวัด '.$profile->province,$fontbody,$fontcenter);
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 1 ข้อมูลพื้นฐานของผู้ประกอบการ/ผลิตภัณฑ์',$fontheading,$fontcenter);
            $text = $section->addText('1. ชื่อ-นามสกุล '.$profile->prefix.$profile->name.' '.$profile->lastname.' อายุ..................ปี ตำแหน่ง................................................................',$fontbody);
            $text = $section->addText('	ระดับการศึกษาและสถาบัน..............................................................................................',$fontbody);
            $text = $section->addText('	ประสบการณ์/ความถนัดของผู้บริหาร..............................................................................................',$fontbody);
            $text = $section->addText('	ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name.' ปีที่ก่อตั้ง..................',$fontbody);
            $text = $section->addText('	ระยะเวลาดำเนินธุรกิจ '.$profile->company->business_processing_time.' ปี',$fontbody);
            $text = $section->addText('	ที่ตั้งธุรกิจ..............................................................................................',$fontbody);
            $text = $section->addText('	โทรศัพท์ '.$profile->phone.' Line ID:................................................................',$fontbody);
            $text = $section->addText('	ประเภท/ รายละเอียดกิจการ..............................................................................................',$fontbody);
            $text = $section->addText('2. การจดทะเบียนจัดตั้งธุรกิจ',$fontbody);
            switch ($profile->business_type_select->id_business_type) {
                case '1':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '2':
                    $text = $section->addText('	🗹 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '3':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '4':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) กลุ่มสหกรณ์',$fontbody);
                    break;
                case '5':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) '.$profile->business_type_select->other_please_specify,$fontbody);
                    break;
                default:
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
            }
            $text = $section->addText('	หมายเหตุ: การจดทะเบียนจัดตั้งธุรกิจ ได้แก่',$fontbody14);
            $text = $section->addText('	1) บุคคลธรรมดา ได้แก่ จดทะเบียนพาณิชย์ และจดทะเบียนการค้า',$fontbody14);
            $text = $section->addText('	2) นิติบุคคล ได้แก่ บริษัทจำกัด และ ห้างส่วนจำกัด',$fontbody14);
            $text = $section->addText('	3) วิสาหกิจชุมชน',$fontbody14);
            $text = $section->addText('3. วิธีการบริหารกิจการ :..............................................................................................',$fontbody);
            $text = $section->addText('	☐ บริหารโดยเจ้าของคนเดียว			🗹 บริหารโดยเจ้าของและพี่น้อง, หรือญาติ',$fontbody);
            $text = $section->addText('	☐ บริหารโดยมีการว่าจ้างบุคคลภายนอก		☐ อื่นๆ (ระบุ)..................',$fontbody);
            $text = $section->addText('4. ประเภทแพคเกจ (เลือกมาหนึ่งอย่างที่เด่นชัดที่สุด)',$fontbody);
            $text = $section->addText('	☐ 1) พัฒนามาตรฐานผลิตภัณฑ์สมุนไพร/เครื่องสำอาง',$fontbody);
            $text = $section->addText('	☐ 2) พัฒนามาตรฐานผลิตภัณฑ์อาหาร เครื่องดื่ม เกษตรแปรรูป',$fontbody);
            $text = $section->addText('	☐ 3) พัฒนามาตรฐานผลิตภัณฑ์ที่มีมูลค่าสูง',$fontbody);
            $text = $section->addText('	☐ 4) พัฒนาการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์/สร้างแบนรด์',$fontbody);
            $text = $section->addText('5. ลักษณะกิจการ / โครงการ',$fontbody);
            $text = $section->addText('	☐ 1) เริ่มกิจการใหม่	☐ 2) ขยายกิจการ	☐ 3) ปรับปรุงกิจการ',$fontbody);
            $text = $section->addText('	คาดว่าจะเริ่มดำเนินการ ประมาณเดือน..................ปี',$fontbody);
            $text = $section->addText('6. ลักษณะสินค้า/บริการที่เข้ารับการพัฒนา  (กรณีที่มีภาพตัวอย่างมากกว่านี้ อาจทำเป็นเอกสารแนบ) ',$fontbody);
            $count_product = Product::where('id_card',$id_card)->count();
            $product = Product::where('id_card',$id_card)->first();
            if ($count_product <> 0) {
                $section->addImage("./voucher/product/".$profile->id_team_owner.'/'.$product->product_image,$imagestyle);
                $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
                $text = $section->addText($product->product_detail,$fontbody);
            } else {
                $section->addImage("./dist/img/block_image.png",$imagestyle);
                $text = $section->addText('รูปภาพสินค้า/บริการ',$fontbody,$fontcenter);
                $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            }
            $text = $section->addText('7. ลักษณะบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('รูปภาพบรรจุภัณฑ์ (ถ้ามี)',$fontbody,$fontcenter);
            $text = $section->addText('รายละเอียดบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('8. ยอดขายโดยประมาณ (บาทต่อเดือน)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('9. แผนผังที่ตั้งกิจการ (ระบุพิกัด GPS  X………………Y………………)',$fontbody);
            $section->addImage("./voucher/document/".$profile->id_team_owner.'/'.$profile->id_card.'/'.$profile->document->map,$imagestyle);
            //ส่วนที่สอง
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 2 รายงานการวิเคราะห์สภาพปัญหา/ความต้องการรับการพัฒนา',$fontheading,$fontcenter);
            if ($count_product <> 0) {
                $text = $section->addText('1. สินค้า/บริการหลัก : '.$product->product_name,$fontbody);
            } else {
                $text = $section->addText('1. สินค้า/บริการหลัก :…………………………………………………………………………………',$fontbody);
            }
            $text = $section->addText('2. ชื่อตราสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('LOGO (ถ้ามี)',$fontbody);
            $text = $section->addText('3. มาตรฐานสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
            $text = $section->addText('4. กำลังการผลิต/บริการสูงสุดต่อปี ……………………………………หน่วย……………………………………',$fontbody);
            $text = $section->addText('กำลังการผลิต/บริการจริงต่อปี ……………………………………หน่วย……………………………………',$fontbody);
            $text = $section->addText('คิดเป็น……………………………………% ปริมาณการผลิตที่คาดหวัง ปี 2564……………………………………',$fontbody);
            $text = $section->addText('5. บุคลากร (Man) ได้แก่',$fontbody);
            $text = $section->addText('	(1) บุคลากรการผลิต',$fontbody);
            $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('	(2) บุคลากรขายและบริหาร',$fontbody);
            $text = $section->addText('		☐ ผู้บริหาร…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('6. เครื่องจักร (Machine) (ถ้ามี)',$fontbody);

            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(2000)->addText("รายการ",$fontbody);
            $table->addCell(2000)->addText("ราคาต่อหน่วย (บาท)",$fontbody);
            $table->addCell(2000)->addText("จำนวน",$fontbody);
            $table->addCell(2000)->addText("ราคารวม (บาท)",$fontbody);
            $table->addCell(2000)->addText("อายุการใช้งาน (ปี)",$fontbody);
            $table->addCell(2000)->addText("ค่าเสื่อมราคา (บาทต่อปี)",$fontbody);
            $table->addCell(2000)->addText("ลักษณะการใช้งาน",$fontbody);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(4000, $cellColSpan)->addText("รวม ค่าเสื่อมราคาในการผลิต",$fontbody);
            $table->addCell(2000);
            $table->addCell(2000)->addText("บาทต่อปี ",$fontbody);

            $text = $section->addText('7. วัตถุดิบหลัก (Material) ได้แก่',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(3000)->addText("ลำดับ",$fontbody);
            $table->addCell(3000)->addText("ชื่อวัตถุดิบ",$fontbody);
            $table->addCell(3000)->addText("สถานที่",$fontbody);
            $table->addRow();
            $table->addCell(3000)->addText("1",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("2",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("3",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("4",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("5",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $text = $section->addText('8. กระบวนการผลิต (Method)',$fontbody);
            $text = $section->addText('	ขั้นตอนกระบวนการผลิต (Process Flow) (อธิบายขั้นตอนพร้อมภาพประกอบ)',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('9. การขาย/การตลาด',$fontbody);
            $text = $section->addText('ลูกค้าเป้าหมาย :',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('ช่องทางการจัดจำหน่าย : ',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('10. สภาพปัญหาเบื้องต้นและข้อเสนอแนะเพื่อการพัฒนาผลิตภัณฑ์ (กรุณา X ไม่จำกัดจำนวนข้อ)',$fontbody);
            $text = $section->addText('	◯ การพัฒนาศักยภาพการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านมาตรฐานการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านการปรับปรุงกระบวนการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านออกแบบปรับปรุงเครื่องจักรเพื่อการลดต้นทุน หรือลดขั้นตอนกระบวนการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ การพัฒนาผลิตภัณฑ์หรือบริการให้เป็นที่ต้องการของตลาด ',$fontbody);
            $text = $section->addText('		☐ ด้านมาตรฐานผลิตภัณฑ์ อาทิ อย. ฉลากโภชนาการ การตรวจวิเคราะห์ ฯลฯ',$fontbody);
            $text = $section->addText('		☐ ด้านพัฒนาผลิตภัณฑ์ อาทิ ปรับปรุงสูตร ยืดอายุสินค้า ฯลฯ',$fontbody);
            $text = $section->addText('		☐ ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์',$fontbody);
            $text = $section->addText('		☐ ด้านการสร้างแบรนด์',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	การพัฒนาด้านอื่นๆ ที่เกี่ยวข้อง (ถ้ามี)',$fontbodybold);
            $text = $section->addText('	◯ การเชื่อมโยงแหล่งเงินทุน ',$fontbody);
            $text = $section->addText('		☐ ด้านการเขียนแผนธุรกิจฉบับที่สามารถยื่นขอสินเชื่อกับธนาคาร',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ การขยายช่องทางการตลาด ',$fontbody);
            $text = $section->addText('		☐ การทดสอบตลาด (Market Testing) งานแสดงสินค้า',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ อื่นๆ (ระบุ) ',$fontbody);
            $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('11. วิธีการวัดผลการพัฒนาผู้ประกอบการ',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('12. รายละเอียดประมาณการค่าใช้จ่ายในการพัฒนาผู้ประกอบการ (ต่อราย) ',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(8000)->addText("รายจ่าย",$fontbody);
            $table->addCell(2000)->addText("จำนวนเงิน",$fontbody);
            $table->addRow();
            $table->addCell(8000)->addText("งบประมาณสนับสนุน",$fontbody);
            $table->addCell(2000)->addText("(ตามจำนวนที่ได้รับ/หัว และใช้จ่ายจริงในแต่ละราย)",$fontbody);
            $table->addRow();
            $table->addCell(8000)->addText("1. ค่าใช้จ่ายในการให้คำปรึกษาแนะนำ การถ่ายทอดองค์ความรู้เชิงลึก",$fontbody);
            $table->addCell(2000);
            foreach ($consult as $c) {
                $table->addRow();
                $table->addCell(8000)->addText($c['voucher_topic'],$fontbody);
                $table->addCell(2000)->addText($c['voucher_cost'],$fontbody,$fontright);
            }
            $table->addRow();
            $table->addCell(8000)->addText("2. ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์/ตรวจวิเคราะห์",$fontbody);
            $table->addCell(2000);
            foreach ($voucher as $v) {
                $table->addRow();
                $table->addCell(8000)->addText($v['voucher_topic'],$fontbody);
                $table->addCell(2000)->addText($v['voucher_cost'],$fontbody,$fontright);
            }
            $sum_cost = VoucherRegister::where('id_card',$id_card)->sum('voucher_cost');
            $vat_sum_cost = ($sum_cost*7)/100;
            $total_vat = $sum_cost+$vat_sum_cost;
            $table->addRow();
            $table->addCell(8000)->addText("ค่าใช้จ่ายรวม (ก่อน Vat.)",$fontbody);
            $table->addCell(2000)->addText($sum_cost,$fontbody,$fontright);
            $table->addRow();
            $table->addCell(8000)->addText("ภาษีมูลค่าเพิ่ม 7%",$fontbody);
            $table->addCell(2000)->addText($vat_sum_cost,$fontbody,$fontright);
            $table->addRow();
            $table->addCell(8000)->addText("รวมทั้งสิ้น",$fontbody);
            $table->addCell(2000)->addText($total_vat,$fontbody,$fontright);
            //ส่วนที่ 3
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 3 รายงานการพัฒนาผลิตภัณฑ์/บรรจุภัณฑ์',$fontheading,$fontcenter);
            $text = $section->addText('หัวข้อการพัฒนา…………………………………………………………………………………………',$fontbody);
            $text = $section->addText('ขั้นตอน/กระบวนการการพัฒนา',$fontbody);
            $text = $section->addText('	1. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	2. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	3. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	4. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	5. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	6. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('รายละเอียดการพัฒนา  (กำหนดแนวทางการปรับปรุง/แก้ไขปัญหา)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(5000);
            $table->addCell(5000);
            $table->addRow();
            $table->addCell(5000)->addText("รูปก่อนให้คำปรึกษา",$fontbody,$fontcenter);
            $table->addCell(5000)->addText("รูปหลังให้คำปรึกษา",$fontbody,$fontcenter);
            $text = $section->addText('ภาพเปรียบเทียบก่อนและหลังให้คำปรึกษา เช่น พัฒนาผลิตภัณฑ์ใหม่ (รูปผลิตภัณฑ์) การออกแบบบรรจุภัณฑ์ (รูปบรรจุภัณฑ์) ปรับปรุงกระบวนการผลิต (Workflow การดำเนินงาน) ปรับปรุงเครื่องจักร (รูปเครื่องจักร) เป็นต้น',$fontbody14);
            $text = $section->addText('	ผลสำเร็จของงาน',$fontbodybold);
            $text = $section->addText('	สรุปผลดำเนินงานพัฒนาผู้ประกอบการที่เห็นผลชัดเจนในการยกระดับศักยภาพทางธุรกิจของผู้ประกอบการ ด้าน ………………………………………… โดยสามารถแก้ไขปัญหา ………………………………………… และประเมินผลการยกระดับผู้ประกอบการที่คาดหวัง เช่น มียอดขายเพิ่มขึ้น หรือมีการลงทุนเพิ่มขึ้น หรือมูลค่าการลงทุน หรือการจ้างงานเพิ่มขึ้น หรือการลดต้นทุนต่อหน่วย (บาท) การลดขั้นตอนกระบวนการผลิต (ระยะเวลา : นาที) เป็นต้น)',$fontbody);
            $text = $section->addText('ภาพประกอบการการให้คำปรึกษา',$fontheading,$fontcenter);
            $table = $section->addTable($tableStyle);
            $imagestyle2 = array('width' => 200, 'alignment'=>'center');
            $evidence = array();
            foreach ($consult as $c) {
                if ($c['voucher_evidence_1'] != '') {
                    array_push($evidence, $c['voucher_evidence_1']);
                }
                if ($c['voucher_evidence_2'] != '') {
                    array_push($evidence, $c['voucher_evidence_2']);
                }
                if ($c['voucher_evidence_3'] != '') {
                    array_push($evidence, $c['voucher_evidence_3']);
                }
                if ($c['voucher_evidence_4'] != '') {
                    array_push($evidence, $c['voucher_evidence_4']);
                }
                if ($c['voucher_evidence_5'] != '') {
                    array_push($evidence, $c['voucher_evidence_5']);
                }
            }
            $c_evidence = count($evidence);
            for ($i=0; $i < $c_evidence; $i++) {
                if (($i%2) == 0) {
                    $table->addRow();
                }
                $table->addCell(2000)->addImage("./voucher/evidence/".$profile->id_team_owner."/".$profile->id_card."/".$evidence[$i],$imagestyle2);
            }
            $text = $section->addText();
            $text = $section->addText('ผู้ประกอบการ ………………………………………………         ที่ปรึกษา ………………………………………………',$fontbody,$fontcenter);
            $text = $section->addText('             (                                       )                   (                                            )',$fontbody,$fontcenter);
            $text = $section->addText('พยาน 	………………………………………………',$fontbody,$fontcenter);
            $text = $section->addText('(                                       )',$fontbody,$fontcenter);
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $filename = $id_card.'-รายงานการพัฒนาผู้ประกอบการตามแพ็คเกจ-'.$profile->name.' '.$profile->lastname.'.docx';
            $path = public_path('voucher/reportdoc_temp/' . $filename);
            $objWriter->save($path);
        }
        $zip = new ZipArchive;
        $fileName = 'รายงาน.zip';
        @unlink($fileName);

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(public_path('voucher/reportdoc_temp'));
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        return response()->download(public_path($fileName));
        //return $request->id_card;
        //return $count_id_card
    }

    public function downloadreportwordall(Request $request)
    {
        File::deleteDirectory('voucher/reportdoc_temp');
        // @rmdir('voucher/reportdoc_temp');
        if (!is_dir("voucher/reportdoc_temp")) {
            mkdir("voucher/reportdoc_temp");
        }
        if ($request->data == 1) {
            $profile_select = Profile::all();
        } else {
            $profile_select = Profile::where('id_team_owner',$request->data)->get();
        }
        //$count_id_card = count($request->id_card);
        //return $request->id_card[1];
        foreach ($profile_select as $ps) {
            $id_card = $ps['id_card'];
            $profile = Profile::find($id_card);
            $consult = VoucherRegister::where('id_card',$id_card)->whereNotNull('id_advisor')->get();
            $voucher = VoucherRegister::where('id_card',$id_card)->whereNull('id_advisor')->get();
            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $fontheading = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'bgColor'=>'FFFF00','lineHeight'=> 1.0);
            $fontheadingnogb = array('name'=>'TH Sarabun New','size' => 20,'bold' => true,'lineHeight'=> 1.0);
            $fontbody = array('name'=>'TH Sarabun New','size' => 16,'lineHeight'=> 1.0);
            $fontbodybold = array('name'=>'TH Sarabun New','size' => 16,'bold' => true,'lineHeight'=> 1.0);
            $fontbody14 = array('name'=>'TH Sarabun New','size' => 14,'lineHeight'=> 1.0);
            $tableStyle = array('borderSize' => 1, 'borderColor' => '#000000','position'=>'center','cellMargin'  => 50);
            $tableLogoStyle = array('position'=>'center');
            $cellRowSpan = array('vMerge' => 'restart');
            $cellRowContinue = array('vMerge' => 'continue');
            $cellColSpan = array('gridSpan' => 5);
            $imagestyle = array('width' => 300, 'alignment'=>'center');
            $logostyle = array('height' => 100, 'alignment'=>'center');
            $fontcenter = array('align'=>'center');
            $fontright = array('align'=>'right');
            $section = $phpWord->addSection();
            switch ($profile->id_team_owner) {
                case '2':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CEA.png",$logostyle);
                    break;
                case '3':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CLT.png",$logostyle);
                    break;
                case '4':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/CMU.png",$logostyle);
                    break;
                case '5':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/TISTR.png",$logostyle);
                    break;
                case '6':
                    $table = $section->addTable($tableLogoStyle);
                    $table->addRow();
                    $table->addCell(2000)->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    $table->addCell(2000)->addImage("./dist/img/logo/NFI.png",$logostyle);
                    break;
                default:
                    $section->addImage("./dist/img/logo/OSMEP.jpg",$logostyle);
                    break;
            }
            $text = $section->addText('โครงการ Voucher เพื่อการพัฒนาผลิตภัณฑ์',$fontheadingnogb,$fontcenter);
            $text = $section->addText('สำหรับวิสาหกิจรายย่อย/วิสาหกิจชุมชน',$fontheadingnogb,$fontcenter);
            $text = $section->addText('ชื่อผู้ประกอบการ '.$profile->prefix.$profile->name.' '.$profile->lastname,$fontbody,$fontcenter);
            $text = $section->addText('ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name,$fontbody,$fontcenter);
            $text = $section->addText('จังหวัด '.$profile->province,$fontbody,$fontcenter);
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 1 ข้อมูลพื้นฐานของผู้ประกอบการ/ผลิตภัณฑ์',$fontheading,$fontcenter);
            $text = $section->addText('1. ชื่อ-นามสกุล '.$profile->prefix.$profile->name.' '.$profile->lastname.' อายุ..................ปี ตำแหน่ง................................................................',$fontbody);
            $text = $section->addText('	ระดับการศึกษาและสถาบัน..............................................................................................',$fontbody);
            $text = $section->addText('	ประสบการณ์/ความถนัดของผู้บริหาร..............................................................................................',$fontbody);
            $text = $section->addText('	ชื่อธุรกิจ (ถ้ามี) '.$profile->company->company_name.' ปีที่ก่อตั้ง..................',$fontbody);
            $text = $section->addText('	ระยะเวลาดำเนินธุรกิจ '.$profile->company->business_processing_time.' ปี',$fontbody);
            $text = $section->addText('	ที่ตั้งธุรกิจ..............................................................................................',$fontbody);
            $text = $section->addText('	โทรศัพท์ '.$profile->phone.' Line ID:................................................................',$fontbody);
            $text = $section->addText('	ประเภท/ รายละเอียดกิจการ..............................................................................................',$fontbody);
            $text = $section->addText('2. การจดทะเบียนจัดตั้งธุรกิจ',$fontbody);
            switch ($profile->business_type_select->id_business_type) {
                case '1':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '2':
                    $text = $section->addText('	🗹 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '3':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
                case '4':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) กลุ่มสหกรณ์',$fontbody);
                    break;
                case '5':
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	🗹 4) อื่นๆ (ระบุ) '.$profile->business_type_select->other_please_specify,$fontbody);
                    break;
                default:
                    $text = $section->addText('	☐ 1) นิติบุคคล ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 2) บุคคลธรรมดา ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 3) วิสาหกิจชุมชน ระบุเลข..............................................................................................',$fontbody);
                    $text = $section->addText('	☐ 4) อื่นๆ (ระบุ)..............................................................................................',$fontbody);
                    break;
            }
            $text = $section->addText('	หมายเหตุ: การจดทะเบียนจัดตั้งธุรกิจ ได้แก่',$fontbody14);
            $text = $section->addText('	1) บุคคลธรรมดา ได้แก่ จดทะเบียนพาณิชย์ และจดทะเบียนการค้า',$fontbody14);
            $text = $section->addText('	2) นิติบุคคล ได้แก่ บริษัทจำกัด และ ห้างส่วนจำกัด',$fontbody14);
            $text = $section->addText('	3) วิสาหกิจชุมชน',$fontbody14);
            $text = $section->addText('3. วิธีการบริหารกิจการ :..............................................................................................',$fontbody);
            $text = $section->addText('	☐ บริหารโดยเจ้าของคนเดียว			🗹 บริหารโดยเจ้าของและพี่น้อง, หรือญาติ',$fontbody);
            $text = $section->addText('	☐ บริหารโดยมีการว่าจ้างบุคคลภายนอก		☐ อื่นๆ (ระบุ)..................',$fontbody);
            $text = $section->addText('4. ประเภทแพคเกจ (เลือกมาหนึ่งอย่างที่เด่นชัดที่สุด)',$fontbody);
            $text = $section->addText('	☐ 1) พัฒนามาตรฐานผลิตภัณฑ์สมุนไพร/เครื่องสำอาง',$fontbody);
            $text = $section->addText('	☐ 2) พัฒนามาตรฐานผลิตภัณฑ์อาหาร เครื่องดื่ม เกษตรแปรรูป',$fontbody);
            $text = $section->addText('	☐ 3) พัฒนามาตรฐานผลิตภัณฑ์ที่มีมูลค่าสูง',$fontbody);
            $text = $section->addText('	☐ 4) พัฒนาการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์/สร้างแบนรด์',$fontbody);
            $text = $section->addText('5. ลักษณะกิจการ / โครงการ',$fontbody);
            $text = $section->addText('	☐ 1) เริ่มกิจการใหม่	☐ 2) ขยายกิจการ	☐ 3) ปรับปรุงกิจการ',$fontbody);
            $text = $section->addText('	คาดว่าจะเริ่มดำเนินการ ประมาณเดือน..................ปี',$fontbody);
            $text = $section->addText('6. ลักษณะสินค้า/บริการที่เข้ารับการพัฒนา  (กรณีที่มีภาพตัวอย่างมากกว่านี้ อาจทำเป็นเอกสารแนบ) ',$fontbody);
            $count_product = Product::where('id_card',$id_card)->count();
            $product = Product::where('id_card',$id_card)->first();
            if ($count_product <> 0) {
                $section->addImage("./voucher/product/".$profile->id_team_owner.'/'.$product->product_image,$imagestyle);
                $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
                $text = $section->addText($product->product_detail,$fontbody);
            } else {
                $section->addImage("./dist/img/block_image.png",$imagestyle);
                $text = $section->addText('รูปภาพสินค้า/บริการ',$fontbody,$fontcenter);
                $text = $section->addText('รายละเอียดสินค้า/บริการ',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
                $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            }
            $text = $section->addText('7. ลักษณะบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('รูปภาพบรรจุภัณฑ์ (ถ้ามี)',$fontbody,$fontcenter);
            $text = $section->addText('รายละเอียดบรรจุภัณฑ์ (ถ้ามี)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('8. ยอดขายโดยประมาณ (บาทต่อเดือน)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('9. แผนผังที่ตั้งกิจการ (ระบุพิกัด GPS  X………………Y………………)',$fontbody);
            $section->addImage("./voucher/document/".$profile->id_team_owner.'/'.$profile->id_card.'/'.$profile->document->map,$imagestyle);
            //ส่วนที่สอง
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 2 รายงานการวิเคราะห์สภาพปัญหา/ความต้องการรับการพัฒนา',$fontheading,$fontcenter);
            if ($count_product <> 0) {
                $text = $section->addText('1. สินค้า/บริการหลัก : '.$product->product_name,$fontbody);
            } else {
                $text = $section->addText('1. สินค้า/บริการหลัก :…………………………………………………………………………………',$fontbody);
            }
            $text = $section->addText('2. ชื่อตราสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('LOGO (ถ้ามี)',$fontbody);
            $text = $section->addText('3. มาตรฐานสินค้า/บริการ (ถ้ามี) :…………………………………………………………………………………',$fontbody);
            $text = $section->addText('4. กำลังการผลิต/บริการสูงสุดต่อปี ……………………………………หน่วย……………………………………',$fontbody);
            $text = $section->addText('กำลังการผลิต/บริการจริงต่อปี ……………………………………หน่วย……………………………………',$fontbody);
            $text = $section->addText('คิดเป็น……………………………………% ปริมาณการผลิตที่คาดหวัง ปี 2564……………………………………',$fontbody);
            $text = $section->addText('5. บุคลากร (Man) ได้แก่',$fontbody);
            $text = $section->addText('	(1) บุคลากรการผลิต',$fontbody);
            $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('	(2) บุคลากรขายและบริหาร',$fontbody);
            $text = $section->addText('		☐ ผู้บริหาร…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานประจำ…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('		☐ พนักงานรายวัน…………………………คน ค่าจ้างรวม…………………………บาท/เดือน',$fontbody);
            $text = $section->addText('6. เครื่องจักร (Machine) (ถ้ามี)',$fontbody);

            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(2000)->addText("รายการ",$fontbody);
            $table->addCell(2000)->addText("ราคาต่อหน่วย (บาท)",$fontbody);
            $table->addCell(2000)->addText("จำนวน",$fontbody);
            $table->addCell(2000)->addText("ราคารวม (บาท)",$fontbody);
            $table->addCell(2000)->addText("อายุการใช้งาน (ปี)",$fontbody);
            $table->addCell(2000)->addText("ค่าเสื่อมราคา (บาทต่อปี)",$fontbody);
            $table->addCell(2000)->addText("ลักษณะการใช้งาน",$fontbody);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addCell(2000);
            $table->addRow();
            $table->addCell(4000, $cellColSpan)->addText("รวม ค่าเสื่อมราคาในการผลิต",$fontbody);
            $table->addCell(2000);
            $table->addCell(2000)->addText("บาทต่อปี ",$fontbody);

            $text = $section->addText('7. วัตถุดิบหลัก (Material) ได้แก่',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(3000)->addText("ลำดับ",$fontbody);
            $table->addCell(3000)->addText("ชื่อวัตถุดิบ",$fontbody);
            $table->addCell(3000)->addText("สถานที่",$fontbody);
            $table->addRow();
            $table->addCell(3000)->addText("1",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("2",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("3",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("4",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $table->addRow();
            $table->addCell(3000)->addText("5",$fontbody);
            $table->addCell(3000);
            $table->addCell(3000);
            $text = $section->addText('8. กระบวนการผลิต (Method)',$fontbody);
            $text = $section->addText('	ขั้นตอนกระบวนการผลิต (Process Flow) (อธิบายขั้นตอนพร้อมภาพประกอบ)',$fontbody);
            $section->addImage("./dist/img/block_image.png",$imagestyle);
            $text = $section->addText('9. การขาย/การตลาด',$fontbody);
            $text = $section->addText('ลูกค้าเป้าหมาย :',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('ช่องทางการจัดจำหน่าย : ',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('10. สภาพปัญหาเบื้องต้นและข้อเสนอแนะเพื่อการพัฒนาผลิตภัณฑ์ (กรุณา X ไม่จำกัดจำนวนข้อ)',$fontbody);
            $text = $section->addText('	◯ การพัฒนาศักยภาพการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านมาตรฐานการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านการปรับปรุงกระบวนการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านออกแบบปรับปรุงเครื่องจักรเพื่อการลดต้นทุน หรือลดขั้นตอนกระบวนการผลิต',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ การพัฒนาผลิตภัณฑ์หรือบริการให้เป็นที่ต้องการของตลาด ',$fontbody);
            $text = $section->addText('		☐ ด้านมาตรฐานผลิตภัณฑ์ อาทิ อย. ฉลากโภชนาการ การตรวจวิเคราะห์ ฯลฯ',$fontbody);
            $text = $section->addText('		☐ ด้านพัฒนาผลิตภัณฑ์ อาทิ ปรับปรุงสูตร ยืดอายุสินค้า ฯลฯ',$fontbody);
            $text = $section->addText('		☐ ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์',$fontbody);
            $text = $section->addText('		☐ ด้านการสร้างแบรนด์',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	การพัฒนาด้านอื่นๆ ที่เกี่ยวข้อง (ถ้ามี)',$fontbodybold);
            $text = $section->addText('	◯ การเชื่อมโยงแหล่งเงินทุน ',$fontbody);
            $text = $section->addText('		☐ ด้านการเขียนแผนธุรกิจฉบับที่สามารถยื่นขอสินเชื่อกับธนาคาร',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ การขยายช่องทางการตลาด ',$fontbody);
            $text = $section->addText('		☐ การทดสอบตลาด (Market Testing) งานแสดงสินค้า',$fontbody);
            $text = $section->addText('		☐ ด้านอื่นๆ (ระบุ) ……………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	◯ อื่นๆ (ระบุ) ',$fontbody);
            $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('		………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	แนวทางการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	ขั้นตอนการพัฒนา',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('11. วิธีการวัดผลการพัฒนาผู้ประกอบการ',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('12. รายละเอียดประมาณการค่าใช้จ่ายในการพัฒนาผู้ประกอบการ (ต่อราย) ',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(8000)->addText("รายจ่าย",$fontbody);
            $table->addCell(2000)->addText("จำนวนเงิน",$fontbody);
            $table->addRow();
            $table->addCell(8000)->addText("งบประมาณสนับสนุน",$fontbody);
            $table->addCell(2000)->addText("(ตามจำนวนที่ได้รับ/หัว และใช้จ่ายจริงในแต่ละราย)",$fontbody);
            $table->addRow();
            $table->addCell(8000)->addText("1. ค่าใช้จ่ายในการให้คำปรึกษาแนะนำ การถ่ายทอดองค์ความรู้เชิงลึก",$fontbody);
            $table->addCell(2000);
            foreach ($consult as $c) {
                $table->addRow();
                $table->addCell(8000)->addText($c['voucher_topic'],$fontbody);
                $table->addCell(2000)->addText($c['voucher_cost'],$fontbody,$fontright);
            }
            $table->addRow();
            $table->addCell(8000)->addText("2. ค่าใช้จ่ายในการพัฒนาผลิตภัณฑ์/ตรวจวิเคราะห์",$fontbody);
            $table->addCell(2000);
            foreach ($voucher as $v) {
                $table->addRow();
                $table->addCell(8000)->addText($v['voucher_topic'],$fontbody);
                $table->addCell(2000)->addText($v['voucher_cost'],$fontbody,$fontright);
            }
            $sum_cost = VoucherRegister::where('id_card',$id_card)->sum('voucher_cost');
            $vat_sum_cost = ($sum_cost*7)/100;
            $total_vat = $sum_cost+$vat_sum_cost;
            $table->addRow();
            $table->addCell(8000)->addText("ค่าใช้จ่ายรวม (ก่อน Vat.)",$fontbody);
            $table->addCell(2000)->addText($sum_cost,$fontbody,$fontright);
            $table->addRow();
            $table->addCell(8000)->addText("ภาษีมูลค่าเพิ่ม 7%",$fontbody);
            $table->addCell(2000)->addText($vat_sum_cost,$fontbody,$fontright);
            $table->addRow();
            $table->addCell(8000)->addText("รวมทั้งสิ้น",$fontbody);
            $table->addCell(2000)->addText($total_vat,$fontbody,$fontright);
            //ส่วนที่ 3
            $section->addPageBreak();
            $text = $section->addText('ส่วนที่ 3 รายงานการพัฒนาผลิตภัณฑ์/บรรจุภัณฑ์',$fontheading,$fontcenter);
            $text = $section->addText('หัวข้อการพัฒนา…………………………………………………………………………………………',$fontbody);
            $text = $section->addText('ขั้นตอน/กระบวนการการพัฒนา',$fontbody);
            $text = $section->addText('	1. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	2. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	3. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	4. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	5. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('	6. …………………………………………………………………………………………',$fontbody);
            $text = $section->addText('รายละเอียดการพัฒนา  (กำหนดแนวทางการปรับปรุง/แก้ไขปัญหา)',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $text = $section->addText('………………………………………………………………………………………………………………………………………………………',$fontbody);
            $table = $section->addTable($tableStyle);
            $table->addRow();
            $table->addCell(5000);
            $table->addCell(5000);
            $table->addRow();
            $table->addCell(5000)->addText("รูปก่อนให้คำปรึกษา",$fontbody,$fontcenter);
            $table->addCell(5000)->addText("รูปหลังให้คำปรึกษา",$fontbody,$fontcenter);
            $text = $section->addText('ภาพเปรียบเทียบก่อนและหลังให้คำปรึกษา เช่น พัฒนาผลิตภัณฑ์ใหม่ (รูปผลิตภัณฑ์) การออกแบบบรรจุภัณฑ์ (รูปบรรจุภัณฑ์) ปรับปรุงกระบวนการผลิต (Workflow การดำเนินงาน) ปรับปรุงเครื่องจักร (รูปเครื่องจักร) เป็นต้น',$fontbody14);
            $text = $section->addText('	ผลสำเร็จของงาน',$fontbodybold);
            $text = $section->addText('	สรุปผลดำเนินงานพัฒนาผู้ประกอบการที่เห็นผลชัดเจนในการยกระดับศักยภาพทางธุรกิจของผู้ประกอบการ ด้าน ………………………………………… โดยสามารถแก้ไขปัญหา ………………………………………… และประเมินผลการยกระดับผู้ประกอบการที่คาดหวัง เช่น มียอดขายเพิ่มขึ้น หรือมีการลงทุนเพิ่มขึ้น หรือมูลค่าการลงทุน หรือการจ้างงานเพิ่มขึ้น หรือการลดต้นทุนต่อหน่วย (บาท) การลดขั้นตอนกระบวนการผลิต (ระยะเวลา : นาที) เป็นต้น)',$fontbody);
            $text = $section->addText('ภาพประกอบการการให้คำปรึกษา',$fontheading,$fontcenter);
            $table = $section->addTable($tableStyle);
            $imagestyle2 = array('width' => 200, 'alignment'=>'center');
            $evidence = array();
            foreach ($consult as $c) {
                if ($c['voucher_evidence_1'] != '') {
                    array_push($evidence, $c['voucher_evidence_1']);
                }
                if ($c['voucher_evidence_2'] != '') {
                    array_push($evidence, $c['voucher_evidence_2']);
                }
                if ($c['voucher_evidence_3'] != '') {
                    array_push($evidence, $c['voucher_evidence_3']);
                }
                if ($c['voucher_evidence_4'] != '') {
                    array_push($evidence, $c['voucher_evidence_4']);
                }
                if ($c['voucher_evidence_5'] != '') {
                    array_push($evidence, $c['voucher_evidence_5']);
                }
            }
            $c_evidence = count($evidence);
            for ($i=0; $i < $c_evidence; $i++) {
                if (($i%2) == 0) {
                    $table->addRow();
                }
                $table->addCell(2000)->addImage("./voucher/evidence/".$profile->id_team_owner."/".$profile->id_card."/".$evidence[$i],$imagestyle2);
            }
            $text = $section->addText();
            $text = $section->addText('ผู้ประกอบการ ………………………………………………         ที่ปรึกษา ………………………………………………',$fontbody,$fontcenter);
            $text = $section->addText('             (                                       )                   (                                            )',$fontbody,$fontcenter);
            $text = $section->addText('พยาน 	………………………………………………',$fontbody,$fontcenter);
            $text = $section->addText('(                                       )',$fontbody,$fontcenter);
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $filename = $id_card.'-รายงานการพัฒนาผู้ประกอบการตามแพ็คเกจ-'.$profile->name.' '.$profile->lastname.'.docx';
            $path = public_path('voucher/reportdoc_temp/' . $filename);
            $objWriter->save($path);
        }
        $zip = new ZipArchive;
        $fileName = 'รายงาน.zip';
        @unlink($fileName);

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(public_path('voucher/reportdoc_temp'));
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        return response()->download(public_path($fileName));
        //return $request->id_card;
        //return $count_id_card
    }
}
