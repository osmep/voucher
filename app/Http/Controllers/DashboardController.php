<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\VoucherRegister;
use App\Models\Product;
use App\Models\DevelopmentDemand;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        switch (Auth::user()->approve_status) {
            case '1':
                return view('home.home_waiting_approve');
                break;
            case '2':
                if (Auth::user()->id_level == 1) {
                    $user = User::where('approve_status',1)->get();
                    $entrepreneur_team_all = Profile::count();
                    $entrepreneur_team_2 = Profile::where('id_comp','like','2%')->count();
                    $entrepreneur_team_3 = Profile::where('id_comp','like','3%')->count();
                    $entrepreneur_team_4 = Profile::where('id_comp','like','4%')->count();
                    $entrepreneur_team_5 = Profile::where('id_comp','like','5%')->count();
                    $entrepreneur_team_6 = Profile::where('id_comp','like','6%')->count();
                    $cost = VoucherRegister::sum('voucher_cost');
                    $product = Product::count();
                    $demand_1 = DevelopmentDemand::where('id_development',1)->count();
                    $demand_2 = DevelopmentDemand::where('id_development',2)->count();
                    $demand_3 = DevelopmentDemand::where('id_development',3)->count();
                    $demand_4 = DevelopmentDemand::where('id_development',4)->count();
                    $demand_5 = DevelopmentDemand::where('id_development',5)->count();
                    //$province = Profile::distinct('province')->orderBy('province','asc')->get();
                    //$province = Profile::groupBy('province')->get();
                    //$province = DB::query('SELECT province,COUNT(province) mycount FROM profile GROUP BY province');
                    //$province = Profile::count('province')->groupBy('province')->get();
                    //SELECT agent_code,COUNT(agent_code) mycount FROM orders GROUP BY agent_code

                    $jan = VoucherRegister::whereMonth('voucher_date', '01')->sum('voucher_cost');
                    $feb = VoucherRegister::whereMonth('voucher_date', '02')->sum('voucher_cost');
                    $mar = VoucherRegister::whereMonth('voucher_date', '03')->sum('voucher_cost');
                    $apr = VoucherRegister::whereMonth('voucher_date', '04')->sum('voucher_cost');
                    $may = VoucherRegister::whereMonth('voucher_date', '05')->sum('voucher_cost');
                    $jun = VoucherRegister::whereMonth('voucher_date', '06')->sum('voucher_cost');
                    $jul = VoucherRegister::whereMonth('voucher_date', '07')->sum('voucher_cost');
                    $aug = VoucherRegister::whereMonth('voucher_date', '08')->sum('voucher_cost');
                    $sep = VoucherRegister::whereMonth('voucher_date', '09')->sum('voucher_cost');
                    $oct = VoucherRegister::whereMonth('voucher_date', '10')->sum('voucher_cost');
                    $nov = VoucherRegister::whereMonth('voucher_date', '11')->sum('voucher_cost');
                    $dec = VoucherRegister::whereMonth('voucher_date', '12')->sum('voucher_cost');
                    $sum_feb = $jan;
                    if ($mar > 0) {
                        $sum_mar = $sum_feb+$feb;
                    } else {
                        $sum_mar = 0;
                    }

                    if ($apr > 0) {
                        $sum_apr = $sum_mar+$mar;
                    } else {
                        $sum_apr = 0;
                    }

                    if ($may > 0) {
                        $sum_may = $sum_apr+$apr;
                    } else {
                        $sum_may = 0;
                    }

                    if ($jun > 0) {
                        $sum_jun = $sum_may+$may;
                    } else {
                        $sum_jun = 0;
                    }

                    if ($jul > 0) {
                        $sum_jul = $sum_jun+$jun;
                    } else {
                        $sum_jul = 0;
                    }

                    if ($aug > 0) {
                        $sum_aug = $sum_jul+$jul;
                    } else {
                        $sum_aug = 0;
                    }

                    if ($sep > 0) {
                        $sum_sep = $sum_aug+$aug;
                    } else {
                        $sum_sep = 0;
                    }

                    if ($oct > 0) {
                        $sum_oct = $sum_sep+$sep;
                    } else {
                        $sum_oct = 0;
                    }

                    if ($nov > 0) {
                        $sum_nov = $sum_oct+$oct;
                    } else {
                        $sum_nov = 0;
                    }

                    if ($dec > 0) {
                        $sum_dec = $sum_nov+$nov;
                    } else {
                        $sum_dec = 0;
                    }

                    //$province = Profile::select('province')->groupBy('province');
                    $province = DB::table('profile')
                                ->select('province', DB::raw('count(*) as total'))
                                ->groupBy('province')
                                ->orderBy('total', 'desc')
                                ->get();
                                // ->paginate(10);

                    $product_type_1 = Product::where('id_product_type',1)->count();
                    $product_type_2 = Product::where('id_product_type',2)->count();
                    $product_type_3 = Product::where('id_product_type',3)->count();
                    $product_type_4 = Product::where('id_product_type',4)->count();

                    $data = array(
                        'user' => $user,
                        'entrepreneur_team_all' => $entrepreneur_team_all,
                        'entrepreneur_team_2' => $entrepreneur_team_2,
                        'entrepreneur_team_3' => $entrepreneur_team_3,
                        'entrepreneur_team_4' => $entrepreneur_team_4,
                        'entrepreneur_team_5' => $entrepreneur_team_5,
                        'entrepreneur_team_6' => $entrepreneur_team_6,
                        'cost' => $cost,
                        'product' => $product,
                        'demand_1' => $demand_1,
                        'demand_2' => $demand_2,
                        'demand_3' => $demand_3,
                        'demand_4' => $demand_4,
                        'demand_5' => $demand_5,
                        //'province' => $province,
                        'jan' => $jan,
                        'feb' => $feb,
                        'mar' => $mar,
                        'apr' => $apr,
                        'may' => $may,
                        'jun' => $jun,
                        'jul' => $jul,
                        'aug' => $aug,
                        'sep' => $sep,
                        'oct' => $oct,
                        'nov' => $nov,
                        'dec' => $dec,

                        'sum_feb' => $sum_feb,
                        'sum_mar' => $sum_mar,
                        'sum_apr' => $sum_apr,
                        'sum_may' => $sum_may,
                        'sum_jun' => $sum_jun,
                        'sum_jul' => $sum_jul,
                        'sum_aug' => $sum_aug,
                        'sum_sep' => $sum_sep,
                        'sum_oct' => $sum_oct,
                        'sum_nov' => $sum_nov,
                        'sum_dec' => $sum_dec,

                        'province' => $province,
                        'product_type_1' => $product_type_1,
                        'product_type_2' => $product_type_2,
                        'product_type_3' => $product_type_3,
                        'product_type_4' => $product_type_4,
                    );
                    return view('home.home_admin',$data);
                }
                else {
                    $entrepreneur_all = Profile::where('id_comp','like',Auth::user()->id_team.'%')->count();
                    $cost = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->sum('voucher_cost');
                    $product = Product::where('id_product','like',Auth::user()->id_team.'%')->count();
                    $demand_1 = DevelopmentDemand::where('id_development_demand','like',Auth::user()->id_team.'%')->where('id_development',1)->count();
                    $demand_2 = DevelopmentDemand::where('id_development_demand','like',Auth::user()->id_team.'%')->where('id_development',2)->count();
                    $demand_3 = DevelopmentDemand::where('id_development_demand','like',Auth::user()->id_team.'%')->where('id_development',3)->count();
                    $demand_4 = DevelopmentDemand::where('id_development_demand','like',Auth::user()->id_team.'%')->where('id_development',4)->count();
                    $demand_5 = DevelopmentDemand::where('id_development_demand','like',Auth::user()->id_team.'%')->where('id_development',5)->count();

                    $product_type_1 = Product::where('id_product','like',Auth::user()->id_team.'%')->where('id_product_type',1)->count();
                    $product_type_2 = Product::where('id_product','like',Auth::user()->id_team.'%')->where('id_product_type',2)->count();
                    $product_type_3 = Product::where('id_product','like',Auth::user()->id_team.'%')->where('id_product_type',3)->count();
                    $product_type_4 = Product::where('id_product','like',Auth::user()->id_team.'%')->where('id_product_type',4)->count();
                    //$province = Profile::distinct('province')->orderBy('province','asc')->get();
                    //$province = Profile::groupBy('province')->get();
                    //$province = DB::query('SELECT province,COUNT(province) mycount FROM profile GROUP BY province');
                    //$province = Profile::count('province')->groupBy('province')->get();
                    //SELECT agent_code,COUNT(agent_code) mycount FROM orders GROUP BY agent_code

                    $jan = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '01')->sum('voucher_cost');
                    $feb = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '02')->sum('voucher_cost');
                    $mar = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '03')->sum('voucher_cost');
                    $apr = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '04')->sum('voucher_cost');
                    $may = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '05')->sum('voucher_cost');
                    $jun = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '06')->sum('voucher_cost');
                    $jul = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '07')->sum('voucher_cost');
                    $aug = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '08')->sum('voucher_cost');
                    $sep = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '09')->sum('voucher_cost');
                    $oct = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '10')->sum('voucher_cost');
                    $nov = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '11')->sum('voucher_cost');
                    $dec = VoucherRegister::where('id_voucher_register','like',Auth::user()->id_team.'%')->whereMonth('voucher_date', '12')->sum('voucher_cost');
                    $sum_feb = $jan;
                    if ($mar > 0) {
                        $sum_mar = $sum_feb+$feb;
                    } else {
                        $sum_mar = 0;
                    }

                    if ($apr > 0) {
                        $sum_apr = $sum_mar+$mar;
                    } else {
                        $sum_apr = 0;
                    }

                    if ($may > 0) {
                        $sum_may = $sum_apr+$apr;
                    } else {
                        $sum_may = 0;
                    }

                    if ($jun > 0) {
                        $sum_jun = $sum_may+$may;
                    } else {
                        $sum_jun = 0;
                    }

                    if ($jul > 0) {
                        $sum_jul = $sum_jun+$jun;
                    } else {
                        $sum_jul = 0;
                    }

                    if ($aug > 0) {
                        $sum_aug = $sum_jul+$jul;
                    } else {
                        $sum_aug = 0;
                    }

                    if ($sep > 0) {
                        $sum_sep = $sum_aug+$aug;
                    } else {
                        $sum_sep = 0;
                    }

                    if ($oct > 0) {
                        $sum_oct = $sum_sep+$sep;
                    } else {
                        $sum_oct = 0;
                    }

                    if ($nov > 0) {
                        $sum_nov = $sum_oct+$oct;
                    } else {
                        $sum_nov = 0;
                    }

                    if ($dec > 0) {
                        $sum_dec = $sum_nov+$nov;
                    } else {
                        $sum_dec = 0;
                    }

                    $province = DB::table('profile')
                                ->where('id_team_owner',Auth::user()->id_team)
                                ->select('province', DB::raw('count(*) as total'))
                                ->groupBy('province')
                                ->orderBy('total', 'desc')
                                ->paginate(10);

                    $data = array(
                        'entrepreneur_all' => $entrepreneur_all,
                        'cost' => $cost,
                        'product' => $product,
                        'demand_1' => $demand_1,
                        'demand_2' => $demand_2,
                        'demand_3' => $demand_3,
                        'demand_4' => $demand_4,
                        'demand_5' => $demand_5,

                        'product_type_1' => $product_type_1,
                        'product_type_2' => $product_type_2,
                        'product_type_3' => $product_type_3,
                        'product_type_4' => $product_type_4,
                        //'province' => $province,
                        'jan' => $jan,
                        'feb' => $feb,
                        'mar' => $mar,
                        'apr' => $apr,
                        'may' => $may,
                        'jun' => $jun,
                        'jul' => $jul,
                        'aug' => $aug,
                        'sep' => $sep,
                        'oct' => $oct,
                        'nov' => $nov,
                        'dec' => $dec,
                        'sum_feb' => $sum_feb,
                        'sum_mar' => $sum_mar,
                        'sum_apr' => $sum_apr,
                        'sum_may' => $sum_may,
                        'sum_jun' => $sum_jun,
                        'sum_jul' => $sum_jul,
                        'sum_aug' => $sum_aug,
                        'sum_sep' => $sum_sep,
                        'sum_oct' => $sum_oct,
                        'sum_nov' => $sum_nov,
                        'sum_dec' => $sum_dec,
                        'province' => $province,
                        'id_team' => Auth::user()->id_team,
                    );
                    return view('home.home_user',$data);
                }
                break;
            case '3':
                return view('home.home_suspend');
            default:
                return view('home.home_suspend');
                break;
        }
    }

    public function dashboardteam($id_team)
    {
        $entrepreneur_all = Profile::where('id_comp','like',$id_team.'%')->count();
        $cost = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->sum('voucher_cost');
        $product = Product::where('id_product','like',$id_team.'%')->count();
        $demand_1 = DevelopmentDemand::where('id_development_demand','like',$id_team.'%')->where('id_development',1)->count();
        $demand_2 = DevelopmentDemand::where('id_development_demand','like',$id_team.'%')->where('id_development',2)->count();
        $demand_3 = DevelopmentDemand::where('id_development_demand','like',$id_team.'%')->where('id_development',3)->count();
        $demand_4 = DevelopmentDemand::where('id_development_demand','like',$id_team.'%')->where('id_development',4)->count();
        $demand_5 = DevelopmentDemand::where('id_development_demand','like',$id_team.'%')->where('id_development',5)->count();
        $product_type_1 = Product::where('id_product','like',$id_team.'%')->where('id_product_type',1)->count();
        $product_type_2 = Product::where('id_product','like',$id_team.'%')->where('id_product_type',2)->count();
        $product_type_3 = Product::where('id_product','like',$id_team.'%')->where('id_product_type',3)->count();
        $product_type_4 = Product::where('id_product','like',$id_team.'%')->where('id_product_type',4)->count();
        //$province = Profile::distinct('province')->orderBy('province','asc')->get();
        //$province = Profile::groupBy('province')->get();
        //$province = DB::query('SELECT province,COUNT(province) mycount FROM profile GROUP BY province');
        //$province = Profile::count('province')->groupBy('province')->get();
        //SELECT agent_code,COUNT(agent_code) mycount FROM orders GROUP BY agent_code

        $jan = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '01')->sum('voucher_cost');
        $feb = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '02')->sum('voucher_cost');
        $mar = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '03')->sum('voucher_cost');
        $apr = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '04')->sum('voucher_cost');
        $may = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '05')->sum('voucher_cost');
        $jun = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '06')->sum('voucher_cost');
        $jul = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '07')->sum('voucher_cost');
        $aug = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '08')->sum('voucher_cost');
        $sep = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '09')->sum('voucher_cost');
        $oct = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '10')->sum('voucher_cost');
        $nov = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '11')->sum('voucher_cost');
        $dec = VoucherRegister::where('id_voucher_register','like',$id_team.'%')->whereMonth('voucher_date', '12')->sum('voucher_cost');
        $sum_feb = $jan;
        if ($mar > 0) {
            $sum_mar = $sum_feb+$feb;
        } else {
            $sum_mar = 0;
        }

        if ($apr > 0) {
            $sum_apr = $sum_mar+$mar;
        } else {
            $sum_apr = 0;
        }

        if ($may > 0) {
            $sum_may = $sum_apr+$apr;
        } else {
            $sum_may = 0;
        }

        if ($jun > 0) {
            $sum_jun = $sum_may+$may;
        } else {
            $sum_jun = 0;
        }

        if ($jul > 0) {
            $sum_jul = $sum_jun+$jun;
        } else {
            $sum_jul = 0;
        }

        if ($aug > 0) {
            $sum_aug = $sum_jul+$jul;
        } else {
            $sum_aug = 0;
        }

        if ($sep > 0) {
            $sum_sep = $sum_aug+$aug;
        } else {
            $sum_sep = 0;
        }

        if ($oct > 0) {
            $sum_oct = $sum_sep+$sep;
        } else {
            $sum_oct = 0;
        }

        if ($nov > 0) {
            $sum_nov = $sum_oct+$oct;
        } else {
            $sum_nov = 0;
        }

        if ($dec > 0) {
            $sum_dec = $sum_nov+$nov;
        } else {
            $sum_dec = 0;
        }

        $province = DB::table('profile')
                    ->where('id_team_owner',$id_team)
                    ->select('province', DB::raw('count(*) as total'))
                    ->groupBy('province')
                    ->orderBy('total', 'desc')
                    ->paginate(10);

        $data = array(
            'entrepreneur_all' => $entrepreneur_all,
            'cost' => $cost,
            'product' => $product,
            'demand_1' => $demand_1,
            'demand_2' => $demand_2,
            'demand_3' => $demand_3,
            'demand_4' => $demand_4,
            'demand_5' => $demand_5,
            'product_type_1' => $product_type_1,
            'product_type_2' => $product_type_2,
            'product_type_3' => $product_type_3,
            'product_type_4' => $product_type_4,
            //'province' => $province,
            'jan' => $jan,
            'feb' => $feb,
            'mar' => $mar,
            'apr' => $apr,
            'may' => $may,
            'jun' => $jun,
            'jul' => $jul,
            'aug' => $aug,
            'sep' => $sep,
            'oct' => $oct,
            'nov' => $nov,
            'dec' => $dec,
            'sum_feb' => $sum_feb,
            'sum_mar' => $sum_mar,
            'sum_apr' => $sum_apr,
            'sum_may' => $sum_may,
            'sum_jun' => $sum_jun,
            'sum_jul' => $sum_jul,
            'sum_aug' => $sum_aug,
            'sum_sep' => $sum_sep,
            'sum_oct' => $sum_oct,
            'sum_nov' => $sum_nov,
            'sum_dec' => $sum_dec,
            'province' => $province,
            'id_team' => $id_team
        );
        return view('home.home_user',$data);
    }
}
