<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advisor;
use App\Models\VoucherRegister;
use App\Models\Profile;
use Auth;
use Session;

class AdvisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_card)
    {
        $profile = Profile::find($id_card);
        $advisor = Advisor::where('id_advisor','like',$profile->id_team_owner.'%')->get();
        $data = array(
            'advisor' => $advisor,
            'id_card' => $id_card
        );
        return view('advisor/form_advisor',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::find($request->id_card_entrepreneur);
        $request->validate([
            'id_card' => 'required|unique:advisor,advisor_id_card',
        ]);
        $advisor = new Advisor;
        $id_advisor = Advisor::where('id_advisor', 'like', $profile->id_team_owner.'%')->pluck('id_advisor')->max();
        if ($id_advisor=='') {
            $id_advisor_1 = $profile->id_team_owner.'000001';
        }
        else
        {
            $id_advisor_1 = $id_advisor+1;
        }
        $advisor->id_advisor = $id_advisor_1;
        $advisor->advisor_id_card = $request->id_card;
        $advisor->advisor_name = $request->advisor_name;
        $advisor->advisor_email = $request->advisor_email;
        $advisor->advisor_phone = $request->advisor_phone;
        $advisor->advisor_expert = $request->advisor_expert;
        $advisor->advisor_sme_coach = $request->advisor_sme_coach;
        if($request->file('advisor_profile')){
            if (!is_dir("voucher/advisor/".$profile->id_team_owner)) {
                mkdir("voucher/advisor/".$profile->id_team_owner);
            }
            $folder = 'voucher/advisor/'.$profile->id_team_owner;
            $filename = $id_advisor_1.'-ประวัติวิทยากร-'.$request->advisor_name.'.'.$request->file('advisor_profile')->getClientOriginalExtension();
            $request->file('advisor_profile')->move($folder,$filename);
            $advisor->advisor_profile = $filename;
        }
        $advisor->save();
        Session::flash('success','เพิ่มข้อมูลสำเร็จ');
        return redirect ('advisor/create/'.$request->id_card_entrepreneur);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$id_card)
    {
        $profile = Profile::find($id_card);
        $advisor = Advisor::where('id_advisor','like',$profile->id_team_owner.'%')->get();
        $advisor_edit = Advisor::find($id);
        $data = array(
            'advisor' => $advisor,
            'advisor_edit' => $advisor_edit,
            'id_card' => $id_card
        );
        return view('advisor/form_advisor',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($request->id_card_entrepreneur);
        $advisor = Advisor::find($id);
        $advisor->advisor_id_card = $request->id_card;
        $advisor->advisor_name = $request->advisor_name;
        $advisor->advisor_email = $request->advisor_email;
        $advisor->advisor_phone = $request->advisor_phone;
        $advisor->advisor_expert = $request->advisor_expert;
        $advisor->advisor_sme_coach = $request->advisor_sme_coach;
        if($request->file('advisor_profile')){
            $folder = 'voucher/advisor/'.$profile->id_team_owner;
            $filename = $id.'-ประวัติวิทยากร-'.$request->advisor_name.'.'.$request->file('advisor_profile')->getClientOriginalExtension();
            $request->file('advisor_profile')->move($folder,$filename);
            $advisor->advisor_profile = $filename;
        }
        $advisor->save();
        Session::flash('success','แก้ไขข้อมูลสำเร็จ');
        return redirect ('advisor/create/'.$request->id_card_entrepreneur);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $voucher = VoucherRegister::where('id_advisor',$id)->get();
        if (count($voucher) > 0) {
            Session::flash('error','ไม่สามารถลบข้อมูลได้ เนื่องจากข้อมูลที่ปรึกษาถูกใช้งานอยู่');
            return redirect('advisor/create/'.$request->id_card);
        } else {
            $advisor = Advisor::find($id);
            @unlink("voucher/advisor/".substr($id,0,1)."/".$advisor->advisor_profile);
            $advisor->delete();
            Session::flash('success','ลบข้อมูลสำเร็จ');
            return redirect('advisor/create/'.$request->id_card);
        }
    }
}
