<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VoucherRegister;
use App\Models\Advisor;
use App\Models\AdvisorRegister;
use App\Models\Profile;
use App\Models\CostSummary;
use Auth;
use Session;
use Illuminate\Support\Facades\Artisan;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $profile = Profile::find($id);
        $voucher_register = VoucherRegister::where('id_card',$id)->orderBy('voucher_date', 'asc')->get();
        $voucher_cost = VoucherRegister::where('id_card',$id)->pluck('voucher_cost')->sum();
        $advisor = Advisor::where('id_advisor','like',$profile->id_team_owner.'%')->get();
        $cost_summary = CostSummary::where('id_card',$id)->first();
        $data = array(
            'voucher_register' => $voucher_register,
            'voucher_cost' => $voucher_cost,
            'advisor' => $advisor,
            'id_card' => $id,
            'cost_summary' => $cost_summary
        );
        return view('voucher/form_voucher',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::find($request->id_card);
        $voucher = new VoucherRegister;
        $id_voucher_register = VoucherRegister::where('id_voucher_register', 'like', $profile->id_team_owner.'%')->pluck('id_voucher_register')->max();
        if ($id_voucher_register=='') {
            $id_voucher_register_1 = $profile->id_team_owner.'000001';
        }
        else
        {
            $id_voucher_register_1 = $id_voucher_register+1;
        }
        $voucher->id_voucher_register = $id_voucher_register_1;
        if ($request->type_activity == 'voucher') {
            $voucher->voucher_topic = $request->voucher_topic;
            $voucher->voucher_detail = $request->voucher_detail;
            if ($request->file('voucher_evidence_1')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $filename = $id_voucher_register_1.'-หลักฐานการใช้งาน Voucher-1.'.$request->file('voucher_evidence_1')->getClientOriginalExtension();
                // $imgwidth = 900; //resize image
                // $file = $request->file('voucher_evidence_1');
                // $path = public_path($folder.'/' . $filename);
                // $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                // if($img->width()>$imgwidth){
                //     // See the docs - http://image.intervention.io/api/resize
                //     // resize the image to a width of 300 and constrain aspect ratio (auto height)
                //     $img->resize($imgwidth, null, function ($constraint) {
                //         $constraint->aspectRatio();
                //     });
                // }
                // $img->save($path);
                $request->file('voucher_evidence_1')->move($folder,$filename);
                $voucher->voucher_evidence_1 = $filename;
            }
            if ($request->file('voucher_evidence_2')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $filename = $id_voucher_register_1.'-หลักฐานการใช้งาน Voucher-2.'.$request->file('voucher_evidence_2')->getClientOriginalExtension();
                // $imgwidth = 900; //resize image
                // $file = $request->file('voucher_evidence_2');
                // $path = public_path($folder.'/' . $filename);
                // $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                // if($img->width()>$imgwidth){
                //     See the docs - http://image.intervention.io/api/resize
                //     resize the image to a width of 300 and constrain aspect ratio (auto height)
                //     $img->resize($imgwidth, null, function ($constraint) {
                //         $constraint->aspectRatio();
                //     });
                // }
                // $img->save($path);
                $request->file('voucher_evidence_2')->move($folder,$filename);
                $voucher->voucher_evidence_2 = $filename;
            }
            if ($request->file('voucher_evidence_3')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $filename = $id_voucher_register_1.'-หลักฐานการใช้งาน Voucher-3.'.$request->file('voucher_evidence_3')->getClientOriginalExtension();
                // $imgwidth = 900; //resize image
                // $file = $request->file('voucher_evidence_3');
                // $path = public_path($folder.'/' . $filename);
                // $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                // if($img->width()>$imgwidth){
                //     // See the docs - http://image.intervention.io/api/resize
                //     // resize the image to a width of 300 and constrain aspect ratio (auto height)
                //     $img->resize($imgwidth, null, function ($constraint) {
                //         $constraint->aspectRatio();
                //     });
                // }
                // $img->save($path);
                $request->file('voucher_evidence_3')->move($folder,$filename);
                $voucher->voucher_evidence_3 = $filename;
            }
            if ($request->file('voucher_evidence_4')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $filename = $id_voucher_register_1.'-หลักฐานการใช้งาน Voucher-4.'.$request->file('voucher_evidence_4')->getClientOriginalExtension();
                // $imgwidth = 900; //resize image
                // $file = $request->file('voucher_evidence_4');
                // $path = public_path($folder.'/' . $filename);
                // $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                // if($img->width()>$imgwidth){
                //     // See the docs - http://image.intervention.io/api/resize
                //     // resize the image to a width of 300 and constrain aspect ratio (auto height)
                //     $img->resize($imgwidth, null, function ($constraint) {
                //         $constraint->aspectRatio();
                //     });
                // }
                // $img->save($path);
                $request->file('voucher_evidence_4')->move($folder,$filename);
                $voucher->voucher_evidence_4 = $filename;
            }
            if ($request->file('voucher_evidence_5')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $filename = $id_voucher_register_1.'-หลักฐานการใช้งาน Voucher-5.'.$request->file('voucher_evidence_5')->getClientOriginalExtension();
                // $imgwidth = 900; //resize image
                // $file = $request->file('voucher_evidence_5');
                // $path = public_path($folder.'/' . $filename);
                // $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                // if($img->width()>$imgwidth){
                //     // See the docs - http://image.intervention.io/api/resize
                //     // resize the image to a width of 300 and constrain aspect ratio (auto height)
                //     $img->resize($imgwidth, null, function ($constraint) {
                //         $constraint->aspectRatio();
                //     });
                // }
                // $img->save($path);
                $request->file('voucher_evidence_5')->move($folder,$filename);
                $voucher->voucher_evidence_5 = $filename;
            }
            $voucher->voucher_cost = $request->voucher_cost;
            $voucher->voucher_date = $request->voucher_date;
            $voucher->voucher_date_stop = $request->voucher_date_stop;
            $voucher->voucher_place = $request->voucher_place;
            $voucher->id_card = $request->id_card;
        } else {
            $voucher->voucher_topic = $request->consult_topic;
            $voucher->voucher_detail = $request->consult_detail;
            if ($request->file('consult_evidence_1')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_1');
                $filename = $id_voucher_register_1.'-หลักฐานการให้คำปรึกษา-1.'.$request->file('consult_evidence_1')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_1 = $filename;
            }
            if ($request->file('consult_evidence_2')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_2');
                $filename = $id_voucher_register_1.'-หลักฐานการให้คำปรึกษา-2.'.$request->file('consult_evidence_2')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_2 = $filename;
            }
            if ($request->file('consult_evidence_3')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_3');
                $filename = $id_voucher_register_1.'-หลักฐานการให้คำปรึกษา-3.'.$request->file('consult_evidence_3')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_3 = $filename;
            }
            if ($request->file('consult_evidence_4')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_4');
                $filename = $id_voucher_register_1.'-หลักฐานการให้คำปรึกษา-4.'.$request->file('consult_evidence_4')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_4 = $filename;
            }
            if ($request->file('consult_evidence_5')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_5');
                $filename = $id_voucher_register_1.'-หลักฐานการให้คำปรึกษา-5.'.$request->file('consult_evidence_5')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_5 = $filename;
            }
            $voucher->voucher_cost = $request->consult_cost;
            $voucher->voucher_date = $request->consult_date;
            $voucher->voucher_date_stop = $request->consult_date_stop;
            $voucher->voucher_place = $request->consult_place;
            $voucher->id_advisor = $request->id_advisor;
            $voucher->id_card = $request->id_card;

            $advisor_register = new AdvisorRegister;
            $id_advisor_register = AdvisorRegister::where('id_advisor_register', 'like', $profile->id_team_owner.'%')->pluck('id_advisor_register')->max();
            if ($id_advisor_register=='') {
                $id_advisor_register_1 = $profile->id_team_owner.'000001';
            }
            else
            {
                $id_advisor_register_1 = $id_advisor_register+1;
            }
            $advisor_register->id_advisor_register = $id_advisor_register_1;
            $advisor_register->id_advisor = $request->id_advisor;
            $advisor_register->date_consult = $request->consult_date;
            $advisor_register->id_voucher_register = $id_voucher_register_1;
            $advisor_register->save();
        }
        $voucher->save();
        Session::flash('success','เพิ่มข้อมูลสำเร็จ');
        return redirect('voucher/create/'.$request->id_card);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $id_card)
    {
        $profile = Profile::find($id_card);
        $voucher_edit = VoucherRegister::find($id);
        $voucher_register = VoucherRegister::where('id_card',$id_card)->orderBy('voucher_date', 'asc')->get();
        $voucher_cost = VoucherRegister::where('id_card',$id_card)->pluck('voucher_cost')->sum();
        $advisor = Advisor::where('id_advisor','like',$profile->id_team_owner.'%')->get();
        $cost_summary = CostSummary::where('id_card',$id)->first();
        // $id_advisor = VoucherRegister::where('voucher_date',$voucher_edit->voucher_date)
        //                         ->where('id_advisor','!=',null)
        //                         ->where('id_voucher_register','like',$profile->id_team_owner.'%')
        //                         ->select('id_advisor')->distinct('id_advisor')->get('id_advisor');
        // $advisor = Advisor::whereNotIn('id_advisor',$id_advisor)->get();
        $data = array(
            'voucher_edit' => $voucher_edit,
            'voucher_register' => $voucher_register,
            'voucher_cost' => $voucher_cost,
            'advisor' => $advisor,
            'id_card' => $id_card,
            'cost_summary' => $cost_summary,
        );
        return view('voucher/form_voucher',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $voucher = VoucherRegister::find($id);
        $profile = Profile::find($voucher->id_card);
        if ($request->type_activity == 'voucher') {
            $voucher->voucher_topic = $request->voucher_topic;
            $voucher->voucher_detail = $request->voucher_detail;
            if ($request->file('voucher_evidence_1')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('voucher_evidence_1');
                $filename = $id.'-หลักฐานการใช้งาน Voucher-1.'.$request->file('voucher_evidence_1')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_1 = $filename;
            }
            if ($request->file('voucher_evidence_2')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('voucher_evidence_2');
                $filename = $id.'-หลักฐานการใช้งาน Voucher-2.'.$request->file('voucher_evidence_2')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_2 = $filename;
            }
            if ($request->file('voucher_evidence_3')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('voucher_evidence_3');
                $filename = $id.'-หลักฐานการใช้งาน Voucher-3.'.$request->file('voucher_evidence_3')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_3 = $filename;
            }
            if ($request->file('voucher_evidence_4')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('voucher_evidence_4');
                $filename = $id.'-หลักฐานการใช้งาน Voucher-4.'.$request->file('voucher_evidence_4')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_4 = $filename;
            }
            if ($request->file('voucher_evidence_5')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('voucher_evidence_5');
                $filename = $id.'-หลักฐานการใช้งาน Voucher-5.'.$request->file('voucher_evidence_5')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_5 = $filename;
            }
            $voucher->voucher_cost = $request->voucher_cost;
            $voucher->voucher_date = $request->voucher_date;
            $voucher->voucher_date_stop = $request->voucher_date_stop;
            $voucher->voucher_place = $request->voucher_place;
        } else {
            $voucher->voucher_topic = $request->consult_topic;
            $voucher->voucher_detail = $request->consult_detail;
            if ($request->file('consult_evidence_1')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_1');
                $filename = $id.'-หลักฐานการให้คำปรึกษา-1.'.$request->file('consult_evidence_1')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_1 = $filename;
            }
            if ($request->file('consult_evidence_2')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_2');
                $filename = $id.'-หลักฐานการให้คำปรึกษา-2.'.$request->file('consult_evidence_2')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_2 = $filename;
            }
            if ($request->file('consult_evidence_3')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_3');
                $filename = $id.'-หลักฐานการให้คำปรึกษา-3.'.$request->file('consult_evidence_3')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_3 = $filename;
            }
            if ($request->file('consult_evidence_4')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_4');
                $filename = $id.'-หลักฐานการให้คำปรึกษา-4.'.$request->file('consult_evidence_4')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_4 = $filename;
            }
            if ($request->file('consult_evidence_5')) {
                if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner);
                }
                if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                    mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
                }
                $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
                $imgwidth = 900; //resize image
                $file = $request->file('consult_evidence_5');
                $filename = $id.'-หลักฐานการให้คำปรึกษา-5.'.$request->file('consult_evidence_5')->getClientOriginalExtension();
                $path = public_path($folder.'/' . $filename);
                $img = \Image::make($file->getRealPath());// create instance of Intervention Image
                if($img->width()>$imgwidth){
                    // See the docs - http://image.intervention.io/api/resize
                    // resize the image to a width of 300 and constrain aspect ratio (auto height)
                    $img->resize($imgwidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save($path);
                $voucher->voucher_evidence_5 = $filename;
            }
            $voucher->voucher_cost = $request->consult_cost;
            $voucher->voucher_date = $request->consult_date;
            $voucher->voucher_date_stop = $request->consult_date_stop;
            $voucher->voucher_place = $request->consult_place;
            $voucher->id_advisor = $request->id_advisor;

            $advisor_register = AdvisorRegister::where('id_voucher_register',$id)->first();
            if ($advisor_register->id_advisor != $request->id_advisor) {
                $advisor_register->id_advisor = $request->id_advisor;
                $advisor_register->date_consult = $request->consult_date;
                $advisor_register->save();
            }
        }
        $voucher->save();
        Artisan::call('view:clear');
        Session::flash('success','แก้ไขข้อมูลสำเร็จ');
        return redirect('voucher/create/'.$request->id_card);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voucher_register = VoucherRegister::find($id);
        @unlink("voucher/evidence/".substr($id,0,1)."/".$voucher_register->id_card."/".$voucher_register->voucher_evidence_1);
        @unlink("voucher/evidence/".substr($id,0,1)."/".$voucher_register->id_card."/".$voucher_register->voucher_evidence_2);
        @unlink("voucher/evidence/".substr($id,0,1)."/".$voucher_register->id_card."/".$voucher_register->voucher_evidence_3);
        @unlink("voucher/evidence/".substr($id,0,1)."/".$voucher_register->id_card."/".$voucher_register->voucher_evidence_4);
        @unlink("voucher/evidence/".substr($id,0,1)."/".$voucher_register->id_card."/".$voucher_register->voucher_evidence_5);
        $voucher_register->delete();
        $advisor_register = AdvisorRegister::where('id_voucher_register',$id)->delete();
        Session::flash('success','ลบข้อมูลสำเร็จ');
        return redirect('voucher/create/'.$voucher_register->id_card);
    }

    function fetchadvisor(Request $request){
        $profile = Profile::find($request->get('id_card'));
        $voucher_date = $request->get('select');
        $result = array();
        $id_advisor = VoucherRegister::where('voucher_date',$voucher_date)
                                ->where('id_advisor','!=',null)
                                ->where('id_voucher_register','like',$profile->id_team_owner.'%')
                                ->select('id_advisor')->distinct('id_advisor')->get('id_advisor');
        $query = Advisor::whereNotIn('id_advisor',$id_advisor)->where('id_advisor','like',$profile->id_team_owner.'%')->get();
        $output = '<option value="">ที่ปรึกษา</option>';
        foreach ($query as $row) {
            $output.='<option value="'.$row->id_advisor.'">'.$row->advisor_name.'</option>';
        }
        echo $output;
    }

    public function savecostsummary(Request $request)
    {
        $profile = Profile::find($request->id_card);
        $cost_summary = new CostSummary;
        $id_cost_summary = CostSummary::where('id_cost_summary', 'like', $profile->id_team_owner.'%')->pluck('id_cost_summary')->max();
        if ($id_cost_summary=='') {
            $id_cost_summary_1 = $profile->id_team_owner.'000001';
        }
        else
        {
            $id_cost_summary_1 = $id_cost_summary+1;
        }
        $cost_summary->id_cost_summary = $id_cost_summary_1;
        if ($request->file('cost_summary')) {
            if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                mkdir("voucher/evidence/".$profile->id_team_owner);
            }
            if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
            }
            $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
            $folderdocument = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
            $filename_doc = $request->id_card.'-สรุปค่าใช้จ่าย.'.$request->file('cost_summary')->getClientOriginalExtension();
            $request->file('cost_summary')->move($folderdocument,$filename_doc);
            $cost_summary->cost_summary = $filename_doc;
        }
        $cost_summary->id_card = $request->id_card;
        $cost_summary->save();
        Session::flash('success','อัพโหลดรายงานสรุปค่าใช้จ่ายสำเร็จ');
        return redirect('voucher/create/'.$request->id_card);
    }

    public function updatecostsummary(Request $request)
    {
        $profile = Profile::find($request->id_card);
        $cost_summary = CostSummary::find($request->id_cost_summary);
        if ($request->file('cost_summary')) {
            if (!is_dir("voucher/evidence/".$profile->id_team_owner)) {
                mkdir("voucher/evidence/".$profile->id_team_owner);
            }
            if (!is_dir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card)) {
                mkdir("voucher/evidence/".$profile->id_team_owner.'/'.$request->id_card);
            }
            $folder = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
            $folderdocument = 'voucher/evidence/'.$profile->id_team_owner.'/'.$request->id_card;
            $filename_doc = $request->id_card.'-สรุปค่าใช้จ่าย.'.$request->file('cost_summary')->getClientOriginalExtension();
            $request->file('cost_summary')->move($folderdocument,$filename_doc);
            $cost_summary->cost_summary = $filename_doc;
        }
        $cost_summary->id_card = $request->id_card;
        $cost_summary->save();
        Artisan::call('view:clear');
        Session::flash('success','แก้ไขรายงานสรุปค่าใช้จ่ายสำเร็จ');
        return redirect('voucher/create/'.$request->id_card);
    }
}
