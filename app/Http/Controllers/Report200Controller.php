<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report200;
use Auth;
use Session;
use File;
use ZipArchive;
use App\Models\Team;

class Report200Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->id_level == 1) {
            $team = Team::where('id_team','!=',1)->get();
            $data = array(
                'team' => $team,
            );
            return view('report200/all_team_report200',$data);
        } else {
            $report = Report200::where('id_team',Auth::user()->id_team)->get();
            $jan1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',1)->first();
            $feb1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',2)->first();
            $mar1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',3)->first();
            $apr1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',4)->first();
            $may1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',5)->first();
            $jun1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',6)->first();
            $jul1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',7)->first();
            $aug1 = Report200::where('id_team',Auth::user()->id_team)->where('report_month',8)->first();
            $data = array(
                'report' => $report,
                'jan1' => $jan1,
                'feb1' => $feb1,
                'mar1' => $mar1,
                'apr1' => $apr1,
                'may1' => $may1,
                'jun1' => $jun1,
                'jul1' => $jul1,
                'aug1' => $aug1,
            );
            return view('report200/all_report200',$data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_report_200 = Report200::where('id_report_200', 'like', Auth::user()->id_team.'%')->pluck('id_report_200')->max();
        if ($id_report_200=='') {
            $id_report_200_1 = Auth::user()->id_team.'000001';
        }
        else
        {
            $id_report_200_1 = $id_report_200+1;
        }
        $find_data = Report200::where('id_team',Auth::user()->id_team)->where('report_month',$request->report_month)->first();
        if ($find_data == '') {
            $report = new Report200;
        } else {
            $report = Report200::find($find_data->id_report_200);
            @unlink("voucher/report200/".substr($find_data->id_report_200,0,1)."/".$find_data->report_200);
        }
        $report->id_report_200 = $id_report_200_1;
        $report->report_month = $request->report_month;
        if($request->file('report_200')){
            if (!is_dir("voucher/report200/".Auth::user()->id_team)) {
                mkdir("voucher/report200/".Auth::user()->id_team);
            }
            $folderdocument = 'voucher/report200/'.Auth::user()->id_team;
            switch ($request->report_month) {
                case '1':
                    $filename_doc = '01-รายงานความคืบหน้า-ประจำเดือนมกราคม.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '2':
                    $filename_doc = '02-รายงานความคืบหน้า-ประจำเดือนกุมภาพันธ์.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '3':
                    $filename_doc = '03-รายงานความคืบหน้า-ประจำเดือนมีนาคม.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '4':
                    $filename_doc = '04-รายงานความคืบหน้า-ประจำเดือนเมษายน.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '5':
                    $filename_doc = '05-รายงานความคืบหน้า-ประจำเดือนพฤษภาคม.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '6':
                    $filename_doc = '06-รายงานความคืบหน้า-ประจำเดือนมิถุนายน.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '7':
                    $filename_doc = '07-รายงานความคืบหน้า-ประจำเดือนกรกฎาคม.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                case '8':
                    $filename_doc = '08-รายงานความคืบหน้า-ประจำเดือนสิงหาคม.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
                default:
                    $filename_doc = '00-รายงานความคืบหน้า.'.$request->file('report_200')->getClientOriginalExtension();
                    break;
            }
            $request->file('report_200')->move($folderdocument,$filename_doc);
            $report->report_200 = $filename_doc;
            $report->id_team = Auth::user()->id_team;
            $report->save();
            Session::flash('success','อัพโหลดเอกสารสำเร็จ');
            return redirect('report200');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report200::find($id);
        @unlink("voucher/report200/".substr($id,0,1)."/".$report->report_200);
        $report->delete();
        Session::flash('success','ลบเอกสารสำเร็จ');
        return redirect('report200');
    }

    public function report200team($id_team)
    {
        $report = Report200::where('id_team',$id_team)->count();
        $jan1 = Report200::where('id_team',$id_team)->where('report_month',1)->first();
        $feb1 = Report200::where('id_team',$id_team)->where('report_month',2)->first();
        $mar1 = Report200::where('id_team',$id_team)->where('report_month',3)->first();
        $apr1 = Report200::where('id_team',$id_team)->where('report_month',4)->first();
        $may1 = Report200::where('id_team',$id_team)->where('report_month',5)->first();
        $jun1 = Report200::where('id_team',$id_team)->where('report_month',6)->first();
        $jul1 = Report200::where('id_team',$id_team)->where('report_month',7)->first();
        $aug1 = Report200::where('id_team',$id_team)->where('report_month',8)->first();
        $team = Team::where('id_team','!=',1)->get();
        $team_name = Team::find($id_team);

        $data = array(
            'report' => $report,
            'jan1' => $jan1,
            'feb1' => $feb1,
            'mar1' => $mar1,
            'apr1' => $apr1,
            'may1' => $may1,
            'jun1' => $jun1,
            'jul1' => $jul1,
            'aug1' => $aug1,
            'team' => $team,
            'team_name' => $team_name,
        );
        return view('report200/all_report200',$data);
    }

    public function downloadallreport200($id_team)
    {
        $zip = new ZipArchive;

        switch ($id_team) {
            case '2':
                $fileName = 'รายงาน สสว200-สำนักงานส่งเสริมเศรษฐกิจสร้างสรรค์ (องค์การมหาชน).zip';
                break;
            case '3':
                $fileName = 'รายงาน สสว200-บริษัท ห้องปฏิบัติการกลาง (ประเทศไทย) จำกัด.zip';
                break;
            case '4':
                $fileName = 'รายงาน สสว200-มหาวิทยาลัยเชียงใหม่.zip';
                break;
            case '5':
                $fileName = 'รายงาน สสว200-สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย.zip';
                break;
            case '6':
                $fileName = 'รายงาน สสว200-อุตสาหกรรมพัฒนามูลนิธิ เพื่อสถาบันอาหาร.zip';
                break;
            default:
                $fileName = 'รายงาน สสว200.zip';
                break;
        }
        @unlink($fileName);

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(public_path('voucher/report200/'.$id_team));
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        return response()->download(public_path($fileName));
    }
}
