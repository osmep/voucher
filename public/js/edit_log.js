$(document).ready(function(){
    $("#prefix").change(function () {
        $("#field_prefix").val("คำนำหน้าชื่อ");
        console.log('คำนำหน้าชื่อ');
    });
    $("#name").change(function () {
        $("#field_name").val("ชื่อ");
        console.log('ชื่อ');
    });
    $("#lastname").change(function () {
        $("#field_lastname").val("นามสกุล");
        console.log('นามสกุล');
    });
    $("#address").change(function () {
        $("#field_address").val("บ้านเลขที่");
        console.log('บ้านเลขที่');
    });
    $("#moo").change(function () {
        $("#field_moo").val("หมู่");
        console.log('หมู่');
    });
    $("#soy").change(function () {
        $("#field_soy").val("ซอย");
        console.log('ซอย');
    });
    $("#road").change(function () {
        $("#field_road").val("ถนน");
        console.log('ถนน');
    });
    $("#province").change(function () {
        $("#field_province").val("จังหวัด");
        console.log('จังหวัด');
    });
    $("#amphur").change(function () {
        $("#field_amphur").val("อำเภอ");
        console.log('อำเภอ');
    });
    $("#district").change(function () {
        $("#field_district").val("ตำบล");
        console.log('ตำบล');
    });
    $("#zipcode").change(function () {
        $("#field_zipcode").val("รหัสไปรษณีย์");
        console.log('รหัสไปรษณีย์');
    });
    $("#phone").change(function () {
        $("#field_phone").val("โทรศัพท์");
        console.log('โทรศัพท์');
    });
    $("#fax").change(function () {
        $("#field_fax").val("โทรสาร");
        console.log('โทรสาร');
    });
    $("#email").change(function () {
        $("#field_email").val("อีเมล");
        console.log('อีเมล');
    });
    $("#company_name").change(function () {
        $("#field_company_name").val("ชื่อสถานประกอบการ");
        console.log('ชื่อสถานประกอบการ');
    });
    $("#osmep_number").change(function () {
        $("#field_osmep_number").val("เลขสมาชิก สสว.");
        console.log('เลขสมาชิก สสว.');
    });
    $(document.getElementsByName("id_business_type")).change(function () {
        $("#field_id_business_type").val("รูปแบบการดำเนินกิจการ");
        console.log('รูปแบบการดำเนินกิจการ');
    });
    $("#other_please_specify").change(function () {
        $("#field_other_please_spacify").val("โปรดระบุรูปแบบการดำเนินกิจการ");
        console.log('โปรดระบุรูปแบบการดำเนินกิจการ');
    });
    $("#business_processing_time").change(function () {
        $("#field_business_processing_time").val("ระยะเวลาดำเนินกิจการ");
        console.log('ระยะเวลาดำเนินกิจการ');
    });
    $("#number_of_employment").change(function () {
        $("#field_number_of_employment").val("จำนวนการจ้างงาน");
        console.log('จำนวนการจ้างงาน');
    });
    $("#problems_in_business").change(function () {
        $("#field_problems_in_business").val("ปัญหาและอุปสรรค ในการดำเนินกิจการ");
        console.log('ปัญหาและอุปสรรค ในการดำเนินกิจการ');
    });
    $(document.getElementsByName("id_development")).change(function () {
        $("#field_id_development").val("ความต้องการรับการพัฒนา");
        console.log('ความต้องการรับการพัฒนา');
    });
    $("#development_other").change(function () {
        $("#field_development_other").val("โปรดระบุความต้องการรับการพัฒนา");
        console.log('โปรดระบุความต้องการรับการพัฒนา');
    });
    $("#development_detail").change(function () {
        $("#field_development_detail").val("รายละเอียดความต้องการรับการพัฒนา");
        console.log('รายละเอียดความต้องการรับการพัฒนา');
    });
    $(document.getElementsByName("id_sector")).change(function () {
        $("#field_id_sector").val("ภาคธุรกิจ");
        console.log('ภาคธุรกิจ');
    });
    $("#tsic").change(function () {
        $("#field_tsic").val("เลข TSIC");
        console.log('เลข TSIC');
    });
    $("#tsic").change(function () {
        $("#field_tsic").val("เลข TSIC");
        console.log('เลข TSIC');
    });

    $("#document").change(function () {
        $("#field_document").val("สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล");
        console.log('สำเนาหนังสือรับรองการจดทะเบียนนิติบุคล');
    });
    $("#map").change(function () {
        $("#field_map").val("แผนที่แสดงที่ตั้งของกิจการ");
        console.log('แผนที่แสดงที่ตั้งของกิจการ');
    });
});
