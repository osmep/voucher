$(document).ready(function ()
{
    // $("#type_activity").click(function () {
    //     $("#type_activity").change(function () {
    //         var str = "";
    //         $("#type_activity option:selected").each(function () {
    //             str += $(this).text();
    //         });
    //         if(str=="voucher")
    //         {
    //             $("#voucher_form").show("slow");
    //             $("#consult_form").hide();
    //         }
    //         else if(str == "consult")
    //         {
    //             $("#voucher_form").hide();
    //             $("#consult_form").show("slow");
    //         }
    //         else
    //         {
    //             $("#voucher_form").hide();
    //             $("#consult_form").hide();
    //         }
    //     })
    //     .change();
    // });

    $("#voucher").click(function () {
        $("#voucher_form").show();
        $("#consult_form").hide();
        console.log('voucher');
        $( "#voucher_topic" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_detail" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_evidence" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_cost" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_date" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_date_stop" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#voucher_place" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $("#consult_topic").prop('required',false);
        $("#consult_detail").prop('required',false);
        $("#consult_evidence").prop('required',false);
        $("#consult_cost").prop('required',false);
        $("#consult_date").prop('required',false);
        $("#consult_date_stop").prop('required',false);
        $("#consult_place").prop('required',false);
        $("#id_advisor").prop('required',false);
        $("#consult_topic").val();
        $("#consult_detail").val();
        $("#consult_evidence").val();
        $("#consult_cost").val();
        $("#consult_date").val();
        $("#consult_date_stop").val();
        $("#consult_place").val();
        $("#id_advisor").val();
    });
    $("#consult").click(function () {
        $("#voucher_form").hide();
        $("#consult_form").show();
        console.log('consult');
        $( "#consult_topic" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_detail" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_evidence" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_cost" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_date" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_date_stop" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#consult_place" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $( "#id_advisor" ).rules( "add", {
            required: true,
            messages: {
            required: "กรุณากรอกข้อมูล",
            }
        });
        $("#voucher_topic").prop('required',false);
        $("#voucher_detail").prop('required',false);
        $("#voucher_evidence").prop('required',false);
        $("#voucher_cost").prop('required',false);
        $("#voucher_date").prop('required',false);
        $("#voucher_date_stop").prop('required',false);
        $("#voucher_place").prop('required',false);
        $("#voucher_topic").val();
        $("#voucher_detail").val('');
        $("#voucher_evidence").val('');
        $("#voucher_cost").val('');
        $("#voucher_date").val('');
        $("#voucher_date_stop").val('');
        $("#voucher_place").val('');
    });
});
