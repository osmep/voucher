<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEditLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edit_log', function (Blueprint $table) {
            $table->bigIncrements('id_edit_log');
            $table->string('id_card',13);
            $table->bigInteger('id_user_edit');
            $table->string('field_name');
            $table->text('text_old');
            $table->text('text_new');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edit_log');
    }
}
