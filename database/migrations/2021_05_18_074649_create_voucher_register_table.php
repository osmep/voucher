<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_register', function (Blueprint $table) {
            $table->unsignedBigInteger('id_voucher_register')->primary();
            $table->string('voucher_topic', 150);
            $table->text('voucher_detail');
            $table->string('voucher_evidence_1', 150)->nullable();
            $table->string('voucher_evidence_2', 150)->nullable();
            $table->string('voucher_evidence_3', 150)->nullable();
            $table->string('voucher_evidence_4', 150)->nullable();
            $table->string('voucher_evidence_5', 150)->nullable();
            $table->unsignedBigInteger('voucher_cost');
            $table->date('voucher_date');
            $table->date('voucher_date_stop');
            $table->string('voucher_place', 150);
            $table->unsignedBigInteger('id_advisor')->nullable();
            $table->string('id_card', 13);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_register');
    }
}
