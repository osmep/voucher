<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->string('id_card',13)->primary();
            $table->string('prefix', 100);
            $table->string('name', 150);
            $table->string('lastname', 150);
            $table->string('address', 100);
            $table->string('moo', 100)->nullable();
            $table->string('soy', 100)->nullable();
            $table->string('road', 100)->nullable();
            $table->string('district', 100);
            $table->string('amphur', 100);
            $table->string('province', 100);
            $table->string('zipcode', 5);
            $table->string('phone', 10);
            $table->string('fax', 10)->nullable();
            $table->string('email', 150);
            $table->unsignedBigInteger('id_team_owner');
            $table->unsignedBigInteger('id_owner');
            $table->unsignedBigInteger('id_comp');
            $table->unsignedBigInteger('id_document');
            $table->unsignedBigInteger('id_development_demand');
            $table->unsignedBigInteger('id_business_type_select');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
