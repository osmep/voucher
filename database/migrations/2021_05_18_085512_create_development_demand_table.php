<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopmentDemandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('development_demand', function (Blueprint $table) {
            $table->unsignedBigInteger('id_development_demand')->primary();
            $table->text('development_detail');
            $table->string('development_other',150)->nullable();
            $table->unsignedBigInteger('id_development');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('development_demand');
    }
}
