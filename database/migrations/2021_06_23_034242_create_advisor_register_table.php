<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisorRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisor_register', function (Blueprint $table) {
            $table->unsignedBigInteger('id_advisor_register')->primary();
            $table->unsignedBigInteger('id_advisor');
            $table->date('date_consult');
            $table->unsignedBigInteger('id_voucher_register');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisor_register');
    }
}
