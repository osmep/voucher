<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReport200Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_200', function (Blueprint $table) {
            $table->unsignedBigInteger('id_report_200')->primary();
            $table->string('report_200',100);
            $table->unsignedBigInteger('report_month');
            $table->unsignedBigInteger('id_team');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_200');
    }
}
