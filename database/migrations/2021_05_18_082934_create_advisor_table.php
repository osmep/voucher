<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvisorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advisor', function (Blueprint $table) {
            $table->unsignedBigInteger('id_advisor')->primary();
            $table->string('advisor_id_card',13);
            $table->string('advisor_name',150);
            $table->string('advisor_email',150);
            $table->string('advisor_phone',150);
            $table->string('advisor_expert',150);
            $table->string('advisor_profile',150);
            $table->string('advisor_sme_coach',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advisor');
    }
}
