<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->unsignedBigInteger('id_comp')->primary();
            $table->string('company_name', 150);
            $table->string('osmep_number', 14);
            $table->unsignedBigInteger('business_processing_time');
            $table->unsignedBigInteger('number_of_employment');
            $table->text('problems_in_business');
            $table->string('tsic', 5);
            $table->unsignedBigInteger('id_sector');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
