<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_type')->insert([
            ['product_type' => 'กลุ่มผลิตภัณฑ์อาหาร และเครื่องดื่ม'],
            ['product_type' => 'กลุ่มผลิตภัณฑ์สมุนไพร'],
            ['product_type' => 'กลุ่มผลิตภัณฑ์เครื่องสำอาง'],
            ['product_type' => 'กลุ่มผลิตภัณฑ์เกษตรแปรรูป'],
        ]);
    }
}
