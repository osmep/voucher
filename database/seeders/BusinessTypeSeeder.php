<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_type')->insert([
            ['business_type' => 'บุคคลธรรมดา'],
            ['business_type' => 'นิติบุคคล (บริษัท/ห้างหุ้นส่วนจำกัด)'],
            ['business_type' => 'วิสาหกิจชุมชน'],
            ['business_type' => 'กลุ่มสหกรณ์'],
            ['business_type' => 'อื่น ๆ'],
        ]);
    }
}
