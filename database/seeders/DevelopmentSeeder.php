<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('development')->insert([
            ['development' => 'ด้านมาตรฐานผลิตภัณฑ์'],
            ['development' => 'ด้านการพัฒนาเทคโนโลยีการผลิต'],
            ['development' => 'ด้านการออกแบบผลิตภัณฑ์/บรรจุภัณฑ์'],
            ['development' => 'ด้านการสร้างแบรนด์/การตลาด'],
            ['development' => 'อื่น ๆ'],
        ]);
    }
}
