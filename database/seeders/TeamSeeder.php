<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team')->insert([
            ['team_name' => 'สำนักงานส่งเสริมวิสาหกิจขนาดกลางและขนาดย่อม'],
            ['team_name' => 'สำนักงานส่งเสริมเศรษฐกิจสร้างสรรค์ (องค์การมหาชน)'],
            ['team_name' => 'บริษัท ห้องปฏิบัติการกลาง (ประเทศไทย) จำกัด'],
            ['team_name' => 'มหาวิทยาลัยเชียงใหม่'],
            ['team_name' => 'สถาบันวิจัยวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย'],
            ['team_name' => 'อุตสาหกรรมพัฒนามูลนิธิ เพื่อสถาบันอาหาร'],
        ]);
    }
}
